library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AXI_PM_Interface_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 5
	);
	port (
		-- Users to add ports here
        gen_port_in     : in std_logic_vector(1+1+32-1 downto 0);
        gen_port_out    : out std_logic_vector(1+1+1+32*5-1 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end AXI_PM_Interface_v1_0;

architecture arch_imp of AXI_PM_Interface_v1_0 is

    signal s_s00_axi_aclk   : std_logic;
    signal i_axi_done       : std_logic; 
    signal i_axi_rd_done    : std_logic;
    signal o_axi_en         : std_logic;     
    signal o_axi_we         : std_logic;
    signal o_axi_x          : unsigned(31 downto 0);
    signal o_axi_y          : unsigned(31 downto 0);
    signal o_axi_s          : unsigned(31 downto 0);
    signal o_axi_f          : unsigned(31 downto 0);
    signal o_axi_px         : unsigned(31 downto 0);
    signal i_axi_px         : unsigned(31 downto 0);
    
    constant oclk_f         : integer   := 1;
    constant en_f           : integer   := 1 + oclk_f;
    constant owe_f          : integer   := 1 + en_f;
    constant ox_f           : integer   := 32 + owe_f;
    constant oy_f           : integer   := 32 + ox_f;
    constant os_f           : integer   := 32 + oy_f;
    constant of_f           : integer   := 32 + os_f;
    constant opx_f          : integer   := 32 + of_f;
    
    constant done_f         : integer   := 1;
    constant rd_done_f      : integer   := 1 + done_f;
    constant ipx_f          : integer   := 32 + rd_done_f;


	-- component declaration
	component AXI_PM_Interface_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 5
		);
		port (
        i_axi_done      : in std_logic;
        i_axi_rd_done   : in std_logic;
        i_axi_px        : in unsigned(31 downto 0);
        o_axi_en        : out std_logic;
        o_axi_we        : out std_logic;
        o_axi_x         : out unsigned(31 downto 0);
        o_axi_y         : out unsigned(31 downto 0);
        o_axi_s         : out unsigned(31 downto 0);
        o_axi_f         : out unsigned(31 downto 0);
        o_axi_px        : out unsigned(31 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component AXI_PM_Interface_v1_0_S00_AXI;

begin

-- Instantiation of Axi Bus Interface S00_AXI
AXI_PM_Interface_v1_0_S00_AXI_inst : AXI_PM_Interface_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
	    i_axi_done     => i_axi_done,
	    i_axi_rd_done  => i_axi_rd_done,
        i_axi_px       => i_axi_px,
        o_axi_en       => o_axi_en,
        o_axi_we       => o_axi_we,  
        o_axi_x        => o_axi_x,   
        o_axi_y        => o_axi_y,   
        o_axi_s        => o_axi_s,   
        o_axi_f        => o_axi_f,   
        o_axi_px       => o_axi_px,  
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here
    i_axi_done                          <= gen_port_in(done_f-1);
    i_axi_rd_done                       <= gen_port_in(rd_done_f-1);
    i_axi_px                            <= unsigned(gen_port_in(ipx_f-1 downto rd_done_f));
    
    gen_port_out(oclk_f-1)              <= s_s00_axi_aclk;
    gen_port_out(en_f-1)                <= o_axi_en;
    gen_port_out(owe_f-1)               <= o_axi_we;
    gen_port_out(ox_f-1 downto owe_f)   <= std_logic_vector(o_axi_x);
    gen_port_out(oy_f-1 downto ox_f)    <= std_logic_vector(o_axi_y);
    gen_port_out(os_f-1 downto oy_f)    <= std_logic_vector(o_axi_s);
    gen_port_out(of_f-1 downto os_f)    <= std_logic_vector(o_axi_f);
    gen_port_out(opx_f-1 downto of_f)   <= std_logic_vector(o_axi_px);
    
    process(s00_axi_aclk) begin
        if s00_axi_aclk = '1' then
            s_s00_axi_aclk <= '1';
        else
            s_s00_axi_aclk <= '0';
        end if;
    end process;
	-- User logic ends

end arch_imp;
