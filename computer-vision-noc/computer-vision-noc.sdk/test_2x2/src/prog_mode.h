/*
 * prog_mode.h
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#ifndef PROG_MODE_H_
#define PROG_MODE_H_

#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "noc.h"
#include "prog_mode.h"

/************************** Variable Definitions *****************************/

#define MEMORY_LENGTH	7
#define PROGRAM_SIZE	128	// 2^MEMORY_LENGTH

/************************** Function Declarations *****************************/

const char* read_program(const char *filename);
void program_noc(const char *program, int x, int y);

#endif /* PROG_MODE_H_ */
