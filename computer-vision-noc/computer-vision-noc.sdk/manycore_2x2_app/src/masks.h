/*
 * masks.h
 *
 *  Created on: Mar 6, 2020
 *      Author: Bruno Almeida
 */

#ifndef SRC_MASKS_H_
#define SRC_MASKS_H_

const char sobel_mask_x[128] =
{
"0,0,-1,\
0,1,0,\
0,2,1,\
1,0,-2,\
1,1,0,\
1,2,2,\
2,0,-1,\
2,1,0,\
2,2,1,\
"
};

const char sobel_mask_y[128] =
{
"0,0,-1,\
0,1,-2,\
0,2,1,\
1,0,0,\
1,1,0,\
1,2,0,\
2,0,1,\
2,1,2,\
2,2,1,\
"
};

#endif /* SRC_MASKS_H_ */
