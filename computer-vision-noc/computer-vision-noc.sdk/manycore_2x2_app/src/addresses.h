/*
 * adresses.h
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#ifndef SRC_ADDRESSES_H_
#define SRC_ADDRESSES_H_

//	0,0 Tile definitions
#define Y_INIT_0_0		0
#define X_INIT_0_0		0
#define PROC_ADDR_0_0	0xA0000000
#define X_ADDR_0_0		PROC_ADDR_0_0 + 4
#define Y_ADDR_0_0		PROC_ADDR_0_0 + 8
#define S_ADDR_0_0		PROC_ADDR_0_0 + 12
#define F_ADDR_0_0		PROC_ADDR_0_0 + 16
#define WPX_ADDR_0_0	PROC_ADDR_0_0 + 20
#define DONE_ADDR_0_0	PROC_ADDR_0_0 + 24
#define RPX_ADDR_0_0	PROC_ADDR_0_0 + 28

//	0,2 Tile definitions
#define Y_INIT_0_2		0
#define X_INIT_0_2		128
#define PROC_ADDR_0_2	0xA0001000
#define X_ADDR_0_2		PROC_ADDR_0_2 + 4
#define Y_ADDR_0_2		PROC_ADDR_0_2 + 8
#define S_ADDR_0_2		PROC_ADDR_0_2 + 12
#define F_ADDR_0_2		PROC_ADDR_0_2 + 16
#define WPX_ADDR_0_2	PROC_ADDR_0_2 + 20
#define DONE_ADDR_0_2	PROC_ADDR_0_2 + 24
#define RPX_ADDR_0_2	PROC_ADDR_0_2 + 28

//	2,0 Tile definitions
#define Y_INIT_2_0		128
#define X_INIT_2_0		0
#define PROC_ADDR_2_0	0xA0002000
#define X_ADDR_2_0		PROC_ADDR_2_0 + 4
#define Y_ADDR_2_0		PROC_ADDR_2_0 + 8
#define S_ADDR_2_0		PROC_ADDR_2_0 + 12
#define F_ADDR_2_0		PROC_ADDR_2_0 + 16
#define WPX_ADDR_2_0	PROC_ADDR_2_0 + 20
#define DONE_ADDR_2_0	PROC_ADDR_2_0 + 24
#define RPX_ADDR_2_0	PROC_ADDR_2_0 + 28

//	2,2 Tile definitions
#define Y_INIT_2_2		128
#define X_INIT_2_2		128
#define PROC_ADDR_2_2	0xA0003000
#define X_ADDR_2_2		PROC_ADDR_2_2 + 4
#define Y_ADDR_2_2		PROC_ADDR_2_2 + 8
#define S_ADDR_2_2		PROC_ADDR_2_2 + 12
#define F_ADDR_2_2		PROC_ADDR_2_2 + 16
#define WPX_ADDR_2_2	PROC_ADDR_2_2 + 20
#define DONE_ADDR_2_2	PROC_ADDR_2_2 + 24
#define RPX_ADDR_2_2	PROC_ADDR_2_2 + 28

#endif /* SRC_ADDRESSES_H_ */
