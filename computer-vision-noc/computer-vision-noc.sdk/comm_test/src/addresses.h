/*
 * addresses.h
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#ifndef SRC_ADDRESSES_H_
#define SRC_ADDRESSES_H_

#define EN_ADDR			0xA0000000
#define WEN_ADDR		EN_ADDR << 1
#define X_ADDR			EN_ADDR + 4
#define Y_ADDR			EN_ADDR + 8
#define S_ADDR			EN_ADDR + 12
#define F_ADDR			EN_ADDR + 16
#define WPX_ADDR		EN_ADDR + 20
#define DONE_ADDR		EN_ADDR + 24
#define RD_DONE_ADDR	DONE_ADDR << 1
#define RPX_ADDR		EN_ADDR + 28

#endif /* SRC_ADDRESSES_H_ */
