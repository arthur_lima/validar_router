/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"

/************************** Variable Definitions *****************************/
#define OPCODE			4 // architecture opcode size

// P-type instruction parameters
#define X_SIZE			6
#define Y_SIZE			6
#define S_SIZE 			6
#define F_SIZE			5
#define PX_TRUE_SIZE	8 // True size, must include 1 bit because of foward/backward function

#define PX_SIZE 		(PX_TRUE_SIZE + 1) // >= BREG_SIZE (MUST SATISFY) MUST INCLUDE + 1

#define PX_INSTR_SIZE	(X_SIZE + Y_SIZE + S_SIZE + F_SIZE + PX_SIZE + OPCODE)

// Memory size (in bits)
#define ADDRESS_SIZE	17

// Register file size (in bits)
#define BREG_SIZE		5

// Considering maximum memory size of 2**17 addresses
#define DEFAULT_SIZE	32 // Based on R-type, branch, nop, engpr and jmp instructions

#if (PX_INSTR_SIZE > DEFAULT_SIZE)
	#define INSTRUCTION_SIZE	PX_INSTR_SIZE
#else
	#define INSTRUCTION_SIZE	DEFAULT_SIZE
#endif

#define PROGRAM_SIZE	2048

#define PROC_ADDR	0x43C00000
#define X_ADDR		PROC_ADDR + 4
#define Y_ADDR		PROC_ADDR + 8
#define S_ADDR		PROC_ADDR + 12
#define F_ADDR		PROC_ADDR + 16
#define WPX_ADDR	PROC_ADDR + 20
#define DONE_ADDR	PROC_ADDR + 24
#define RPX_ADDR	PROC_ADDR + 28

char program[PROGRAM_SIZE * INSTRUCTION_SIZE];

const char* read_program(const char *filename)
{
	FILE *fp;
	char program[PROGRAM_SIZE * INSTRUCTION_SIZE];
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	fp = fopen(filename, "r");
	if (fp == NULL)
	        return 0;

	int line_cnt = 0;
	while((read = __getline(&line, &len, fp)) != -1)
	{
		char *p = &line[0];
		int char_cnt = 0;
		while(*p != '\n')
		{
			program[line_cnt * INSTRUCTION_SIZE + char_cnt] = *p;
			p++;
			char_cnt++;
		}
		line_cnt++;
	}

	char *return_prog = (char *)malloc(PROGRAM_SIZE * INSTRUCTION_SIZE);
	strcpy(return_prog, (const char *)program);

	return (const char *)return_prog;
}

void init_noc()
{
	Xil_Out32(PROC_ADDR, 1);
	usleep(1);
	Xil_Out32(PROC_ADDR, 0);
}

void wait_tile_done()
{
    xil_printf("Wait processing.");
    while(Xil_In32(DONE_ADDR) != 1)
    {
    	xil_printf(".");
    	sleep(0.5);
    }
    xil_printf("\r\n");
}

void init_write_pm()
{
	return Xil_Out32(PROC_ADDR, 1);
}

void end_write_pm()
{
	Xil_Out32(PROC_ADDR, 0);

	// Prevent to write the wrong position if restart
	return Xil_Out32(WPX_ADDR, 0);
}

int write_px(int x, int y, int s, int f, int px)
{
    Xil_Out32(X_ADDR, x);
    Xil_Out32(Y_ADDR, y);
    Xil_Out32(S_ADDR, s);
    Xil_Out32(F_ADDR, f);
    Xil_Out32(WPX_ADDR, px);

    return 1;
}

int read_px(int x, int y, int s, int f)
{
	Xil_Out32(X_ADDR, x);
	Xil_Out32(Y_ADDR, y);
	Xil_Out32(S_ADDR, s);
	Xil_Out32(F_ADDR, f);
	// usleep(10); // Use delay to wait NoC finish processing

	return Xil_In32(RPX_ADDR);
}

int main()
{
    init_platform();
    init_noc();

     wait_tile_done();
//    xil_printf("Wainting...\r\n");
//    sleep(10);

    // Read Pixel values after processing
    int px_value;
    px_value = read_px(0,0,1,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(1,1,1,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,0,2,1);
    xil_printf("Valor lido: %d\r\n", px_value);

    // Start Write Phase
    init_write_pm();
    write_px(1,1,3,1,49);
    write_px(1,1,4,1,255);
    write_px(0,1,3,2,2);
    end_write_pm();
    // End Write Phase

    // Read Pixel values after processing
    px_value = read_px(1,1,3,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(1,1,4,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,1,3,2);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,0,4,0);
    xil_printf("Valor lido: %d\r\n", px_value);

    cleanup_platform();
    return 0;
}
