/*
 * noc.h
 *
 *  Created on: Feb 14, 2020
 *      Author: Bruno Almeida
 */

#ifndef SRC_NOC_H_
#define SRC_NOC_H_

#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "addresses.h"

/************************** Variable Definitions *****************************/

#define NOC_YLIM	1
#define NOC_XLIM	1
#define NOC_SIZE	NOC_YLIM * NOC_XLIM

#define OPCODE			4 // architecture opcode size

// P-type instruction parameters
#define X_SIZE			10
#define Y_SIZE			10
#define S_SIZE 			6
#define F_SIZE			5
#define PX_TRUE_SIZE	8 // True size, must include 1 bit because of foward/backward function

#define PX_SIZE 		(PX_TRUE_SIZE + 1) // >= BREG_SIZE (MUST SATISFY) MUST INCLUDE + 1

#define PX_INSTR_SIZE	(X_SIZE + Y_SIZE + S_SIZE + F_SIZE + PX_SIZE + OPCODE)

// Memory size (in bits)
#define ADDRESS_SIZE	17

// Register file size (in bits)
#define BREG_SIZE		7

// Considering maximum memory size of 2**17 addresses
#define DEFAULT_SIZE	32 // Based on R-type, branch, nop, engpr and jmp instructions

#if (PX_INSTR_SIZE > DEFAULT_SIZE)
	#define INSTRUCTION_SIZE	PX_INSTR_SIZE
#else
	#define INSTRUCTION_SIZE	DEFAULT_SIZE
#endif

/************************** Function Declarations *****************************/

void init_noc();
void (*reset_noc)(void);
int get_noc_index(int x, int y);
void wait_tile_done();
void init_write_pm();
void end_write_pm();
int write_px(int x, int y, int s, int f, int px);
int read_px(int x, int y, int s, int f);
int get_clk_count();

#endif /* SRC_NOC_H_ */
