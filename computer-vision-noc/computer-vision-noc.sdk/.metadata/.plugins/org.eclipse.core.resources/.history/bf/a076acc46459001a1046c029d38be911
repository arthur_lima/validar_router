/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "noc.h"
#include "prog_mode.h"
#include "image.h"

/************************** Variable Definitions *****************************/


const char program[PROGRAM_SIZE * INSTRUCTION_SIZE] =
{
"000000000000000000010000000100000010011\
000000000000000000011000000100000010011\
000000000000000000100000001100000100101\
000000000000000000101000000000000000011\
000000000000000000010000001100000100101\
000000000000000000101000010100000010011\
000000000000100000000000010000001011100\
000000000000000000110000001100000010011\
000000000000000000111000011000001100011\
000000000000000001000000011100001100011\
000000000000000001001000010000000000011\
000000000000000001010000010000000000011\
000000000000000001011000000000000000011\
000000000000000001100000000000000000011\
000000000000000001101000000000000000011\
000000000000000001110000000000000000011\
000000000000000001111000101100010010011\
000000000000000010000000110000010100011\
000111100100000000001000000100011100001\
000000000000000000000000000000000000000\
000000000000000001110000000100011100101\
000000000000000001101000111000011010011\
000000000000000001100000000100011000011\
000000000010000000000000011000011001101\
000000000000000001100000000000000000011\
000000000000000001011000000100010110011\
000000000001110000000000011000010111101\
000000000000000001101000100000011010110\
000000000000000001111000011000010010011\
000000000000000010000000011000010100011\
000111100100000000010000000100011010010\
000000000000000000000000000000000000000\
000000000000000000000000000000000001111\
"
};

#include "square.h"

int main()
{
    init_platform();

//    const char *program = read_program("code.bin");
//    xil_printf("Print program: \r\n");

    init_noc();

	clean_image();

    write_image(square,1,1);

    program_noc(program);

    reset_noc();

    wait_tile_done();
//    xil_printf("Waiting...\r\n");
//    sleep(20);

//    xil_printf("Getting images...\r\n");
	get_image(1, 1);
	sleep(10);
	get_image(2, 1);

//    get_image(1, 1);
//    write_px(0,0,1,1,0);
    cleanup_platform();
    return 0;
}
