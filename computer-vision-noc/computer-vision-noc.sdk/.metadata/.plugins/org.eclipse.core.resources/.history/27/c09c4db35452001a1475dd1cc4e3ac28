/*
 * noc.c
 *
 *  Created on: Feb 14, 2020
 *      Author: Bruno Almeida
 */

#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "addresses.h"
#include "noc.h"

void wait_tile_done()
{
    xil_printf("Wait processing.");
    while(Xil_In32(DONE_ADDR) != 1)
    {
    	xil_printf(".");
    	sleep(10);
    }
    xil_printf("\r\n");
}

void init_noc()
{
	Xil_Out32(PROC_ADDR, 1);
	usleep(1);
	Xil_Out32(PROC_ADDR, 0);
}

void (*reset_noc)(void) = &init_noc;

void init_write_pm()
{
	return Xil_Out32(PROC_ADDR, 1);
}

void end_write_pm()
{
	Xil_Out32(PROC_ADDR, 0);

	// Prevent to write the wrong position if restart
	return Xil_Out32(WPX_ADDR, 0);
}

int write_px(int x, int y, int s, int f, int px)
{
    Xil_Out32(X_ADDR, x);
    Xil_Out32(Y_ADDR, y);
    Xil_Out32(S_ADDR, s);
    Xil_Out32(F_ADDR, f);
    Xil_Out32(WPX_ADDR, px);
    init_write_pm();
    xil_printf("x/y/s/f/px = %d/%d/%d/%d/%d",x,y,s,f,px);
    while(Xil_In32(RPX_ADDR) != px);
    end_write_pm();

    return 1;
}

int read_px(int x, int y, int s, int f)
{
	Xil_Out32(X_ADDR, x);
	Xil_Out32(Y_ADDR, y);
	Xil_Out32(S_ADDR, s);
	Xil_Out32(F_ADDR, f);
	// usleep(10); // Use delay to wait NoC finish processing

	return Xil_In32(RPX_ADDR);
}

int get_clk_count()
{
	int __clk_count = 0;
	Xil_Out32(X_ADDR, 0xAAAAAAAA);
	usleep(100);
	__clk_count = Xil_In32(RPX_ADDR);
	Xil_Out32(X_ADDR, 0);

	return __clk_count;
}
