/*
 * noc.c
 *
 *  Created on: Feb 14, 2020
 *      Author: Bruno Almeida
 */

#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "addresses.h"
#include "noc.h"
#include "image.h"

/* Need fixing for second consecutive try */
void wait_tile_done()
{
	xil_printf("addr %d\r\n", Xil_In32(DONE_ADDR));
    xil_printf("Wait processing.");
    int count = 0;
    while((Xil_In32(DONE_ADDR) & 0x00000001) != 1)
    {
    	usleep(100);
    	if(count == 100000)
    	{
    		xil_printf(".");
    		count = 0;
    	}
    	count++;
    }
    xil_printf("\r\n");
}

void init_noc()
{
	Xil_Out32(EN_ADDR, 3);
	sleep(1);
	Xil_Out32(EN_ADDR, 0);
}

void reset_noc()
{
	Xil_Out32(EN_ADDR, 2);
	sleep(1);
	Xil_Out32(EN_ADDR, 0);
}

void init_rd_wt_pm(int read)
{
	if(read == 1)
		Xil_Out32(EN_ADDR, 1);
	else
		Xil_Out32(EN_ADDR, 3);
}

void end_rd_wt_pm()
{
	Xil_Out32(EN_ADDR, 0);

	// Prevent to write the wrong position if restart
	return Xil_Out32(WPX_ADDR, 0);
}

int write_px(int x, int y, int s, int f, int px)
{
    Xil_Out32(X_ADDR, x);
    Xil_Out32(Y_ADDR, y);
    Xil_Out32(S_ADDR, s);
    Xil_Out32(F_ADDR, f);
    Xil_Out32(WPX_ADDR, (uint32_t )px);

    init_rd_wt_pm(0);
    while((Xil_In32(DONE_ADDR) >> 1) == 0);
    end_rd_wt_pm();
    while((Xil_In32(DONE_ADDR) >> 1) == 1);

    return 1;
}

int read_px(int x, int y, int s, int f)
{
	Xil_Out32(X_ADDR, x);
	Xil_Out32(Y_ADDR, y);
	Xil_Out32(S_ADDR, s);
	Xil_Out32(F_ADDR, f);

    init_rd_wt_pm(1);
    while((Xil_In32(DONE_ADDR) >> 1) == 0);
    end_rd_wt_pm();
    while((Xil_In32(DONE_ADDR) >> 1) == 1);

    int px = Xil_In32(RPX_ADDR);
    if((px >> (PX_LEN-1)) == 1)
    {
    	px = px + ((32 << PX_LEN) & 0xFFFFFFFF);
    }

    return (int );
}

int get_clk_count()
{
	int __clk_count = 0;
	Xil_Out32(X_ADDR, 0xAAAAAAAA);
	usleep(100);
	__clk_count = Xil_In32(RPX_ADDR);
	Xil_Out32(X_ADDR, 0);

	return __clk_count;
}
