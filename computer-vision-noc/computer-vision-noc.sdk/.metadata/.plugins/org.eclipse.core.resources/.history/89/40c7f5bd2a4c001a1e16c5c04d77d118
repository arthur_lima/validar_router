/*
 * prog_mode.h
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#ifndef PROG_MODE_H_
#define PROG_MODE_H_

#include "addresses.h"

/************************** Variable Definitions *****************************/
#define OPCODE			4 // architecture opcode size

// P-type instruction parameters
#define X_SIZE			6
#define Y_SIZE			6
#define S_SIZE 			6
#define F_SIZE			5
#define PX_TRUE_SIZE	8 // True size, must include 1 bit because of foward/backward function

#define PX_SIZE 		(PX_TRUE_SIZE + 1) // >= BREG_SIZE (MUST SATISFY) MUST INCLUDE + 1

#define PX_INSTR_SIZE	(X_SIZE + Y_SIZE + S_SIZE + F_SIZE + PX_SIZE + OPCODE)

// Memory size (in bits)
#define ADDRESS_SIZE	17

// Register file size (in bits)
#define BREG_SIZE		5

// Considering maximum memory size of 2**17 addresses
#define DEFAULT_SIZE	32 // Based on R-type, branch, nop, engpr and jmp instructions

#if (PX_INSTR_SIZE > DEFAULT_SIZE)
	#define INSTRUCTION_SIZE	PX_INSTR_SIZE
#else
	#define INSTRUCTION_SIZE	DEFAULT_SIZE
#endif

#define MEMORY_LENGTH	7
#define PROGRAM_SIZE	128	// 2^MEMORY_LENGTH

/************************** Function Declarations *****************************/

const char* read_program(const char *filename);
void wait_write_imem();
void program_noc(const char *program, int tile);

#endif /* PROG_MODE_H_ */
