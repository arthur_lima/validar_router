/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"

/************************** Variable Definitions *****************************/
//	0,0 Tile definitions
#define Y_INIT_0_0		0
#define X_INIT_0_0		0
#define PROC_ADDR_0_0	0x43C00000
#define X_ADDR_0_0		PROC_ADDR_0_0 + 4
#define Y_ADDR_0_0		PROC_ADDR_0_0 + 8
#define S_ADDR_0_0		PROC_ADDR_0_0 + 12
#define F_ADDR_0_0		PROC_ADDR_0_0 + 16
#define WPX_ADDR_0_0	PROC_ADDR_0_0 + 20
#define DONE_ADDR_0_0	PROC_ADDR_0_0 + 24
#define RPX_ADDR_0_0	PROC_ADDR_0_0 + 28

//	0,2 Tile definitions
#define Y_INIT_0_2		0
#define X_INIT_0_2		2
#define PROC_ADDR_0_2	0x43C10000
#define X_ADDR_0_2		PROC_ADDR_0_2 + 4
#define Y_ADDR_0_2		PROC_ADDR_0_2 + 8
#define S_ADDR_0_2		PROC_ADDR_0_2 + 12
#define F_ADDR_0_2		PROC_ADDR_0_2 + 16
#define WPX_ADDR_0_2	PROC_ADDR_0_2 + 20
#define DONE_ADDR_0_2	PROC_ADDR_0_2 + 24
#define RPX_ADDR_0_2	PROC_ADDR_0_2 + 28

//	0,4 Tile definitions
#define Y_INIT_0_4		0
#define X_INIT_0_4		4
#define PROC_ADDR_0_4	0x43C20000
#define X_ADDR_0_4		PROC_ADDR_0_4 + 4
#define Y_ADDR_0_4		PROC_ADDR_0_4 + 8
#define S_ADDR_0_4		PROC_ADDR_0_4 + 12
#define F_ADDR_0_4		PROC_ADDR_0_4 + 16
#define WPX_ADDR_0_4	PROC_ADDR_0_4 + 20
#define DONE_ADDR_0_4	PROC_ADDR_0_4 + 24
#define RPX_ADDR_0_4	PROC_ADDR_0_4 + 28

//	2,0 Tile definitions
#define Y_INIT_2_0		2
#define X_INIT_2_0		0
#define PROC_ADDR_2_0	0x43C30000
#define X_ADDR_2_0		PROC_ADDR_2_0 + 4
#define Y_ADDR_2_0		PROC_ADDR_2_0 + 8
#define S_ADDR_2_0		PROC_ADDR_2_0 + 12
#define F_ADDR_2_0		PROC_ADDR_2_0 + 16
#define WPX_ADDR_2_0	PROC_ADDR_2_0 + 20
#define DONE_ADDR_2_0	PROC_ADDR_2_0 + 24
#define RPX_ADDR_2_0	PROC_ADDR_2_0 + 28

//	2,2 Tile definitions
#define Y_INIT_2_2		2
#define X_INIT_2_2		2
#define PROC_ADDR_2_2	0x43C40000
#define X_ADDR_2_2		PROC_ADDR_2_2 + 4
#define Y_ADDR_2_2		PROC_ADDR_2_2 + 8
#define S_ADDR_2_2		PROC_ADDR_2_2 + 12
#define F_ADDR_2_2		PROC_ADDR_2_2 + 16
#define WPX_ADDR_2_2	PROC_ADDR_2_2 + 20
#define DONE_ADDR_2_2	PROC_ADDR_2_2 + 24
#define RPX_ADDR_2_2	PROC_ADDR_2_2 + 28

//	2,4 Tile definitions
#define Y_INIT_2_4		2
#define X_INIT_2_4		4
#define PROC_ADDR_2_4	0x43C50000
#define X_ADDR_2_4		PROC_ADDR_2_4 + 4
#define Y_ADDR_2_4		PROC_ADDR_2_4 + 8
#define S_ADDR_2_4		PROC_ADDR_2_4 + 12
#define F_ADDR_2_4		PROC_ADDR_2_4 + 16
#define WPX_ADDR_2_4	PROC_ADDR_2_4 + 20
#define DONE_ADDR_2_4	PROC_ADDR_2_4 + 24
#define RPX_ADDR_2_4	PROC_ADDR_2_4 + 28

//	4,0 Tile definitions
#define Y_INIT_4_0		4
#define X_INIT_4_0		0
#define PROC_ADDR_4_0	0x43C60000
#define X_ADDR_4_0		PROC_ADDR_4_0 + 4
#define Y_ADDR_4_0		PROC_ADDR_4_0 + 8
#define S_ADDR_4_0		PROC_ADDR_4_0 + 12
#define F_ADDR_4_0		PROC_ADDR_4_0 + 16
#define WPX_ADDR_4_0	PROC_ADDR_4_0 + 20
#define DONE_ADDR_4_0	PROC_ADDR_4_0 + 24
#define RPX_ADDR_4_0	PROC_ADDR_4_0 + 28

//	4,2 Tile definitions
#define Y_INIT_4_2		4
#define X_INIT_4_2		2
#define PROC_ADDR_4_2	0x43C70000
#define X_ADDR_4_2		PROC_ADDR_4_2 + 4
#define Y_ADDR_4_2		PROC_ADDR_4_2 + 8
#define S_ADDR_4_2		PROC_ADDR_4_2 + 12
#define F_ADDR_4_2		PROC_ADDR_4_2 + 16
#define WPX_ADDR_4_2	PROC_ADDR_4_2 + 20
#define DONE_ADDR_4_2	PROC_ADDR_4_2 + 24
#define RPX_ADDR_4_2	PROC_ADDR_4_2 + 28

//	4,4 Tile definitions
#define Y_INIT_4_4		4
#define X_INIT_4_4		4
#define PROC_ADDR_4_4	0x43C80000
#define X_ADDR_4_4		PROC_ADDR_4_4 + 4
#define Y_ADDR_4_4		PROC_ADDR_4_4 + 8
#define S_ADDR_4_4		PROC_ADDR_4_4 + 12
#define F_ADDR_4_4		PROC_ADDR_4_4 + 16
#define WPX_ADDR_4_4	PROC_ADDR_4_4 + 20
#define DONE_ADDR_4_4	PROC_ADDR_4_4 + 24
#define RPX_ADDR_4_4	PROC_ADDR_4_4 + 28


void init_noc()
{
	Xil_Out32(PROC_ADDR_0_0, 1);
	Xil_Out32(PROC_ADDR_0_2, 1);
	Xil_Out32(PROC_ADDR_2_0, 1);
	Xil_Out32(PROC_ADDR_2_2, 1);
	usleep(1);
	Xil_Out32(PROC_ADDR_0_0, 0);
	Xil_Out32(PROC_ADDR_0_2, 0);
	Xil_Out32(PROC_ADDR_2_0, 0);
	Xil_Out32(PROC_ADDR_2_2, 0);
}

void wait_tile_done()
{
	int done_mult;
    xil_printf("Wait processing.");
    while(done_mult != 4*1)
    {
    	done_mult = 0;
    	xil_printf(".");
    	sleep(0.5);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_2);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_2);
    }
    xil_printf("\r\n");
}

void init_write_pm()
{
	Xil_Out32(PROC_ADDR_0_0, 1);
	Xil_Out32(PROC_ADDR_0_2, 1);
	Xil_Out32(PROC_ADDR_2_0, 1);
	Xil_Out32(PROC_ADDR_2_2, 1);
}

void end_write_pm()
{
	Xil_Out32(PROC_ADDR_0_0, 0);
	Xil_Out32(PROC_ADDR_0_2, 0);
	Xil_Out32(PROC_ADDR_2_0, 0);
	Xil_Out32(PROC_ADDR_2_2, 0);

	// Prevent to write the wrong position if restart
	Xil_Out32(WPX_ADDR_0_0, 0);
	Xil_Out32(WPX_ADDR_0_2, 0);
	Xil_Out32(WPX_ADDR_2_0, 0);
	Xil_Out32(WPX_ADDR_2_2, 0);
}

int write_px(int x, int y, int s, int f, int px)
{
	if((x < X_INIT_0_2) && (y < Y_INIT_2_0))
	{
		Xil_Out32(X_ADDR_0_0, x);
		Xil_Out32(Y_ADDR_0_0, y);
		Xil_Out32(S_ADDR_0_0, s);
		Xil_Out32(F_ADDR_0_0, f);
		Xil_Out32(WPX_ADDR_0_0, px);
	}else{
		if((x < X_INIT_0_2) && (y < X_INIT_4_0))
		{
			Xil_Out32(X_ADDR_2_0, x);
			Xil_Out32(Y_ADDR_2_0, y);
			Xil_Out32(S_ADDR_2_0, s);
			Xil_Out32(F_ADDR_2_0, f);
			Xil_Out32(WPX_ADDR_2_0, px);
		}else{
			if(x < Y_INIT_0_2)
			{
				Xil_Out32(X_ADDR_0_4, x);
				Xil_Out32(Y_ADDR_0_4, y);
				Xil_Out32(S_ADDR_0_4, s);
				Xil_Out32(F_ADDR_0_4, f);
				Xil_Out32(WPX_ADDR_0_4, px);
			}else{
				if((x < X_INIT_4_0) && (y < ))
				{
					Xil_Out32(X_ADDR_2_2, x);
					Xil_Out32(Y_ADDR_2_2, y);
					Xil_Out32(S_ADDR_2_2, s);
					Xil_Out32(F_ADDR_2_2, f);
					Xil_Out32(WPX_ADDR_2_2, px);
				}
			}
		}
	}

//	int x_y_sum = x + y;
//	switch(x_y_sum)
//	{
//		case 0:
//			Xil_Out32(X_ADDR_0_0, x);
//			Xil_Out32(Y_ADDR_0_0, y);
//			Xil_Out32(S_ADDR_0_0, s);
//			Xil_Out32(F_ADDR_0_0, f);
//			Xil_Out32(WPX_ADDR_0_0, px);
//			break;
//		case 2:
//			if(x > X_INIT_0_2)
//			{
//				Xil_Out32(X_ADDR_0_2, x);
//				Xil_Out32(Y_ADDR_0_2, y);
//				Xil_Out32(S_ADDR_0_2, s);
//				Xil_Out32(F_ADDR_0_2, f);
//				Xil_Out32(WPX_ADDR_0_2, px);
//			}else{
//				Xil_Out32(X_ADDR_2_4, x);
//				Xil_Out32(Y_ADDR_2_4, y);
//				Xil_Out32(S_ADDR_2_4, s);
//				Xil_Out32(F_ADDR_2_4, f);
//				Xil_Out32(WPX_ADDR_2_4, px);
//			}
//			break;
//		case 4:
//			if(y == 4)
//			{
//
//			}
//	}

    return 1;
}

int read_px(int x, int y, int s, int f)
{
	if((x < X_INIT_2_0) && (y < Y_INIT_0_2))
	{
		Xil_Out32(X_ADDR_0_0, x);
		Xil_Out32(Y_ADDR_0_0, y);
		Xil_Out32(S_ADDR_0_0, s);
		Xil_Out32(F_ADDR_0_0, f);

		return Xil_In32(RPX_ADDR_0_0);

	}else{
		if(x < X_INIT_2_0)
		{
			Xil_Out32(X_ADDR_0_2, x);
			Xil_Out32(Y_ADDR_0_2, y);
			Xil_Out32(S_ADDR_0_2, s);
			Xil_Out32(F_ADDR_0_2, f);

			return Xil_In32(RPX_ADDR_0_2);

		}else{
			if(y < Y_INIT_0_2)
			{
				Xil_Out32(X_ADDR_2_0, x);
				Xil_Out32(Y_ADDR_2_0, y);
				Xil_Out32(S_ADDR_2_0, s);
				Xil_Out32(F_ADDR_2_0, f);

				return Xil_In32(RPX_ADDR_2_0);

			}else{
				Xil_Out32(X_ADDR_2_2, x);
				Xil_Out32(Y_ADDR_2_2, y);
				Xil_Out32(S_ADDR_2_2, s);
				Xil_Out32(F_ADDR_2_2, f);

				return Xil_In32(RPX_ADDR_2_2);

			}
		}
	}
	// usleep(10); // Use delay to wait NoC finish processing
}

int main()
{
    init_platform();
    init_noc();

    wait_tile_done();
//    xil_printf("Wainting...\r\n");
//    sleep(10);

    // Read Pixel values after processing
    int px_value;
    px_value = read_px(0,0,1,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(1,1,1,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,0,2,1);
    xil_printf("Valor lido: %d\r\n", px_value);

    // Start Write Phase
    init_write_pm();
    write_px(1,1,3,1,49);
    write_px(1,1,4,1,255);
    write_px(0,1,3,2,2);
    end_write_pm();
    // End Write Phase

    // Read Pixel values after processing
    px_value = read_px(1,1,3,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(1,1,4,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,1,3,2);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,0,4,0);
    xil_printf("Valor lido: %d\r\n", px_value);

    cleanup_platform();
    return 0;
}
