/*
 * noc.c
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "addresses.h"
#include "noc.h"

void init_noc()
{

	noc[0].y		= Y_INIT_0_0;
	noc[0].x 		= X_INIT_0_0;
	noc[0].pc_addr	= PROC_ADDR_0_0;
	noc[0].x_addr	= X_ADDR_0_0;
	noc[0].y_addr	= Y_ADDR_0_0;
	noc[0].s_addr	= S_ADDR_0_0;
	noc[0].f_addr	= F_ADDR_0_0;
	noc[0].wpx_addr	= WPX_ADDR_0_0;
	noc[0].dn_addr	= DONE_ADDR_0_0;
	noc[0].rpx_addr	= RPX_ADDR_0_0;
	noc[0].p		= &noc[1];

	noc[1].y		= Y_INIT_0_2;
	noc[1].x 		= X_INIT_0_2;
	noc[1].pc_addr	= PROC_ADDR_0_2;
	noc[1].x_addr	= X_ADDR_0_2;
	noc[1].y_addr	= Y_ADDR_0_2;
	noc[1].s_addr	= S_ADDR_0_2;
	noc[1].f_addr	= F_ADDR_0_2;
	noc[1].wpx_addr	= WPX_ADDR_0_2;
	noc[1].dn_addr	= DONE_ADDR_0_2;
	noc[1].rpx_addr	= RPX_ADDR_0_2;
	noc[1].p		= &noc[2];

	noc[2].y		= Y_INIT_2_0;
	noc[2].x 		= X_INIT_2_0;
	noc[2].pc_addr	= PROC_ADDR_2_0;
	noc[2].x_addr	= X_ADDR_2_0;
	noc[2].y_addr	= Y_ADDR_2_0;
	noc[2].s_addr	= S_ADDR_2_0;
	noc[2].f_addr	= F_ADDR_2_0;
	noc[2].wpx_addr	= WPX_ADDR_2_0;
	noc[2].dn_addr	= DONE_ADDR_2_0;
	noc[2].rpx_addr	= RPX_ADDR_2_0;
	noc[2].p		= &noc[3];

	noc[3].y		= Y_INIT_2_2;
	noc[3].x 		= X_INIT_2_2;
	noc[3].pc_addr	= PROC_ADDR_2_2;
	noc[3].x_addr	= X_ADDR_2_2;
	noc[3].y_addr	= Y_ADDR_2_2;
	noc[3].s_addr	= S_ADDR_2_2;
	noc[3].f_addr	= F_ADDR_2_2;
	noc[3].wpx_addr	= WPX_ADDR_2_2;
	noc[3].dn_addr	= DONE_ADDR_2_2;
	noc[3].rpx_addr	= RPX_ADDR_2_2;
	noc[3].p		= 0;

	int __count = 0;
	while(__count < NOC_SIZE)
	{
		Xil_Out32(noc[__count].pc_addr, 1);
	}

	usleep(1);

	int __count = 0;
	while(__count < NOC_SIZE)
	{
		Xil_Out32(noc[__count].pc_addr, 0);
	}
}

void (*reset_noc)(void) = &init_noc;

void wait_tile_done()
{
	int done_mult;
    xil_printf("Wait processing.");
    while(done_mult != 4*1)
    {
    	done_mult = 0;
    	xil_printf(".");
    	sleep(0.5);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_2);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_2);
    }
    xil_printf("\r\n");
}

void init_write_pm()
{
	Xil_Out32(PROC_ADDR_0_0, 1);
	Xil_Out32(PROC_ADDR_0_2, 1);
	Xil_Out32(PROC_ADDR_2_0, 1);
	Xil_Out32(PROC_ADDR_2_2, 1);
}

void end_write_pm()
{
	Xil_Out32(PROC_ADDR_0_0, 0);
	Xil_Out32(PROC_ADDR_0_2, 0);
	Xil_Out32(PROC_ADDR_2_0, 0);
	Xil_Out32(PROC_ADDR_2_2, 0);

	// Prevent to write the wrong position if restart
	Xil_Out32(WPX_ADDR_0_0, 0);
	Xil_Out32(WPX_ADDR_0_2, 0);
	Xil_Out32(WPX_ADDR_2_0, 0);
	Xil_Out32(WPX_ADDR_2_2, 0);
}

int write_px(int x, int y, int s, int f, int px)
{
	if((x < X_INIT_0_2) && (y < Y_INIT_2_0))
	{
		Xil_Out32(X_ADDR_0_0, x);
		Xil_Out32(Y_ADDR_0_0, y);
		Xil_Out32(S_ADDR_0_0, s);
		Xil_Out32(F_ADDR_0_0, f);
		Xil_Out32(WPX_ADDR_0_0, px);
	}else{
		if(x < X_INIT_0_2)
		{
			Xil_Out32(X_ADDR_2_0, x);
			Xil_Out32(Y_ADDR_2_0, y);
			Xil_Out32(S_ADDR_2_0, s);
			Xil_Out32(F_ADDR_2_0, f);
			Xil_Out32(WPX_ADDR_2_0, px);
		}else{
			if(y < Y_INIT_2_0)
			{
				Xil_Out32(X_ADDR_0_2, x);
				Xil_Out32(Y_ADDR_0_2, y);
				Xil_Out32(S_ADDR_0_2, s);
				Xil_Out32(F_ADDR_0_2, f);
				Xil_Out32(WPX_ADDR_0_2, px);
			}else{
				Xil_Out32(X_ADDR_2_2, x);
				Xil_Out32(Y_ADDR_2_2, y);
				Xil_Out32(S_ADDR_2_2, s);
				Xil_Out32(F_ADDR_2_2, f);
				Xil_Out32(WPX_ADDR_2_2, px);
			}
		}
	}

    return 1;
}

int read_px(int x, int y, int s, int f)
{
	if((x < X_INIT_0_2) && (y < Y_INIT_2_0))
	{
		Xil_Out32(X_ADDR_0_0, x);
		Xil_Out32(Y_ADDR_0_0, y);
		Xil_Out32(S_ADDR_0_0, s);
		Xil_Out32(F_ADDR_0_0, f);

		return Xil_In32(RPX_ADDR_0_0);

	}else{
		if(x < X_INIT_0_2)
		{
			Xil_Out32(X_ADDR_2_0, x);
			Xil_Out32(Y_ADDR_2_0, y);
			Xil_Out32(S_ADDR_2_0, s);
			Xil_Out32(F_ADDR_2_0, f);

			return Xil_In32(RPX_ADDR_2_0);

		}else{
			if(y < Y_INIT_2_0)
			{
				Xil_Out32(X_ADDR_0_2, x);
				Xil_Out32(Y_ADDR_0_2, y);
				Xil_Out32(S_ADDR_0_2, s);
				Xil_Out32(F_ADDR_0_2, f);

				return Xil_In32(RPX_ADDR_0_2);

			}else{
				Xil_Out32(X_ADDR_2_2, x);
				Xil_Out32(Y_ADDR_2_2, y);
				Xil_Out32(S_ADDR_2_2, s);
				Xil_Out32(F_ADDR_2_2, f);

				return Xil_In32(RPX_ADDR_2_2);

			}
		}
	}
	// usleep(10); // Use delay to wait NoC finish processing
}

int get_clk_count()
{
	int __clk_count = 0;
	Xil_Out32(X_ADDR_0_0, 0xAAAAAAAA);
	usleep(100);
	__clk_count = Xil_In32(RPX_ADDR_0_0);
	Xil_Out32(X_ADDR_0_0, 0);

	return __clk_count;
}
