/*
 * image.c
 *
 *  Created on: Feb 14, 2020
 *      Author: Bruno Almeida
 */

#include <stdlib.h>
#include <string.h>
#include "sleep.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "noc.h"
#include "image.h"

/************** Reference: https://stackoverflow.com/a/12923949	****************/

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

typedef enum {
    STR2INT_SUCCESS,
    STR2INT_OVERFLOW,
    STR2INT_UNDERFLOW,
    STR2INT_INCONVERTIBLE
} str2int_errno;

/* Convert string s to int out.
 *
 * @param[out] out The converted int. Cannot be NULL.
 *
 * @param[in] s Input string to be converted.
 *
 *     The format is the same as strtol,
 *     except that the following are inconvertible:
 *
 *     - empty string
 *     - leading whitespace
 *     - any trailing characters that are not part of the number
 *
 *     Cannot be NULL.
 *
 * @param[in] base Base to interpret string in. Same range as strtol (2 to 36).
 *
 * @return Indicates if the operation succeeded, or why it failed.
 */
str2int_errno str2int(int *out, char *s, int base) {
    char *end;
    if (s[0] == '\0' || isspace(s[0]))
        return STR2INT_INCONVERTIBLE;
    errno = 0;
    long l = strtol(s, &end, base);
    /* Both checks are needed because INT_MAX == LONG_MAX is possible. */
    if (l > INT_MAX || (errno == ERANGE && l == LONG_MAX))
        return STR2INT_OVERFLOW;
    if (l < INT_MIN || (errno == ERANGE && l == LONG_MIN))
        return STR2INT_UNDERFLOW;
    if (*end != '\0')
        return STR2INT_INCONVERTIBLE;
    *out = l;
    return STR2INT_SUCCESS;
}

/*******************************************************************************/

void get_word(char *word, const char *image, int init_field, int end_field)
{
	strncpy(word, image + init_field + 1, end_field - init_field - 1);
	word[end_field - init_field] = '\0';
}

void write_image(const char *image, const int s, const int f)
{
	int cnt = 0, step = 0;
	int x, y, px = 0;
	int init_field = 0;
	char token = ',';
	char *p = (char *)image;
	while(*p)
	{
		if(*p == token)
		{
			int end_field = cnt;
			char temp[64] = {0};
			get_word(temp, image, init_field, end_field);
			switch(step%3)
			{
				case 0:
					str2int(&x, temp, 10);
					break;
				case 1:
					str2int(&y, temp, 10);
					break;
				case 2:
					str2int(&px, temp, 10);
					write_px(x,y,s,f,px);
					while(read_px(x,y,s,f) != px)
					{
						write_px(x,y,s,f,px);
						usleep(1);
						xil_printf("%d\r\n",read_px(x,y,s,f));
					}
					break;
			}
			step++;
			init_field = end_field;
		}
		cnt++;
		p++;
	}
}

void clean_image()
{
	for(int i=0; i < (1 << TILE_WIDTH); i++)
	{
		for(int j=0; j < (1 << TILE_HEIGTH); j++)
		{
			write_px(i,j,1,1,0);
		}
	}
}

void get_image(const int s, const int f)
{
	// Flag to send new image
	xil_printf("imgstart\r\n");
	// Print image properties
	xil_printf("%d,%d,%d,%d\r\n",(1 << TILE_WIDTH),(1 << TILE_HEIGTH),s,f);
	for(int i=0; i < (1 << TILE_WIDTH); i++)
	{
		for(int j=0; j < (1 << TILE_HEIGTH); j++)
		{
			int px;
			px = read_px(i,j,s,f);
			xil_printf("%d, %d, %d\r\n",i,j,px);
		}
	}
	// Image final package
	xil_printf("imgend\r\n");
}
