/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "noc.h"
#include "prog_mode.h"

/************************** Variable Definitions *****************************/

const char program[PROGRAM_SIZE * INSTRUCTION_SIZE] =
{
"000000000000000000001000001000010011\
000000000000000000001100010000010011\
000000000000000000001100011000100011\
000000000000000000101000001000000011\
000000000000000000101100001000000011\
000000000000000000010100011000010100\
000000000000000000010000011000100101\
000000000000000000101100010010110101\
000000000000000000101001010000010011\
000000000000111000000000011010101100\
000000000000000000110000010010110101\
000000000000000001000010000001010010\
000000000000000000000000000000000000\
000001000001000001000010000011000010\
000000000000000000000000000000000000\
000000000000000001000010000001100001\
000000000000000000000000000000000000\
000000000000000010000010000010110010\
000000000000000000000000000000000000\
000000000000000010000010000001110001\
000000000000000000000000000000000000\
000001000001000001000010000010000001\
000000000000000000000000000000000000\
000000000000000000000000000000001111"
};

const char prog_endpgr[PROGRAM_SIZE * INSTRUCTION_SIZE] =
{
		"000000000000000000000000000000001111"
};

int main()
{
    init_platform();

    init_noc();

    for(int i=1; i <= NOC_XLIM; i++){
    	for(int j=1; j <= NOC_YLIM; j++)
    	{
    		if (i == 3 && j == 3)
    			program_noc(program, i, j);
    		else
				program_noc(prog_endpgr, i, j);
    	}
    }

    reset_noc();

    wait_tile_done();
    // xil_printf("Waiting...\r\n");
    // sleep(30);

    // Read Pixel values after processing
    int px_value;
    px_value = read_px(0,0,1,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(1,1,1,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(0,0,2,1);
    xil_printf("Valor lido: %d\r\n", px_value);

    xil_printf("Oscilations = %d\r\n", get_clk_count());

    // Start Write Phase
    init_write_pm();
    write_px(3,1,3,1,49);
    write_px(3,1,4,1,255);
    write_px(2,1,3,2,2);
    end_write_pm();
    // End Write Phase

    // Read Pixel values after processing
    px_value = read_px(3,1,3,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(3,1,4,1);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(2,1,3,2);
    xil_printf("Valor lido: %d\r\n", px_value);
    px_value = read_px(3,0,4,0);
    xil_printf("Valor lido: %d\r\n", px_value);

    cleanup_platform();
    return 0;
}
