/*
 * noc.c
 *
 *  Created on: 10 de fev de 2020
 *      Author: Bruno Almeida
 */

#include "sleep.h"
#include "xparameters.h"
#include "xil_io.h"
#include "platform.h"
#include "xil_printf.h"
#include "addresses.h"
#include "noc.h"

void init_noc()
{
	noc[0].y		= Y_INIT_0_0;
	noc[0].x 		= X_INIT_0_0;
	noc[0].pc_addr	= PROC_ADDR_0_0;
	noc[0].x_addr	= X_ADDR_0_0;
	noc[0].y_addr	= Y_ADDR_0_0;
	noc[0].s_addr	= S_ADDR_0_0;
	noc[0].f_addr	= F_ADDR_0_0;
	noc[0].wpx_addr	= WPX_ADDR_0_0;
	noc[0].dn_addr	= DONE_ADDR_0_0;
	noc[0].rpx_addr	= RPX_ADDR_0_0;

	noc[1].y		= Y_INIT_0_2;
	noc[1].x 		= X_INIT_0_2;
	noc[1].pc_addr	= PROC_ADDR_0_2;
	noc[1].x_addr	= X_ADDR_0_2;
	noc[1].y_addr	= Y_ADDR_0_2;
	noc[1].s_addr	= S_ADDR_0_2;
	noc[1].f_addr	= F_ADDR_0_2;
	noc[1].wpx_addr	= WPX_ADDR_0_2;
	noc[1].dn_addr	= DONE_ADDR_0_2;
	noc[1].rpx_addr	= RPX_ADDR_0_2;

	noc[2].y		= Y_INIT_0_4;
	noc[2].x 		= X_INIT_0_4;
	noc[2].pc_addr	= PROC_ADDR_0_4;
	noc[2].x_addr	= X_ADDR_0_4;
	noc[2].y_addr	= Y_ADDR_0_4;
	noc[2].s_addr	= S_ADDR_0_4;
	noc[2].f_addr	= F_ADDR_0_4;
	noc[2].wpx_addr	= WPX_ADDR_0_4;
	noc[2].dn_addr	= DONE_ADDR_0_4;
	noc[2].rpx_addr	= RPX_ADDR_0_4;

	noc[3].y		= Y_INIT_2_0;
	noc[3].x 		= X_INIT_2_0;
	noc[3].pc_addr	= PROC_ADDR_2_0;
	noc[3].x_addr	= X_ADDR_2_0;
	noc[3].y_addr	= Y_ADDR_2_0;
	noc[3].s_addr	= S_ADDR_2_0;
	noc[3].f_addr	= F_ADDR_2_0;
	noc[3].wpx_addr	= WPX_ADDR_2_0;
	noc[3].dn_addr	= DONE_ADDR_2_0;
	noc[3].rpx_addr	= RPX_ADDR_2_0;

	noc[4].y		= Y_INIT_2_2;
	noc[4].x 		= X_INIT_2_2;
	noc[4].pc_addr	= PROC_ADDR_2_2;
	noc[4].x_addr	= X_ADDR_2_2;
	noc[4].y_addr	= Y_ADDR_2_2;
	noc[4].s_addr	= S_ADDR_2_2;
	noc[4].f_addr	= F_ADDR_2_2;
	noc[4].wpx_addr	= WPX_ADDR_2_2;
	noc[4].dn_addr	= DONE_ADDR_2_2;
	noc[4].rpx_addr	= RPX_ADDR_2_2;

	noc[5].y		= Y_INIT_2_4;
	noc[5].x 		= X_INIT_2_4;
	noc[5].pc_addr	= PROC_ADDR_2_4;
	noc[5].x_addr	= X_ADDR_2_4;
	noc[5].y_addr	= Y_ADDR_2_4;
	noc[5].s_addr	= S_ADDR_2_4;
	noc[5].f_addr	= F_ADDR_2_4;
	noc[5].wpx_addr	= WPX_ADDR_2_4;
	noc[5].dn_addr	= DONE_ADDR_2_4;
	noc[5].rpx_addr	= RPX_ADDR_2_4;

	noc[6].y		= Y_INIT_4_0;
	noc[6].x 		= X_INIT_4_0;
	noc[6].pc_addr	= PROC_ADDR_4_0;
	noc[6].x_addr	= X_ADDR_4_0;
	noc[6].y_addr	= Y_ADDR_4_0;
	noc[6].s_addr	= S_ADDR_4_0;
	noc[6].f_addr	= F_ADDR_4_0;
	noc[6].wpx_addr	= WPX_ADDR_4_0;
	noc[6].dn_addr	= DONE_ADDR_4_0;
	noc[6].rpx_addr	= RPX_ADDR_4_0;

	noc[7].y		= Y_INIT_4_2;
	noc[7].x 		= X_INIT_4_2;
	noc[7].pc_addr	= PROC_ADDR_4_2;
	noc[7].x_addr	= X_ADDR_4_2;
	noc[7].y_addr	= Y_ADDR_4_2;
	noc[7].s_addr	= S_ADDR_4_2;
	noc[7].f_addr	= F_ADDR_4_2;
	noc[7].wpx_addr	= WPX_ADDR_4_2;
	noc[7].dn_addr	= DONE_ADDR_4_2;
	noc[7].rpx_addr	= RPX_ADDR_4_2;

	noc[8].y		= Y_INIT_4_4;
	noc[8].x 		= X_INIT_4_4;
	noc[8].pc_addr	= PROC_ADDR_4_4;
	noc[8].x_addr	= X_ADDR_4_4;
	noc[8].y_addr	= Y_ADDR_4_4;
	noc[8].s_addr	= S_ADDR_4_4;
	noc[8].f_addr	= F_ADDR_4_4;
	noc[8].wpx_addr	= WPX_ADDR_4_4;
	noc[8].dn_addr	= DONE_ADDR_4_4;
	noc[8].rpx_addr	= RPX_ADDR_4_4;

	int __count = 0;
	while(__count < NOC_SIZE)
	{
		Xil_Out32(noc[__count].pc_addr, 1);
		__count++;
	}

	usleep(1);

	__count = 0;
	while(__count < NOC_SIZE)
	{
		Xil_Out32(noc[__count].pc_addr, 0);
		__count++;
	}
}

void reset_noc()
{
	int __count = 0;
	while(__count < NOC_SIZE)
	{
		Xil_Out32(noc[__count].pc_addr, 1);
		__count++;
	}

	usleep(1);

	__count = 0;
	while(__count < NOC_SIZE)
	{
		Xil_Out32(noc[__count].pc_addr, 0);
		__count++;
	}
}

int get_noc_index(int x, int y)
{
	int __count = 0;
	while(__count < NOC_SIZE)
	{
		if(noc[__count].x == x)
			if(noc[__count].y == y)
				return __count;
		__count++;
	}

	return 0;
}

void wait_tile_done()
{
	int done_mult;
    xil_printf("Wait processing.");
    while(done_mult != 9*1)
    {
    	done_mult = 0;
    	xil_printf(".");
    	sleep(0.5);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_2);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_0_4);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_2);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_2_4);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_4_0);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_4_2);
    	done_mult = done_mult + Xil_In32(DONE_ADDR_4_4);
    }
    xil_printf("\r\n");
}

void init_write_pm()
{
	Xil_Out32(PROC_ADDR_0_0, 1);
	Xil_Out32(PROC_ADDR_0_2, 1);
	Xil_Out32(PROC_ADDR_0_4, 1);
	Xil_Out32(PROC_ADDR_2_0, 1);
	Xil_Out32(PROC_ADDR_2_2, 1);
	Xil_Out32(PROC_ADDR_2_4, 1);
	Xil_Out32(PROC_ADDR_4_0, 1);
	Xil_Out32(PROC_ADDR_4_2, 1);
	Xil_Out32(PROC_ADDR_4_4, 1);
}

void end_write_pm()
{
	Xil_Out32(PROC_ADDR_0_0, 0);
	Xil_Out32(PROC_ADDR_0_2, 0);
	Xil_Out32(PROC_ADDR_0_4, 0);
	Xil_Out32(PROC_ADDR_2_0, 0);
	Xil_Out32(PROC_ADDR_2_2, 0);
	Xil_Out32(PROC_ADDR_2_4, 0);
	Xil_Out32(PROC_ADDR_4_0, 0);
	Xil_Out32(PROC_ADDR_4_2, 0);
	Xil_Out32(PROC_ADDR_4_4, 0);

	// Prevent to write the wrong position if restart
	Xil_Out32(WPX_ADDR_0_0, 0);
	Xil_Out32(WPX_ADDR_0_2, 0);
	Xil_Out32(WPX_ADDR_0_4, 0);
	Xil_Out32(WPX_ADDR_2_0, 0);
	Xil_Out32(WPX_ADDR_2_2, 0);
	Xil_Out32(WPX_ADDR_2_4, 0);
	Xil_Out32(WPX_ADDR_4_0, 0);
	Xil_Out32(WPX_ADDR_4_2, 0);
	Xil_Out32(WPX_ADDR_4_4, 0);
}

int write_px(int x, int y, int s, int f, int px)
{
	if((x < X_INIT_0_2) && (y < Y_INIT_2_0))
	{
		Xil_Out32(X_ADDR_0_0, x);
		Xil_Out32(Y_ADDR_0_0, y);
		Xil_Out32(S_ADDR_0_0, s);
		Xil_Out32(F_ADDR_0_0, f);
		Xil_Out32(WPX_ADDR_0_0, px);

	}else{
		if((x < X_INIT_0_2) && (y < Y_INIT_4_0))
		{
			Xil_Out32(X_ADDR_2_0, x);
			Xil_Out32(Y_ADDR_2_0, y);
			Xil_Out32(S_ADDR_2_0, s);
			Xil_Out32(F_ADDR_2_0, f);
			Xil_Out32(WPX_ADDR_2_0, px);

		}else{
			if(x < X_INIT_0_2)
			{
				Xil_Out32(X_ADDR_4_0, x);
				Xil_Out32(Y_ADDR_4_0, y);
				Xil_Out32(S_ADDR_4_0, s);
				Xil_Out32(F_ADDR_4_0, f);
				Xil_Out32(WPX_ADDR_4_0, px);

			}else{
				if((x < X_INIT_0_4) && (y < Y_INIT_2_0))
				{
					Xil_Out32(X_ADDR_0_2, x);
					Xil_Out32(Y_ADDR_0_2, y);
					Xil_Out32(S_ADDR_0_2, s);
					Xil_Out32(F_ADDR_0_2, f);
					Xil_Out32(WPX_ADDR_0_2, px);

				}else{
					if((x < X_INIT_0_4) && (y < Y_INIT_4_0))
					{
						Xil_Out32(X_ADDR_2_2, x);
						Xil_Out32(Y_ADDR_2_2, y);
						Xil_Out32(S_ADDR_2_2, s);
						Xil_Out32(F_ADDR_2_2, f);
						Xil_Out32(WPX_ADDR_2_2, px);

					}else{
						if(x < X_INIT_0_4)
						{
							Xil_Out32(X_ADDR_4_2, x);
							Xil_Out32(Y_ADDR_4_2, y);
							Xil_Out32(S_ADDR_4_2, s);
							Xil_Out32(F_ADDR_4_2, f);
							Xil_Out32(WPX_ADDR_4_2, px);

						}else{
							if(y < Y_INIT_2_0)
							{
								Xil_Out32(X_ADDR_0_4, x);
								Xil_Out32(Y_ADDR_0_4, y);
								Xil_Out32(S_ADDR_0_4, s);
								Xil_Out32(F_ADDR_0_4, f);
								Xil_Out32(WPX_ADDR_0_4, px);

							}else{
								if(y < Y_INIT_4_0)
								{
									Xil_Out32(X_ADDR_2_4, x);
									Xil_Out32(Y_ADDR_2_4, y);
									Xil_Out32(S_ADDR_2_4, s);
									Xil_Out32(F_ADDR_2_4, f);
									Xil_Out32(WPX_ADDR_2_4, px);

								}else{
									Xil_Out32(X_ADDR_4_4, x);
									Xil_Out32(Y_ADDR_4_4, y);
									Xil_Out32(S_ADDR_4_4, s);
									Xil_Out32(F_ADDR_4_4, f);
									Xil_Out32(WPX_ADDR_4_4, px);

								}
							}
						}
					}
				}
			}
		}
	}

    return 1;
}

int read_px(int x, int y, int s, int f)
{
	if((x < X_INIT_0_2) && (y < Y_INIT_2_0))
	{
		Xil_Out32(X_ADDR_0_0, x);
		Xil_Out32(Y_ADDR_0_0, y);
		Xil_Out32(S_ADDR_0_0, s);
		Xil_Out32(F_ADDR_0_0, f);

		return Xil_In32(RPX_ADDR_0_0);

	}else{
		if((x < X_INIT_0_2) && (y < Y_INIT_4_0))
		{
			Xil_Out32(X_ADDR_2_0, x);
			Xil_Out32(Y_ADDR_2_0, y);
			Xil_Out32(S_ADDR_2_0, s);
			Xil_Out32(F_ADDR_2_0, f);

			return Xil_In32(RPX_ADDR_2_0);

		}else{
			if(x < X_INIT_0_2)
			{
				Xil_Out32(X_ADDR_4_0, x);
				Xil_Out32(Y_ADDR_4_0, y);
				Xil_Out32(S_ADDR_4_0, s);
				Xil_Out32(F_ADDR_4_0, f);

				return Xil_In32(RPX_ADDR_4_0);

			}else{
				if((x < X_INIT_0_4) && (y < Y_INIT_2_0))
				{
					Xil_Out32(X_ADDR_0_2, x);
					Xil_Out32(Y_ADDR_0_2, y);
					Xil_Out32(S_ADDR_0_2, s);
					Xil_Out32(F_ADDR_0_2, f);

					return Xil_In32(RPX_ADDR_0_2);

				}else{
					if((x < X_INIT_0_4) && (y < Y_INIT_4_0))
					{
						Xil_Out32(X_ADDR_2_2, x);
						Xil_Out32(Y_ADDR_2_2, y);
						Xil_Out32(S_ADDR_2_2, s);
						Xil_Out32(F_ADDR_2_2, f);

						return Xil_In32(RPX_ADDR_2_2);

					}else{
						if(x < X_INIT_0_4)
						{
							Xil_Out32(X_ADDR_4_2, x);
							Xil_Out32(Y_ADDR_4_2, y);
							Xil_Out32(S_ADDR_4_2, s);
							Xil_Out32(F_ADDR_4_2, f);

							return Xil_In32(RPX_ADDR_4_2);

						}else{
							if(y < Y_INIT_2_0)
							{
								Xil_Out32(X_ADDR_0_4, x);
								Xil_Out32(Y_ADDR_0_4, y);
								Xil_Out32(S_ADDR_0_4, s);
								Xil_Out32(F_ADDR_0_4, f);

								return Xil_In32(RPX_ADDR_0_4);

							}else{
								if(y < Y_INIT_4_0)
								{
									Xil_Out32(X_ADDR_2_4, x);
									Xil_Out32(Y_ADDR_2_4, y);
									Xil_Out32(S_ADDR_2_4, s);
									Xil_Out32(F_ADDR_2_4, f);

									return Xil_In32(RPX_ADDR_2_4);

								}else{
									Xil_Out32(X_ADDR_4_4, x);
									Xil_Out32(Y_ADDR_4_4, y);
									Xil_Out32(S_ADDR_4_4, s);
									Xil_Out32(F_ADDR_4_4, f);

									return Xil_In32(RPX_ADDR_4_4);
								}
							}
						}
					}
				}
			}
		}
	}
}

int get_clk_count()
{
	int __clk_count = 0;
	Xil_Out32(X_ADDR_0_0, 0xAAAAAAAA);
	usleep(100);
	__clk_count = Xil_In32(RPX_ADDR_0_0);
	Xil_Out32(X_ADDR_0_0, 0);

	return __clk_count;
}
