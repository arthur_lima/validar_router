----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/03/2020 05:14:19 PM
-- Design Name: 
-- Module Name: manycore_2x2_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity manycore_2x2_tb is
--  Port ( );
end manycore_2x2_tb;

architecture Behavioral of manycore_2x2_tb is
component manycore_2x2 is
port(
    clk                 : in std_logic; -- general clock for processing
    gen_port_in_0_0     : in std_logic_vector(1+1+32*5-1 downto 0);
    gen_port_out_0_0    : out std_logic_vector(1+32-1 downto 0);
    gen_port_in_0_2     : in std_logic_vector(1+1+32*5-1 downto 0);
    gen_port_out_0_2    : out std_logic_vector(1+32-1 downto 0);
    gen_port_in_2_0     : in std_logic_vector(1+1+32*5-1 downto 0);
    gen_port_out_2_0    : out std_logic_vector(1+32-1 downto 0);
    gen_port_in_2_2     : in std_logic_vector(1+1+32*5-1 downto 0);
    gen_port_out_2_2    : out std_logic_vector(1+32-1 downto 0)
);
end component manycore_2x2;

signal    clk                 : std_logic; -- general clock for processing
signal    gen_port_in_0_0     : std_logic_vector(1+1+32*5-1 downto 0);
signal    gen_port_out_0_0    : std_logic_vector(1+32-1 downto 0);
signal    gen_port_in_0_2     : std_logic_vector(1+1+32*5-1 downto 0);
signal    gen_port_out_0_2    : std_logic_vector(1+32-1 downto 0);
signal    gen_port_in_2_0     : std_logic_vector(1+1+32*5-1 downto 0);
signal    gen_port_out_2_0    : std_logic_vector(1+32-1 downto 0);
signal    gen_port_in_2_2     : std_logic_vector(1+1+32*5-1 downto 0);
signal    gen_port_out_2_2    : std_logic_vector(1+32-1 downto 0);

begin
    mpsoc_tb : manycore_2x2 port    map(
                                        clk => clk,
                                        gen_port_in_0_0 => gen_port_in_0_0,
                                        gen_port_out_0_0 => gen_port_out_0_0,
                                        gen_port_in_0_2 => gen_port_in_0_2,
                                        gen_port_out_0_2 => gen_port_out_0_2,
                                        gen_port_in_2_0 => gen_port_in_2_0,
                                        gen_port_out_2_0 => gen_port_out_2_0,
                                        gen_port_in_2_2 => gen_port_in_2_2,
                                        gen_port_out_2_2 => gen_port_out_2_2
                                        );

process
begin
    gen_port_in_0_0 <= std_logic_vector(to_unsigned(2, gen_port_in_0_2'length));
    gen_port_in_0_2 <= std_logic_vector(to_unsigned(2, gen_port_in_0_2'length));
    gen_port_in_2_0 <= std_logic_vector(to_unsigned(2, gen_port_in_0_2'length));
    gen_port_in_2_2 <= std_logic_vector(to_unsigned(2, gen_port_in_0_2'length));
    wait for 5 ns;
    gen_port_in_0_0 <= (others => '0');
    gen_port_in_0_2 <= (others => '0');
    gen_port_in_2_0 <= (others => '0');
    gen_port_in_2_2 <= (others => '0');
    wait for 500000 ms;    
end process;

process
begin
    clk <= '1';
    wait for 10 ns;
    clk <= '0';
    wait for 10 ns;
end process;

end Behavioral;
