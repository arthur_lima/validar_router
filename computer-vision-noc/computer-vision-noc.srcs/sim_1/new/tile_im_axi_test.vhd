----------------------------------------------------------------------------------
-- Company: Universidade de Brasília
-- Engineer: Bruno Almeida
-- 
-- Create Date: 08.02.2020 14:40:26
-- Design Name: 
-- Module Name: tile_im_axi_test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use STD.TEXTIO.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tile_im_axi_test is
--  Port ( );
end tile_im_axi_test;

architecture Behavioral of tile_im_axi_test is

component tile_comm_test is
port(
    clk             : in std_logic; -- general clock for processing
    gen_port_in     : in std_logic_vector(1+1+1+32*5-1 downto 0);
    gen_port_out    : out std_logic_vector(1+1+32-1 downto 0);
    o_router_addr    : out unsigned(31 downto 0);
    o_router_x       : out unsigned(31 downto 0);
    o_router_y       : out unsigned(31 downto 0);
    o_router_s       : out unsigned(31 downto 0);
    o_router_f       : out unsigned(31 downto 0);
    o_router_px      : out unsigned(31 downto 0);
    
    o_x_out        : out UNSIGNED(31 downto 0);      
    o_y_out        : out UNSIGNED(31 downto 0);      
    o_f_out        : out UNSIGNED(31 downto 0);      
    o_s_out        : out UNSIGNED(31 downto 0);      
    o_px_out       : out UNSIGNED(31 downto 0) ;     

    o_idec_x : out STD_LOGIC_VECTOR(6 downto 0);                  
    o_idec_y : out STD_LOGIC_VECTOR(6 downto 0);                  
    o_idec_f : out STD_LOGIC_VECTOR(6 downto 0);                  
    o_idec_s : out STD_LOGIC_VECTOR(6 downto 0);                  
    o_idec_px : out STD_LOGIC_VECTOR(6 downto 0);                
    o_instruction : out STD_LOGIC_VECTOR(38 downto 0);
            
    o_PE_pixel      : out STD_LOGIC_VECTOR(8 downto 0);
    o_PE_x_dest     : out STD_LOGIC_VECTOR(7 downto 0);
    o_PE_y_dest     : out STD_LOGIC_VECTOR(7 downto 0);
    o_PE_step       : out STD_LOGIC_VECTOR(3 downto 0);  
    o_PE_frame      : out STD_LOGIC_VECTOR(0 downto 0); 
    o_PE_x_orig     : out STD_LOGIC_VECTOR(7 downto 0);
    o_PE_y_orig     : out STD_LOGIC_VECTOR(7 downto 0);
    
    o_opcode    : out STD_LOGIC_VECTOR(4-1 downto 0);
    o_rs, o_rt, o_ula_out  : out STD_LOGIC_VECTOR(32-1 downto 0)
);
end component tile_comm_test;

signal clk          : std_logic; -- general clock for processing
signal gen_port_out : std_logic_vector(1+1+1+32*5-1 downto 0);
signal gen_port_in  : std_logic_vector(1+1+32-1 downto 0);

signal i_axi_done       : std_logic;     
signal o_axi_en         : std_logic;
signal o_axi_we         : std_logic;
signal o_axi_x          : unsigned(31 downto 0)   := (others => '0');
signal o_axi_y          : unsigned(31 downto 0)   := (others => '0');
signal o_axi_s          : unsigned(31 downto 0)   := (others => '0');
signal o_axi_f          : unsigned(31 downto 0)   := (others => '0');
signal o_axi_px         : signed(31 downto 0)   := (others => '0');
signal i_axi_rd_done    : std_logic;
signal i_axi_px         : unsigned(31 downto 0);

constant oclk_f         : integer   := 1;
constant oen_f          : integer   := 1 + oclk_f;
constant owe_f          : integer   := 1 + oen_f;
constant ox_f           : integer   := 32 + owe_f;
constant oy_f           : integer   := 32 + ox_f;
constant os_f           : integer   := 32 + oy_f;
constant of_f           : integer   := 32 + os_f;
constant opx_f          : integer   := 32 + of_f;

constant done_f         : integer   := 1;
constant rd_done_f      : integer   := 1 + done_f;
constant ipx_f          : integer   := 32 + rd_done_f;

-- TODO: change x, y, s, f and px based on noc topology
constant noc_width : natural := 1; -- noc size
constant noc_height : natural := 1; -- noc size
constant px_true_size : natural := 9;   -- true pixel depth
constant pix_depth : natural := px_true_size + 1; -- must include +1 because of foward/backward 
constant img_width : natural := 8; -- resolution
constant img_height: natural := 8; -- resolution
constant tile_width  : natural := 8; -- tile resolution 
constant tile_height : natural := 8; -- tile resolution
constant n_steps     : natural := 4; -- steps in the image processing chain
constant n_frames    : natural := 1; -- frames needed by an algorithm

-- router parameters
constant subimg_width 	: natural := 2**8; --2**2; --4;
constant subimg_height	: natural := 2**8; --2**2; --4;
constant buffer_length  : natural := 6;

-- secondary parameters
constant memory_length : natural := 2**11; -- size of memory (2^program_length-1 downto to 0)
constant regfile_word : natural := 3;   -- word length in each register (2^regfile_word-1 downto 0)
                                        -- (word length)*(number of words) = bit width
                                        -- (2**regfile_word)*(2**(bit_width-regfile_word)) = 2**bit_width 
constant regfile_size : natural := 7;   -- size of register file (2^regfile_size-1 downto 0)
constant regfile_num : natural := 2**regfile_size;    -- total of registers inside regfile - 1
                                                        -- (for index purposes)
constant bit_width : natural := 5;  -- processor bit width (2^bit_width-1 downto 0)
                                    -- considering each register with the same width as bit_width

constant addr_size : natural := 17; -- address field size in bits

-- instruction set parameters (isf = instruction set field)
constant isf_opcode : natural := 4;
constant isf_x : natural := regfile_size; -- temporary 
constant isf_y : natural := regfile_size; -- temporary
constant isf_s : natural := regfile_size; -- temporary
constant isf_f : natural := regfile_size; -- temporary
constant isf_px: natural := regfile_size; -- px value range = 0 to 255
constant isf_rs : natural := regfile_size;
constant isf_rt : natural := regfile_size;
constant isf_rd : natural := regfile_size;
constant isf_dest : natural := regfile_size;

-- TODO: use if statement to choose instruction length based on P-type instru. size and 2**bit_width
constant instruction_length : natural := isf_opcode+isf_x+isf_y+isf_s+isf_f+isf_px; -- bits per instruction
--constant instruction_length : natural := 32;

constant file_name : string := "/home/bsilva/Documents/UnB/Projeto_Final/parts/sample_code/code_new_isf_test.bin";
--constant file_name : string := "/home/bsilva/Documentos/UnB/Final-Project/digital_systems/Vivado/code/code_new_isf_test.bin";

type mem_array is array(0 to memory_length-1)
                   of STD_LOGIC_VECTOR(instruction_length-1 downto 0);

-- by FPGA4student.com
-- --   modified by Bruno Almeida
-- inputs binary text files of .text Memory Segment 
impure function init_mem(code_f_name : in string) return mem_array is
    file temp_code_file : text open read_mode is code_f_name;
    variable temp_line : line;
    variable temp_bv : bit_vector(instruction_length-1 downto 0);
--        variable temp_bv : bit_vector(7 downto 0); -- temporary byte
    variable temp_mem : mem_array;
begin
    for i in mem_array'range loop
        if (not endfile(temp_code_file)) then 
            readline(temp_code_file, temp_line);
            read(temp_line, temp_bv);
            temp_mem(i) := to_stdlogicvector(temp_bv);
        else
            temp_mem(i) := (others => '0');
        end if;
    end loop;
    return temp_mem;
end function;

signal mem : mem_array := init_mem(file_name);

begin
    tile_inst : tile_comm_test port map(
                                        clk             => clk,
                                        gen_port_in     => gen_port_out,
                                        gen_port_out    => gen_port_in
                                        );


    i_axi_done                          <= gen_port_in(done_f-1);
    i_axi_rd_done                       <= gen_port_in(rd_done_f-1);
    i_axi_px                            <= unsigned(gen_port_in(ipx_f-1 downto rd_done_f));
    
    gen_port_out(oclk_f-1)              <= clk;
    gen_port_out(oen_f-1)               <= o_axi_en;
    gen_port_out(owe_f-1)               <= o_axi_we;
    gen_port_out(ox_f-1 downto owe_f)   <= std_logic_vector(o_axi_x);
    gen_port_out(oy_f-1 downto ox_f)    <= std_logic_vector(o_axi_y);
    gen_port_out(os_f-1 downto oy_f)    <= std_logic_vector(o_axi_s);
    gen_port_out(of_f-1 downto os_f)    <= std_logic_vector(o_axi_f);
    gen_port_out(opx_f-1 downto of_f)   <= std_logic_vector(o_axi_px);
    
    process
    begin
        clk <= '1';
        wait for 10 ns;
        clk <= '0';
        wait for 10 ns;
    end process;
    
    process
        variable pc : natural := 0;
        variable ref_write, ref_read : natural := 0;
    begin

        for i in -64 to 63 loop
            for j in -64 to 63 loop
                o_axi_en    <= '1';
                o_axi_we    <= '1';
                o_axi_x     <= to_unsigned(i+128, o_axi_x'length);
                o_axi_y     <= to_unsigned(j+128, o_axi_x'length);
                o_axi_s     <= to_unsigned(1, o_axi_x'length);
                o_axi_f     <= to_unsigned(0, o_axi_x'length);
                o_axi_px    <= to_signed(255, o_axi_x'length);
                wait for 10 ns;
                while(i_axi_rd_done = '0') loop
                    wait for 5 ns;
                end loop;
                o_axi_en    <= '0';
                o_axi_we    <= '0';
                while(i_axi_rd_done = '1') loop
                    wait for 5 ns;
                end loop;
                ref_write := ref_write + 1;
                wait for 30 ns;
            end loop;
        end loop;

        for i in -2 to 1 loop
            for j in -2 to 1 loop
                o_axi_en    <= '1';
                o_axi_we    <= '1';
                o_axi_x     <= to_unsigned(i+2, o_axi_x'length);
                o_axi_y     <= to_unsigned(j+2, o_axi_x'length);
                o_axi_s     <= to_unsigned(2, o_axi_x'length);
                o_axi_f     <= to_unsigned(0, o_axi_x'length);
                o_axi_px    <= to_signed(-1, o_axi_x'length);
                wait for 10 ns;
                while(i_axi_rd_done = '0') loop
                    wait for 5 ns;
                end loop;
                o_axi_en    <= '0';
                o_axi_we    <= '0';
                while(i_axi_rd_done = '1') loop
                    wait for 5 ns;
                end loop;
                ref_write := ref_write + 1;
                wait for 30 ns;
            end loop;
        end loop;

        o_axi_en    <= '0';
        o_axi_we    <= '0';
        wait for 30 ns;
    
        -- Setup programming mode
        o_axi_we    <= '1';
        o_axi_x     <= x"FFFFFFFF";
        
        for i in mem_array'range loop
            o_axi_y     <= to_unsigned(pc, o_axi_y'length);
--            o_axi_s     <= signed(mem(i)(instruction_length-1 downto 0));
--            o_axi_f     <= (others => '0');
            o_axi_s     <= unsigned(mem(i)(31 downto 0));
            o_axi_f     <= resize(unsigned(mem(i)(instruction_length-1 downto 32)), o_axi_f'length);
            -- Handshake - memory pull up i_axi_done to inform data was written correctly
            while i_axi_done /= '1' loop
                wait for 1 ns;
            end loop;
            -- then, pull down to tell AXI that it can receive one more package. 
            while i_axi_done /= '0' loop
                wait for 1 ns;
            end loop;
            pc := pc + 4;
        end loop;
                
        -- Setup programming mode
        o_axi_we    <= '0';
        o_axi_x     <= x"00000000";
        
--        wait for 12 us;
        
--        -- Start to read PM
--        o_axi_we    <= '1';
        
--        o_axi_x     <= to_signed(1, o_axi_x'length);
--        o_axi_y     <= to_signed(1, o_axi_x'length);
--        o_axi_s     <= to_signed(1, o_axi_x'length);
--        o_axi_f     <= to_signed(1, o_axi_x'length);
        
--        o_axi_we    <= '0';
        
--        wait for 30 ns;

--        o_axi_x     <= to_signed(0, o_axi_x'length);
--        o_axi_y     <= to_signed(0, o_axi_x'length);
--        o_axi_s     <= to_signed(1, o_axi_x'length);
--        o_axi_f     <= to_signed(1, o_axi_x'length);
        
--        o_axi_we    <= '0';
        
--        wait for 30 ns;
    
--        o_axi_x     <= to_signed(0, o_axi_x'length);
--        o_axi_y     <= to_signed(0, o_axi_x'length);
--        o_axi_s     <= to_signed(2, o_axi_x'length);
--        o_axi_f     <= to_signed(1, o_axi_x'length);

        o_axi_en    <= '1';
        o_axi_we    <= '1';        
        wait for 30 ns;
        o_axi_en    <= '0';
        o_axi_we    <= '0';
        wait for 30 ns;
        
        while i_axi_done /= '1' loop
            wait for 15 ns;
        end loop;
        
        for i in 0 to 255 loop
            for j in 0 to 255 loop
                o_axi_en    <= '1';
                o_axi_x     <= to_unsigned(i, o_axi_x'length);
                o_axi_y     <= to_unsigned(j, o_axi_x'length);
                o_axi_s     <= to_unsigned(0, o_axi_x'length);
                o_axi_f     <= to_unsigned(0, o_axi_x'length);
                while(i_axi_rd_done = '0') loop
                    wait for 5 ns;
                end loop;
                o_axi_en    <= '0';
                while(i_axi_rd_done = '1') loop
                    wait for 5 ns;
                end loop;
                if i_axi_px = 255 then
                    ref_read := ref_read + 1;
                end if;
                wait for 30 ns;
            end loop;
        end loop;        

        for i in 0 to 255 loop
            for j in 0 to 255 loop
                o_axi_en    <= '1';
                o_axi_x     <= to_unsigned(i, o_axi_x'length);
                o_axi_y     <= to_unsigned(j, o_axi_x'length);
                o_axi_s     <= to_unsigned(1, o_axi_x'length);
                o_axi_f     <= to_unsigned(0, o_axi_x'length);
                while(i_axi_rd_done = '0') loop
                    wait for 5 ns;
                end loop;
                wait for 10 ns;
                o_axi_en    <= '0';
                if i_axi_px = 255 then
                    ref_read := ref_read + 1;
                end if;
                wait for 30 ns;
            end loop;
        end loop;        

--    o_axi_we    <= '1';
--    o_axi_x     <= to_signed(0, o_axi_x'length);
--    o_axi_y     <= to_signed(0, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    o_axi_px    <= to_signed(255, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_x     <= to_signed(1, o_axi_x'length);
--    o_axi_y     <= to_signed(0, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    o_axi_px    <= to_signed(255, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_x     <= to_signed(0, o_axi_x'length);
--    o_axi_y     <= to_signed(1, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    o_axi_px    <= to_signed(255, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_x     <= to_signed(1, o_axi_x'length);
--    o_axi_y     <= to_signed(1, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    o_axi_px    <= to_signed(255, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_we    <= '0';
--    wait for 30 ns;
--    o_axi_x     <= to_signed(0, o_axi_x'length);
--    o_axi_y     <= to_signed(0, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_x     <= to_signed(1, o_axi_x'length);
--    o_axi_y     <= to_signed(0, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_x     <= to_signed(0, o_axi_x'length);
--    o_axi_y     <= to_signed(1, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
--    wait for 30 ns;
--    o_axi_x     <= to_signed(2, o_axi_x'length);
--    o_axi_y     <= to_signed(1, o_axi_x'length);
--    o_axi_s     <= to_signed(0, o_axi_x'length);
--    o_axi_f     <= to_signed(0, o_axi_x'length);
            
        wait for 10000000 us;
    end process;

end Behavioral;
