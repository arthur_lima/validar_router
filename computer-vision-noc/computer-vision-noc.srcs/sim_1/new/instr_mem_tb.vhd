----------------------------------------------------------------------------------
-- Company: Universidade de Brasília
-- Engineer: Bruno Almeida
-- 
-- Create Date: 06.02.2020 19:06:11
-- Design Name: 
-- Module Name: instr_mem_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity instr_mem_tb is
--  Port ( );
end instr_mem_tb;

architecture Behavioral of instr_mem_tb is
component memory is 
    generic(
            bit_width           : natural;
            memory_length       : natural;
            instruction_length  : natural
            );
    port(
        -- Axi IO
        i_axi_clk       : in STD_LOGIC;
        i_axi_we        : in STD_LOGIC;
        i_axi_addr      : in STD_LOGIC_VECTOR(10 downto 0);
        i_axi_data0     : in STD_LOGIC_VECTOR(31 downto 0);
        i_axi_data1     : in STD_LOGIC_VECTOR(31 downto 0);
        o_axi_imok      : inout STD_LOGIC;
        ----------------------------------------------------
        -- PE IO
        pc              : in STD_LOGIC_VECTOR(10 downto 0) := (others => '0');
        instruction     : out STD_LOGIC_VECTOR(instruction_length-1 downto 0) := (others => '0')
        );
end component memory;

    signal i_axi_clk        : STD_LOGIC;
    signal i_axi_we         : STD_LOGIC;
    signal i_axi_addr       : STD_LOGIC_VECTOR(10 downto 0);
    signal i_axi_data0      : STD_LOGIC_VECTOR(31 downto 0);
    signal i_axi_data1      : STD_LOGIC_VECTOR(31 downto 0);
    signal o_axi_imok       : STD_LOGIC;
    signal pc               : STD_LOGIC_VECTOR(10 downto 0);
    signal instruction      : STD_LOGIC_VECTOR(34-1 downto 0);

begin

    instr_mem : memory generic  map(
                                    bit_width           => 5,
                                    memory_length       => 8,
                                    instruction_length  => 34
                                    )
                         port   map(
                                    i_axi_clk   => i_axi_clk,
                                    i_axi_we    => i_axi_we,
                                    i_axi_addr  => i_axi_addr,
                                    i_axi_data0 => i_axi_data0,
                                    i_axi_data1 => i_axi_data1,
                                    o_axi_imok  => o_axi_imok,
                                    pc          => pc,
                                    instruction => instruction
                                    );

    process
    begin
        i_axi_clk <= '1';
        wait for 5 ns;
        i_axi_clk <= '0';
        wait for 5 ns;
    end process;

    process
    begin
        -- Setup programming mode
        i_axi_we    <= '1';
         
        -- Start programming
        i_axi_addr  <= std_logic_vector(to_unsigned(0, i_axi_addr'length));
        i_axi_data0 <= x"F0000000";
        i_axi_data1 <= x"C0000000";
        while o_axi_imok /= '1' loop
            wait for 1 ns;
        end loop;
        wait for 10 ns;
        i_axi_addr  <= std_logic_vector(to_unsigned(4, i_axi_addr'length));
        i_axi_data0 <= x"F0000001";
        i_axi_data1 <= x"C0000001";
        while o_axi_imok /= '1' loop
            wait for 1 ns;
        end loop;
        wait for 10 ns;
        i_axi_addr  <= std_logic_vector(to_unsigned(8, i_axi_addr'length));
        i_axi_data0 <= x"F0000002";
        i_axi_data1 <= x"C0000002";
        while o_axi_imok /= '1' loop
            wait for 1 ns;
        end loop;
        wait for 10 ns;
        i_axi_addr  <= std_logic_vector(to_unsigned(12, i_axi_addr'length));
        i_axi_data0 <= x"F0000003";
        i_axi_data1 <= x"C0000003";
        while o_axi_imok /= '1' loop
            wait for 1 ns;
        end loop;
        wait for 9 ns;
        
        -- End programming mode
        i_axi_we    <= '0';
        
        -- Start common operation
        pc          <= std_logic_vector(to_unsigned(4 * 2, pc'length));
        wait for 30 ns;
        pc          <= std_logic_vector(to_unsigned(4 * 1, pc'length));
        wait for 30 ns;
        pc          <= std_logic_vector(to_unsigned(4 * 5, pc'length));
        wait for 30 ns;
        pc          <= std_logic_vector(to_unsigned(4 * 0, pc'length));
        wait for 10000000 ns;
    end process;
    
    

end Behavioral;
