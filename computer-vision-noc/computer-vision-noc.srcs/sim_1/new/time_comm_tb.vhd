----------------------------------------------------------------------------------
-- Company: Universidade de Brasilia
-- Engineer: Bruno Almeida
-- 
-- Create Date: 01/30/2020 05:11:15 PM
-- Design Name: 
-- Module Name: tile_comm_test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tile_comm_tb is

end tile_comm_tb;

architecture Behavioral of tile_comm_tb is

-- general parameters

constant x_init : natural := 0;
constant y_init : natural := 0;
constant code_init : natural := 0;
constant file_name : string := "/home/bsilva/Documents/UnB/Projeto_Final/parts/sample_code/code_2_2.bin";

-- TODO: change x, y, s, f and px based on noc topology
constant noc_width : natural := 3; -- noc size
constant noc_height : natural := 3; -- noc size
constant px_true_size : natural := 8;   -- true pixel depth
constant pix_depth : natural := px_true_size + 1; -- must include +1 because of foward/backward 
constant img_width : natural := 6; -- resolution
constant img_height: natural := 6; -- resolution
constant tile_width  : natural := 2; -- tile resolution 
constant tile_height : natural := 2; -- tile resolution
constant n_steps     : natural := 6; -- steps in the image processing chain
constant n_frames    : natural := 5; -- frames needed by an algorithm

-- router parameters
constant subimg_width 	: natural := 2; --2**2; --4;
constant subimg_height	: natural := 2; --2**2; --4;
constant buffer_length  : natural := 6;

-- secondary parameters
constant memory_length : natural := 11; -- size of memory (2^program_length-1 downto to 0)
constant regfile_word : natural := 3;   -- word length in each register (2^regfile_word-1 downto 0)
                                        -- (word length)*(number of words) = bit width
                                        -- (2**regfile_word)*(2**(bit_width-regfile_word)) = 2**bit_width 
constant regfile_size : natural := 5;   -- size of register file (2^regfile_size-1 downto 0)
constant regfile_num : natural := 2**regfile_size;    -- total of registers inside regfile - 1
                                                        -- (for index purposes)
constant bit_width : natural := 5;  -- processor bit width (2^bit_width-1 downto 0)
                                    -- considering each register with the same width as bit_width

constant addr_size : natural := 17; -- address field size in bits

constant p_bit_width : natural := 6; -- P-type instruction field size

-- instruction set parameters (isf = instruction set field)
constant isf_opcode : natural := 4;
constant isf_x : natural := img_width; -- temporary 
constant isf_y : natural := img_height; -- temporary
constant isf_s : natural := n_steps; -- temporary
constant isf_f : natural := n_frames; -- temporary
constant isf_px: natural := pix_depth; -- px value range = 0 to 255
constant isf_rs : natural := regfile_size;
constant isf_rt : natural := regfile_size;
constant isf_rd : natural := regfile_size;
constant isf_dest : natural := regfile_size;

-- TODO: use if statement to choose instruction length based on P-type instru. size and 2**bit_width
constant instruction_length : natural := isf_opcode+isf_x+isf_y+isf_s+isf_f+isf_px; -- bits per instruction

-- opcodes
constant NOP :   std_logic_vector(isf_opcode-1 downto 0):= "0000";
constant GPX :   std_logic_vector(isf_opcode-1 downto 0):= "0001";
constant SPX :   std_logic_vector(isf_opcode-1 downto 0):= "0010";
constant ADD :   std_logic_vector(isf_opcode-1 downto 0):= "0011";
constant SUB :   std_logic_vector(isf_opcode-1 downto 0):= "0100";
constant MUL :   std_logic_vector(isf_opcode-1 downto 0):= "0101";
constant DIV :   std_logic_vector(isf_opcode-1 downto 0):= "0110";
constant AND1 :  std_logic_vector(isf_opcode-1 downto 0):= "0111";
constant OR1 :   std_logic_vector(isf_opcode-1 downto 0):= "1000";
constant XOR1 :  std_logic_vector(isf_opcode-1 downto 0):= "1001";
constant NOT1 :  std_logic_vector(isf_opcode-1 downto 0):= "1010";
constant BGT :   std_logic_vector(isf_opcode-1 downto 0):= "1011";
constant BST :   std_logic_vector(isf_opcode-1 downto 0):= "1100";
constant BEQ :   std_logic_vector(isf_opcode-1 downto 0):= "1101";
constant JMP :   std_logic_vector(isf_opcode-1 downto 0):= "1110";
constant ENDPGR :std_logic_vector(isf_opcode-1 downto 0):= "1111";

constant code_2_2 : string := "/home/bsilva/Documents/UnB/Projeto_Final/parts/sample_code/code_2_2.bin";

    signal clk : STD_LOGIC;
    signal reset: STD_LOGIC;
    signal done : STD_LOGIC;     

    ----------------------------------------------------
    -- port to write pixel values from the AXI
    signal i_axi_clk   : std_logic;
    signal i_axi_we    : std_logic;
    signal i_axi_x     : signed(31 downto 0);
    signal i_axi_y     : signed(31 downto 0);
    signal i_axi_s     : signed(31 downto 0);
    signal i_axi_f     : signed(31 downto 0);
    signal i_axi_px    : signed(31 downto 0);
    signal o_axi_px    : signed(31 downto 0);


    ------------------------------------------------------------
    -- North connections
signal    i_N_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    i_N_x_dest : std_logic_vector(img_width-1 downto 0);
signal    i_N_y_dest : std_logic_vector(img_height-1 downto 0);
signal    i_N_step   : std_logic_vector(n_steps-1 downto 0);
signal    i_N_frame  : std_logic_vector(n_frames-1 downto 0);
signal    i_N_x_orig : std_logic_vector(img_width-1 downto 0);
signal    i_N_y_orig : std_logic_vector(img_height-1 downto 0);
signal    i_N_fb     : std_logic; -- message forward/backward
signal    i_N_req    : std_logic; -- message request

signal    i_N_ack    : std_logic; -- message acknowledge  

signal    t_N_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    t_N_x_dest : std_logic_vector(img_width-1 downto 0);
signal    t_N_y_dest : std_logic_vector(img_height-1 downto 0);
signal    t_N_step   : std_logic_vector(n_steps-1 downto 0);
signal    t_N_frame  : std_logic_vector(n_frames-1 downto 0);
signal    t_N_x_orig : std_logic_vector(img_width-1 downto 0);
signal    t_N_y_orig : std_logic_vector(img_height-1 downto 0);
signal    t_N_fb     : std_logic; -- message forward/backward
signal    t_N_req    : std_logic; -- message request

signal    t_N_ack    :  std_logic; -- message acknowledge 
          -------------------------------------------------------------
          -- South connections
signal    i_S_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    i_S_x_dest : std_logic_vector(img_width-1 downto 0);
signal    i_S_y_dest : std_logic_vector(img_height-1 downto 0);
signal    i_S_step   : std_logic_vector(n_steps-1 downto 0);
signal    i_S_frame  : std_logic_vector(n_frames-1 downto 0);
signal    i_S_x_orig : std_logic_vector(img_width-1 downto 0);
signal    i_S_y_orig : std_logic_vector(img_height-1 downto 0);
signal    i_S_fb     : std_logic; -- message forward/backward
signal    i_S_req    : std_logic; -- message request

signal    i_S_ack    : std_logic; -- message acknowledge  

signal    t_S_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    t_S_x_dest : std_logic_vector(img_width-1 downto 0);
signal    t_S_y_dest : std_logic_vector(img_height-1 downto 0);
signal    t_S_step   : std_logic_vector(n_steps-1 downto 0);
signal    t_S_frame  : std_logic_vector(n_frames-1 downto 0);
signal    t_S_x_orig : std_logic_vector(img_width-1 downto 0);
signal    t_S_y_orig : std_logic_vector(img_height-1 downto 0);
signal    t_S_fb     : std_logic; -- message forward/backward
signal    t_S_req    : std_logic; -- message request

signal    t_S_ack    : std_logic; -- message acknowledge        
          ---------------------------------------------------------------
          -- East connections
signal    i_E_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    i_E_x_dest : std_logic_vector(img_width-1 downto 0);
signal    i_E_y_dest : std_logic_vector(img_height-1 downto 0);
signal    i_E_step   : std_logic_vector(n_steps-1 downto 0);
signal    i_E_frame  : std_logic_vector(n_frames-1 downto 0);
signal    i_E_x_orig : std_logic_vector(img_width-1 downto 0);
signal    i_E_y_orig : std_logic_vector(img_height-1 downto 0);
signal    i_E_fb     : std_logic; -- message forward/backward
signal    i_E_req    : std_logic; -- message request

signal    i_E_ack    : std_logic; -- message acknowledge  

signal    t_E_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    t_E_x_dest : std_logic_vector(img_width-1 downto 0);
signal    t_E_y_dest : std_logic_vector(img_height-1 downto 0);
signal    t_E_step   : std_logic_vector(n_steps-1 downto 0);
signal    t_E_frame  : std_logic_vector(n_frames-1 downto 0);
signal    t_E_x_orig : std_logic_vector(img_width-1 downto 0);
signal    t_E_y_orig : std_logic_vector(img_height-1 downto 0);
signal    t_E_fb     : std_logic; -- message forward/backward
signal    t_E_req    : std_logic; -- message request

signal    t_E_ack    : std_logic; -- message acknowledge        
          -----------------------------------------------------------------
          -- West
signal    i_W_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    i_W_x_dest : std_logic_vector(img_width-1 downto 0);
signal    i_W_y_dest : std_logic_vector(img_height-1 downto 0);
signal    i_W_step   : std_logic_vector(n_steps-1 downto 0);
signal    i_W_frame  : std_logic_vector(n_frames-1 downto 0);
signal    i_W_x_orig : std_logic_vector(img_width-1 downto 0);
signal    i_W_y_orig : std_logic_vector(img_height-1 downto 0);
signal    i_W_fb     : std_logic; -- message forward/backward
signal    i_W_req    : std_logic; -- message request

signal    i_W_ack    : std_logic; -- message acknowledge  

signal    t_W_pixel  : std_logic_vector(pix_depth-1 downto 0);
signal    t_W_x_dest : std_logic_vector(img_width-1 downto 0);
signal    t_W_y_dest : std_logic_vector(img_height-1 downto 0);
signal    t_W_step   : std_logic_vector(n_steps-1 downto 0);
signal    t_W_frame  : std_logic_vector(n_frames-1 downto 0);
signal    t_W_x_orig : std_logic_vector(img_width-1 downto 0);
signal    t_W_y_orig : std_logic_vector(img_height-1 downto 0);
signal    t_W_fb     : std_logic; -- message forward/backward
signal    t_W_req    : std_logic; -- message request

signal    t_W_ack    : std_logic; -- message acknowledge     

component tile
generic(
    x_init              : integer := 0;
    y_init              : integer := 0;
    img_width           : natural;
    img_height          : natural;
    n_frames            : natural;
    n_steps             : natural;
    pix_depth           : natural;
    subimg_width        : natural;
    subimg_height       : natural;
    buffer_length       : natural;
    bit_width           : natural;
    memory_length       : natural;
    instruction_length  : natural;
    code_init           : natural;
    file_name           : string;
    isf_opcode          : natural;
    regfile_size        : natural;
    isf_x               : natural;
    isf_y               : natural;
    isf_s               : natural;
    isf_f               : natural;
    isf_px              : natural;
    NOP                 : std_logic_vector(4-1 downto 0);
    GPX                 : std_logic_vector(4-1 downto 0);
    SPX                 : std_logic_vector(4-1 downto 0);
    ADD                 : std_logic_vector(4-1 downto 0);
    SUB                 : std_logic_vector(4-1 downto 0);
    MUL                 : std_logic_vector(4-1 downto 0);
    DIV                 : std_logic_vector(4-1 downto 0);
    AND1                : std_logic_vector(4-1 downto 0);
    OR1                 : std_logic_vector(4-1 downto 0);
    XOR1                : std_logic_vector(4-1 downto 0);
    NOT1                : std_logic_vector(4-1 downto 0);
    BGT                 : std_logic_vector(4-1 downto 0);
    BST                 : std_logic_vector(4-1 downto 0);
    BEQ                 : std_logic_vector(4-1 downto 0);
    JMP                 : std_logic_vector(4-1 downto 0);
    ENDPGR              : std_logic_vector(4-1 downto 0);
    addr_size           : natural
    );

port(
    clk : in STD_LOGIC;
    reset: in STD_LOGIC;
    done : out STD_LOGIC;     
    ------------------------------------------------------------
    -- North connections
    i_N_pixel  : out std_logic_vector(pix_depth-1 downto 0);
    i_N_x_dest : out std_logic_vector(img_width-1 downto 0);
    i_N_y_dest : out std_logic_vector(img_height-1 downto 0);
    i_N_step   : out std_logic_vector(n_steps-1 downto 0);
    i_N_frame  : out std_logic_vector(n_frames-1 downto 0);
    i_N_x_orig : out std_logic_vector(img_width-1 downto 0);
    i_N_y_orig : out std_logic_vector(img_height-1 downto 0);
    i_N_fb     : out std_logic; -- message forward/backward
    i_N_req    : inout std_logic; -- message request
   --i_N_busy   : in std_logic; -- router is busy
    i_N_ack    : in std_logic; -- message acknowledge  
     
    t_N_pixel  : in std_logic_vector(pix_depth-1 downto 0);
    t_N_x_dest : in std_logic_vector(img_width-1 downto 0);
    t_N_y_dest : in std_logic_vector(img_height-1 downto 0);
    t_N_step   : in std_logic_vector(n_steps-1 downto 0);
    t_N_frame  : in std_logic_vector(n_frames-1 downto 0);
    t_N_x_orig : in std_logic_vector(img_width-1 downto 0);
    t_N_y_orig : in std_logic_vector(img_height-1 downto 0);
    t_N_fb     : in std_logic; -- message forward/backward
    t_N_req    : in std_logic; -- message request
    --t_N_busy   : out std_logic; -- router is busy
    t_N_ack    : out std_logic; -- message acknowledge 
    -------------------------------------------------------------
    -- South connections
    i_S_pixel  : out std_logic_vector(pix_depth-1 downto 0);
    i_S_x_dest : out std_logic_vector(img_width-1 downto 0);
    i_S_y_dest : out std_logic_vector(img_height-1 downto 0);
    i_S_step   : out std_logic_vector(n_steps-1 downto 0);
    i_S_frame  : out std_logic_vector(n_frames-1 downto 0);
    i_S_x_orig : out std_logic_vector(img_width-1 downto 0);
    i_S_y_orig : out std_logic_vector(img_height-1 downto 0);
    i_S_fb     : out std_logic; -- message forward/backward
    i_S_req    : inout std_logic; -- message request
    --i_S_busy   : in std_logic; -- router is busy
    i_S_ack    : in std_logic; -- message acknowledge  
     
    t_S_pixel  : in std_logic_vector(pix_depth-1 downto 0);
    t_S_x_dest : in std_logic_vector(img_width-1 downto 0);
    t_S_y_dest : in std_logic_vector(img_height-1 downto 0);
    t_S_step   : in std_logic_vector(n_steps-1 downto 0);
    t_S_frame  : in std_logic_vector(n_frames-1 downto 0);
    t_S_x_orig : in std_logic_vector(img_width-1 downto 0);
    t_S_y_orig : in std_logic_vector(img_height-1 downto 0);
    t_S_fb     : in std_logic; -- message forward/backward
    t_S_req    : in std_logic; -- message request
    --t_S_busy   : out std_logic; -- router is busy
    t_S_ack    : out std_logic; -- message acknowledge        
    ---------------------------------------------------------------
    -- East connections
    i_E_pixel  : out std_logic_vector(pix_depth-1 downto 0);
    i_E_x_dest : out std_logic_vector(img_width-1 downto 0);
    i_E_y_dest : out std_logic_vector(img_height-1 downto 0);
    i_E_step   : out std_logic_vector(n_steps-1 downto 0);
    i_E_frame  : out std_logic_vector(n_frames-1 downto 0);
    i_E_x_orig : out std_logic_vector(img_width-1 downto 0);
    i_E_y_orig : out std_logic_vector(img_height-1 downto 0);
    i_E_fb     : out std_logic; -- message forward/backward
    i_E_req    : inout std_logic; -- message request
    --i_E_busy   : in std_logic; -- router is busy
    i_E_ack    : in std_logic; -- message acknowledge  
    
    t_E_pixel  : in std_logic_vector(pix_depth-1 downto 0);
    t_E_x_dest : in std_logic_vector(img_width-1 downto 0);
    t_E_y_dest : in std_logic_vector(img_height-1 downto 0);
    t_E_step   : in std_logic_vector(n_steps-1 downto 0);
    t_E_frame  : in std_logic_vector(n_frames-1 downto 0);
    t_E_x_orig : in std_logic_vector(img_width-1 downto 0);
    t_E_y_orig : in std_logic_vector(img_height-1 downto 0);
    t_E_fb     : in std_logic; -- message forward/backward
    t_E_req    : in std_logic; -- message request
    --t_E_busy   : out std_logic; -- router is busy
    t_E_ack    : out std_logic; -- message acknowledge        
    -----------------------------------------------------------------
    -- West
    i_W_pixel  : out std_logic_vector(pix_depth-1 downto 0);
    i_W_x_dest : out std_logic_vector(img_width-1 downto 0);
    i_W_y_dest : out std_logic_vector(img_height-1 downto 0);
    i_W_step   : out std_logic_vector(n_steps-1 downto 0);
    i_W_frame  : out std_logic_vector(n_frames-1 downto 0);
    i_W_x_orig : out std_logic_vector(img_width-1 downto 0);
    i_W_y_orig : out std_logic_vector(img_height-1 downto 0);
    i_W_fb     : out std_logic; -- message forward/backward
    i_W_req    : inout std_logic; -- message request
    --i_W_busy   : in std_logic; -- router is busy
    i_W_ack    : in std_logic; -- message acknowledge  
     
    t_W_pixel  : in std_logic_vector(pix_depth-1 downto 0);
    t_W_x_dest : in std_logic_vector(img_width-1 downto 0);
    t_W_y_dest : in std_logic_vector(img_height-1 downto 0);
    t_W_step   : in std_logic_vector(n_steps-1 downto 0);
    t_W_frame  : in std_logic_vector(n_frames-1 downto 0);
    t_W_x_orig : in std_logic_vector(img_width-1 downto 0);
    t_W_y_orig : in std_logic_vector(img_height-1 downto 0);
    t_W_fb     : in std_logic; -- message forward/backward
    t_W_req    : in std_logic; -- message request
    --t_W_busy   : out std_logic; -- router is busy
    t_W_ack    : out std_logic; -- message acknowledge     
    
    ----------------------------------------------------
    -- port to write pixel values from the AXI
    i_axi_clk   : in std_logic;
    i_axi_we    : in std_logic;

    i_axi_x     : in signed(31 downto 0);
    i_axi_y     : in signed(31 downto 0);
    i_axi_s     : in signed(31 downto 0);
    i_axi_f     : in signed(31 downto 0);
    i_axi_px    : in signed(31 downto 0);

    o_axi_px    : out signed(31 downto 0)
);
end component tile;

begin

    tile_inst : tile generic map(
                                x_init              => x_init            ,
                                y_init              => y_init            ,
                                img_width           => img_width         ,
                                img_height          => img_height        ,
                                n_frames            => n_frames          ,
                                n_steps             => n_steps           ,
                                pix_depth           => pix_depth         ,
                                subimg_width        => subimg_width      ,
                                subimg_height       => subimg_height     ,
                                buffer_length       => buffer_length     ,
                                bit_width           => bit_width         ,
                                memory_length       => memory_length     ,
                                instruction_length  => instruction_length,
                                code_init           => code_init         ,
                                file_name           => file_name         ,
                                isf_opcode          => isf_opcode        ,
                                regfile_size        => regfile_size      ,
                                isf_x               => isf_x             ,
                                isf_y               => isf_y             ,
                                isf_s               => isf_s             ,
                                isf_f               => isf_f             ,
                                isf_px              => isf_px            ,
                                NOP                 => NOP               ,
                                GPX                 => GPX               ,
                                SPX                 => SPX               ,
                                ADD                 => ADD               ,
                                SUB                 => SUB               ,
                                MUL                 => MUL               ,
                                DIV                 => DIV               ,
                                AND1                => AND1              ,
                                OR1                 => OR1               ,
                                XOR1                => XOR1              ,
                                NOT1                => NOT1              ,
                                BGT                 => BGT               ,
                                BST                 => BST               ,
                                BEQ                 => BEQ               ,
                                JMP                 => JMP               ,
                                ENDPGR              => ENDPGR            ,
                                addr_size           => addr_size         
                                )
                       port map(
                                clk         => clk,    
                                reset       => reset,     
                                done        => done,
                                i_N_pixel   => i_N_pixel  , 
                                i_N_x_dest  => i_N_x_dest , 
                                i_N_y_dest  => i_N_y_dest , 
                                i_N_step    => i_N_step   , 
                                i_N_frame   => i_N_frame  , 
                                i_N_x_orig  => i_N_x_orig , 
                                i_N_y_orig  => i_N_y_orig , 
                                i_N_fb      => i_N_fb     , 
                                i_N_req     => i_N_req    , 
                                i_N_ack     => i_N_ack    , 
                                t_N_pixel   => t_N_pixel  , 
                                t_N_x_dest  => t_N_x_dest , 
                                t_N_y_dest  => t_N_y_dest , 
                                t_N_step    => t_N_step   , 
                                t_N_frame   => t_N_frame  , 
                                t_N_x_orig  => t_N_x_orig , 
                                t_N_y_orig  => t_N_y_orig , 
                                t_N_fb      => t_N_fb     , 
                                t_N_req     => t_N_req    , 
                                t_N_ack     => t_N_ack    , 
                                i_S_pixel   => i_S_pixel  , 
                                i_S_x_dest  => i_S_x_dest , 
                                i_S_y_dest  => i_S_y_dest , 
                                i_S_step    => i_S_step   , 
                                i_S_frame   => i_S_frame  , 
                                i_S_x_orig  => i_S_x_orig , 
                                i_S_y_orig  => i_S_y_orig , 
                                i_S_fb      => i_S_fb     , 
                                i_S_req     => i_S_req    , 
                                i_S_ack     => i_S_ack    , 
                                t_S_pixel   => t_S_pixel  , 
                                t_S_x_dest  => t_S_x_dest , 
                                t_S_y_dest  => t_S_y_dest , 
                                t_S_step    => t_S_step   , 
                                t_S_frame   => t_S_frame  , 
                                t_S_x_orig  => t_S_x_orig , 
                                t_S_y_orig  => t_S_y_orig , 
                                t_S_fb      => t_S_fb     , 
                                t_S_req     => t_S_req    , 
                                t_S_ack     => t_S_ack    , 
                                i_E_pixel   => i_E_pixel  , 
                                i_E_x_dest  => i_E_x_dest , 
                                i_E_y_dest  => i_E_y_dest , 
                                i_E_step    => i_E_step   , 
                                i_E_frame   => i_E_frame  , 
                                i_E_x_orig  => i_E_x_orig , 
                                i_E_y_orig  => i_E_y_orig , 
                                i_E_fb      => i_E_fb     , 
                                i_E_req     => i_E_req    , 
                                i_E_ack     => i_E_ack    , 
                                t_E_pixel   => t_E_pixel  , 
                                t_E_x_dest  => t_E_x_dest , 
                                t_E_y_dest  => t_E_y_dest , 
                                t_E_step    => t_E_step   , 
                                t_E_frame   => t_E_frame  , 
                                t_E_x_orig  => t_E_x_orig , 
                                t_E_y_orig  => t_E_y_orig , 
                                t_E_fb      => t_E_fb     , 
                                t_E_req     => t_E_req    , 
                                t_E_ack     => t_E_ack    , 
                                i_W_pixel   => i_W_pixel  , 
                                i_W_x_dest  => i_W_x_dest , 
                                i_W_y_dest  => i_W_y_dest , 
                                i_W_step    => i_W_step   , 
                                i_W_frame   => i_W_frame  , 
                                i_W_x_orig  => i_W_x_orig , 
                                i_W_y_orig  => i_W_y_orig , 
                                i_W_fb      => i_W_fb     , 
                                i_W_req     => i_W_req    , 
                                i_W_ack     => i_W_ack    , 
                                t_W_pixel   => t_W_pixel  , 
                                t_W_x_dest  => t_W_x_dest , 
                                t_W_y_dest  => t_W_y_dest , 
                                t_W_step    => t_W_step   , 
                                t_W_frame   => t_W_frame  , 
                                t_W_x_orig  => t_W_x_orig , 
                                t_W_y_orig  => t_W_y_orig , 
                                t_W_fb      => t_W_fb     , 
                                t_W_req     => t_W_req    , 
                                t_W_ack     => t_W_ack    , 
                                i_axi_clk   => i_axi_clk  , 
                                i_axi_we    => i_axi_we   , 
                                i_axi_x     => i_axi_x    , 
                                i_axi_y     => i_axi_y    , 
                                i_axi_s     => i_axi_s    , 
                                i_axi_f     => i_axi_f    , 
                                i_axi_px    => i_axi_px   , 
                                o_axi_px    => o_axi_px    
                                );

process
begin
    clk <= '0';
    wait for 10 ns;
    clk <= '1';
    wait for 10 ns;
end process;

end Behavioral;
