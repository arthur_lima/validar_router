----------------------------------------------------------------------------------
-- Company: Universidade de Brasília
-- Engineer: Bruno Almeida
-- 
-- Create Date: 02/03/2020 02:42:27 PM
-- Design Name: 
-- Module Name: manycore_2x2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

---------------------------------------------------------------------- 
-- ManyCore auto-generated code.                                      
-- Pixel Length : 9                               
-- Image Width  : 256                               
-- Image Height : 256                               
-- Region Width : 128                               
-- Region Height: 128                               
-- Array of 2 x 2                           
----------------------------------------------------------------------
library ieee;                                                         
use ieee.std_logic_1164.all;                                          
use ieee.numeric_std.all;                                             
use work.parameters.all;                                              
                                                                      
entity manycore_2x2 is
port(
    clk                 : in std_logic; -- general clock for processing
    gen_port_in_0_0     : in std_logic_vector(1+1+1+32*5-1 downto 0);
    gen_port_out_0_0    : out std_logic_vector(1+1+32-1 downto 0);
    gen_port_in_0_2     : in std_logic_vector(1+1+1+32*5-1 downto 0);
    gen_port_out_0_2    : out std_logic_vector(1+1+32-1 downto 0);
    gen_port_in_2_0     : in std_logic_vector(1+1+1+32*5-1 downto 0);
    gen_port_out_2_0    : out std_logic_vector(1+1+32-1 downto 0);
    gen_port_in_2_2     : in std_logic_vector(1+1+1+32*5-1 downto 0);
    gen_port_out_2_2    : out std_logic_vector(1+1+32-1 downto 0)
);
end manycore_2x2;
                                                                      
architecture behavioral of manycore_2x2 is                                

    component tile
    generic(
        x_init              : integer := 0;
        y_init              : integer := 0;
        img_width           : natural;
        img_height          : natural;
        tile_width          : natural;
        tile_height         : natural;
        n_frames            : natural;
        n_steps             : natural;
        pix_depth           : natural;
        subimg_width        : natural;
        subimg_height       : natural;
        buffer_length       : natural;
        bit_width           : natural;
        memory_length       : natural;
        instruction_length  : natural;
        isf_opcode          : natural;
        regfile_size        : natural;
        isf_x               : natural;
        isf_y               : natural;
        isf_s               : natural;
        isf_f               : natural;
        isf_px              : natural;
        NOP                 : std_logic_vector(4-1 downto 0);
        GPX                 : std_logic_vector(4-1 downto 0);
        SPX                 : std_logic_vector(4-1 downto 0);
        ADD                 : std_logic_vector(4-1 downto 0);
        SUB                 : std_logic_vector(4-1 downto 0);
        MUL                 : std_logic_vector(4-1 downto 0);
        DIV                 : std_logic_vector(4-1 downto 0);
        AND1                : std_logic_vector(4-1 downto 0);
        OR1                 : std_logic_vector(4-1 downto 0);
        XOR1                : std_logic_vector(4-1 downto 0);
        NOT1                : std_logic_vector(4-1 downto 0);
        BGT                 : std_logic_vector(4-1 downto 0);
        BST                 : std_logic_vector(4-1 downto 0);
        BEQ                 : std_logic_vector(4-1 downto 0);
        JMP                 : std_logic_vector(4-1 downto 0);
        ENDPGR              : std_logic_vector(4-1 downto 0);
        addr_size           : natural
        );
    
    port(
        clk : in STD_LOGIC;
        ----------------------------------------------------
        -- port to write pixel values from the AXI
        gen_port_in     : in std_logic_vector(1+1+1+32*5-1 downto 0);
        gen_port_out    : out std_logic_vector(1+1+32-1 downto 0);
        ------------------------------------------------------------
        -- North connections
        i_N_pixel  : out std_logic_vector(pix_depth-1 downto 0);
        i_N_x_dest : out std_logic_vector(img_width-1 downto 0);
        i_N_y_dest : out std_logic_vector(img_height-1 downto 0);
        i_N_step   : out std_logic_vector(n_steps-1 downto 0);
        i_N_frame  : out std_logic_vector(n_frames-1 downto 0);
        i_N_x_orig : out std_logic_vector(img_width-1 downto 0);
        i_N_y_orig : out std_logic_vector(img_height-1 downto 0);
        i_N_fb     : out std_logic; -- message forward/backward
        i_N_req    : inout std_logic; -- message request
       --i_N_busy   : in std_logic; -- router is busy
        i_N_ack    : in std_logic; -- message acknowledge  
         
        t_N_pixel  : in std_logic_vector(pix_depth-1 downto 0);
        t_N_x_dest : in std_logic_vector(img_width-1 downto 0);
        t_N_y_dest : in std_logic_vector(img_height-1 downto 0);
        t_N_step   : in std_logic_vector(n_steps-1 downto 0);
        t_N_frame  : in std_logic_vector(n_frames-1 downto 0);
        t_N_x_orig : in std_logic_vector(img_width-1 downto 0);
        t_N_y_orig : in std_logic_vector(img_height-1 downto 0);
        t_N_fb     : in std_logic; -- message forward/backward
        t_N_req    : in std_logic; -- message request
        --t_N_busy   : out std_logic; -- router is busy
        t_N_ack    : out std_logic; -- message acknowledge 
        -------------------------------------------------------------
        -- South connections
        i_S_pixel  : out std_logic_vector(pix_depth-1 downto 0);
        i_S_x_dest : out std_logic_vector(img_width-1 downto 0);
        i_S_y_dest : out std_logic_vector(img_height-1 downto 0);
        i_S_step   : out std_logic_vector(n_steps-1 downto 0);
        i_S_frame  : out std_logic_vector(n_frames-1 downto 0);
        i_S_x_orig : out std_logic_vector(img_width-1 downto 0);
        i_S_y_orig : out std_logic_vector(img_height-1 downto 0);
        i_S_fb     : out std_logic; -- message forward/backward
        i_S_req    : inout std_logic; -- message request
        --i_S_busy   : in std_logic; -- router is busy
        i_S_ack    : in std_logic; -- message acknowledge  
         
        t_S_pixel  : in std_logic_vector(pix_depth-1 downto 0);
        t_S_x_dest : in std_logic_vector(img_width-1 downto 0);
        t_S_y_dest : in std_logic_vector(img_height-1 downto 0);
        t_S_step   : in std_logic_vector(n_steps-1 downto 0);
        t_S_frame  : in std_logic_vector(n_frames-1 downto 0);
        t_S_x_orig : in std_logic_vector(img_width-1 downto 0);
        t_S_y_orig : in std_logic_vector(img_height-1 downto 0);
        t_S_fb     : in std_logic; -- message forward/backward
        t_S_req    : in std_logic; -- message request
        --t_S_busy   : out std_logic; -- router is busy
        t_S_ack    : out std_logic; -- message acknowledge        
        ---------------------------------------------------------------
        -- East connections
        i_E_pixel  : out std_logic_vector(pix_depth-1 downto 0);
        i_E_x_dest : out std_logic_vector(img_width-1 downto 0);
        i_E_y_dest : out std_logic_vector(img_height-1 downto 0);
        i_E_step   : out std_logic_vector(n_steps-1 downto 0);
        i_E_frame  : out std_logic_vector(n_frames-1 downto 0);
        i_E_x_orig : out std_logic_vector(img_width-1 downto 0);
        i_E_y_orig : out std_logic_vector(img_height-1 downto 0);
        i_E_fb     : out std_logic; -- message forward/backward
        i_E_req    : inout std_logic; -- message request
        --i_E_busy   : in std_logic; -- router is busy
        i_E_ack    : in std_logic; -- message acknowledge  
        
        t_E_pixel  : in std_logic_vector(pix_depth-1 downto 0);
        t_E_x_dest : in std_logic_vector(img_width-1 downto 0);
        t_E_y_dest : in std_logic_vector(img_height-1 downto 0);
        t_E_step   : in std_logic_vector(n_steps-1 downto 0);
        t_E_frame  : in std_logic_vector(n_frames-1 downto 0);
        t_E_x_orig : in std_logic_vector(img_width-1 downto 0);
        t_E_y_orig : in std_logic_vector(img_height-1 downto 0);
        t_E_fb     : in std_logic; -- message forward/backward
        t_E_req    : in std_logic; -- message request
        --t_E_busy   : out std_logic; -- router is busy
        t_E_ack    : out std_logic; -- message acknowledge        
        -----------------------------------------------------------------
        -- West
        i_W_pixel  : out std_logic_vector(pix_depth-1 downto 0);
        i_W_x_dest : out std_logic_vector(img_width-1 downto 0);
        i_W_y_dest : out std_logic_vector(img_height-1 downto 0);
        i_W_step   : out std_logic_vector(n_steps-1 downto 0);
        i_W_frame  : out std_logic_vector(n_frames-1 downto 0);
        i_W_x_orig : out std_logic_vector(img_width-1 downto 0);
        i_W_y_orig : out std_logic_vector(img_height-1 downto 0);
        i_W_fb     : out std_logic; -- message forward/backward
        i_W_req    : inout std_logic; -- message request
        --i_W_busy   : in std_logic; -- router is busy
        i_W_ack    : in std_logic; -- message acknowledge  
         
        t_W_pixel  : in std_logic_vector(pix_depth-1 downto 0);
        t_W_x_dest : in std_logic_vector(img_width-1 downto 0);
        t_W_y_dest : in std_logic_vector(img_height-1 downto 0);
        t_W_step   : in std_logic_vector(n_steps-1 downto 0);
        t_W_frame  : in std_logic_vector(n_frames-1 downto 0);
        t_W_x_orig : in std_logic_vector(img_width-1 downto 0);
        t_W_y_orig : in std_logic_vector(img_height-1 downto 0);
        t_W_fb     : in std_logic; -- message forward/backward
        t_W_req    : in std_logic; -- message request
        --t_W_busy   : out std_logic; -- router is busy
        t_W_ack    : out std_logic -- message acknowledge     
    );
    end component tile;
        
        -- TODO: change x, y, s, f and px based on noc topology
        constant noc_width : natural := 2; -- noc size
        constant noc_height : natural := 2; -- noc size
        constant px_true_size : natural := 9;   -- true pixel depth
        constant pix_depth : natural := px_true_size + 1; -- must include +1 because of foward/backward 
        constant img_width : natural := 8; -- resolution
        constant img_height: natural := 8; -- resolution
        constant tile_width  : natural := 7; -- tile resolution 
        constant tile_height : natural := 7; -- tile resolution
        constant n_steps     : natural := 4; -- steps in the image processing chain
        constant n_frames    : natural := 1; -- frames needed by an algorithm
        
        -- router parameters
        constant subimg_width 	: natural := 2**7; --2**2; --4;
        constant subimg_height	: natural := 2**7; --2**2; --4;
        constant buffer_length  : natural := 6;
        
        -- secondary parameters
        constant memory_length : natural := 11; -- size of memory (2^program_length-1 downto to 0)
        constant regfile_word : natural := 3;   -- word length in each register (2^regfile_word-1 downto 0)
                                                -- (word length)*(number of words) = bit width
                                                -- (2**regfile_word)*(2**(bit_width-regfile_word)) = 2**bit_width 
        constant regfile_size : natural := 7;   -- size of register file (2^regfile_size-1 downto 0)
        constant regfile_num : natural := 2**regfile_size;    -- total of registers inside regfile - 1
                                                                -- (for index purposes)
        constant bit_width : natural := 5;  -- processor bit width (2^bit_width-1 downto 0)
                                            -- considering each register with the same width as bit_width
        
        constant addr_size : natural := 17; -- address field size in bits
        
        -- instruction set parameters (isf = instruction set field)
        constant isf_opcode : natural := 4;
        constant isf_x : natural := regfile_size; -- temporary 
        constant isf_y : natural := regfile_size; -- temporary
        constant isf_s : natural := regfile_size; -- temporary
        constant isf_f : natural := regfile_size; -- temporary
        constant isf_px: natural := regfile_size; -- px value range = 0 to 255
        constant isf_rs : natural := regfile_size;
        constant isf_rt : natural := regfile_size;
        constant isf_rd : natural := regfile_size;
        constant isf_dest : natural := regfile_size;
        
        -- TODO: use if statement to choose instruction length based on P-type instru. size and 2**bit_width
        constant instruction_length : natural := isf_opcode+isf_x+isf_y+isf_s+isf_f+isf_px; -- bits per instruction
        
        -- opcodes
        constant NOP :   std_logic_vector(isf_opcode-1 downto 0):= "0000";
        constant GPX :   std_logic_vector(isf_opcode-1 downto 0):= "0001";
        constant SPX :   std_logic_vector(isf_opcode-1 downto 0):= "0010";
        constant ADD :   std_logic_vector(isf_opcode-1 downto 0):= "0011";
        constant SUB :   std_logic_vector(isf_opcode-1 downto 0):= "0100";
        constant MUL :   std_logic_vector(isf_opcode-1 downto 0):= "0101";
        constant DIV :   std_logic_vector(isf_opcode-1 downto 0):= "0110";
        constant AND1 :  std_logic_vector(isf_opcode-1 downto 0):= "0111";
        constant OR1 :   std_logic_vector(isf_opcode-1 downto 0):= "1000";
        constant XOR1 :  std_logic_vector(isf_opcode-1 downto 0):= "1001";
        constant NOT1 :  std_logic_vector(isf_opcode-1 downto 0):= "1010";
        constant BGT :   std_logic_vector(isf_opcode-1 downto 0):= "1011";
        constant BST :   std_logic_vector(isf_opcode-1 downto 0):= "1100";
        constant BEQ :   std_logic_vector(isf_opcode-1 downto 0):= "1101";
        constant JMP :   std_logic_vector(isf_opcode-1 downto 0):= "1110";
        constant ENDPGR :std_logic_vector(isf_opcode-1 downto 0):= "1111";       
   
   -- Signals for Tile_0_0;                                                                                 
       -- connections to North                                                        
       signal Tile_0_0_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_N_fb     : std_logic;                                
       signal Tile_0_0_i_N_req    : std_logic;                                
       signal Tile_0_0_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_0_0_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_S_fb     : std_logic;                                
       signal Tile_0_0_i_S_req    : std_logic;                                
       signal Tile_0_0_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_0_0_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_E_fb     : std_logic;                                
       signal Tile_0_0_i_E_req    : std_logic;                                
       signal Tile_0_0_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_0_0_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_W_fb     : std_logic;                                
       signal Tile_0_0_i_W_req    : std_logic;                                
       signal Tile_0_0_i_W_ack    : std_logic;                                
   -- Signals for Tile_0_2;                                                   
       -- connections to North                                                        
       signal Tile_0_2_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_N_fb     : std_logic;                                
       signal Tile_0_2_i_N_req    : std_logic;                                
       signal Tile_0_2_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_0_2_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_S_fb     : std_logic;                                
       signal Tile_0_2_i_S_req    : std_logic;                                
       signal Tile_0_2_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_0_2_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_E_fb     : std_logic;                                
       signal Tile_0_2_i_E_req    : std_logic;                                
       signal Tile_0_2_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_0_2_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_W_fb     : std_logic;                                
       signal Tile_0_2_i_W_req    : std_logic;                                
       signal Tile_0_2_i_W_ack    : std_logic;                                
   -- Signals for Tile_2_0;                                                   
       -- connections to North                                                        
       signal Tile_2_0_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_N_fb     : std_logic;                                
       signal Tile_2_0_i_N_req    : std_logic;                                
       signal Tile_2_0_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_2_0_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_S_fb     : std_logic;                                
       signal Tile_2_0_i_S_req    : std_logic;                                
       signal Tile_2_0_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_2_0_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_E_fb     : std_logic;                                
       signal Tile_2_0_i_E_req    : std_logic;                                
       signal Tile_2_0_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_2_0_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_W_fb     : std_logic;                                
       signal Tile_2_0_i_W_req    : std_logic;                                
       signal Tile_2_0_i_W_ack    : std_logic;                                
   -- Signals for Tile_2_2;                                                   
       -- connections to North                                                        
       signal Tile_2_2_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_N_fb     : std_logic;                                
       signal Tile_2_2_i_N_req    : std_logic;                                
       signal Tile_2_2_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_2_2_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_S_fb     : std_logic;                                
       signal Tile_2_2_i_S_req    : std_logic;                                
       signal Tile_2_2_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_2_2_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_E_fb     : std_logic;                                
       signal Tile_2_2_i_E_req    : std_logic;                                
       signal Tile_2_2_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_2_2_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_W_fb     : std_logic;                                
       signal Tile_2_2_i_W_req    : std_logic;                                
       signal Tile_2_2_i_W_ack    : std_logic;                                
begin                                                                 
                                                                      

-----------------------------------------------------------
Tile_0_0 : tile               
     generic map(                                 
            y_init              => 0            ,
            x_init              => 0            ,
            img_width           => img_width         ,
            img_height          => img_height        ,
            tile_width          => tile_width        ,
            tile_height         => tile_height       ,
            n_frames            => n_frames          ,
            n_steps             => n_steps           ,
            pix_depth           => pix_depth         ,
            subimg_width        => subimg_width      ,
            subimg_height       => subimg_height     ,
            buffer_length       => buffer_length     ,
            bit_width           => bit_width         ,
            memory_length       => memory_length     ,
            instruction_length  => instruction_length,
            isf_opcode          => isf_opcode        ,
            regfile_size        => regfile_size      ,
            isf_x               => isf_x             ,
            isf_y               => isf_y             ,
            isf_s               => isf_s             ,
            isf_f               => isf_f             ,
            isf_px              => isf_px            ,
            NOP                 => NOP               ,
            GPX                 => GPX               ,
            SPX                 => SPX               ,
            ADD                 => ADD               ,
            SUB                 => SUB               ,
            MUL                 => MUL               ,
            DIV                 => DIV               ,
            AND1                => AND1              ,
            OR1                 => OR1               ,
            XOR1                => XOR1              ,
            NOT1                => NOT1              ,
            BGT                 => BGT               ,
            BST                 => BST               ,
            BEQ                 => BEQ               ,
            JMP                 => JMP               ,
            ENDPGR              => ENDPGR            ,
            addr_size           => addr_size         
            )                                  
     port map(                                    
         clk => clk,                              
         -----------------------------------------
         -- AXI connections
         gen_port_in    => gen_port_in_0_0,
         gen_port_out   => gen_port_out_0_0,
         -- North connections                     
         i_N_pixel  => Tile_0_0_i_N_pixel, 
         i_N_x_dest => Tile_0_0_i_N_x_dest,
         i_N_y_dest => Tile_0_0_i_N_y_dest,
         i_N_step   => Tile_0_0_i_N_step,  
         i_N_frame  => Tile_0_0_i_N_frame, 
         i_N_x_orig => Tile_0_0_i_N_x_orig,
         i_N_y_orig => Tile_0_0_i_N_y_orig,
         i_N_fb     => Tile_0_0_i_N_fb,    
         i_N_req    => Tile_0_0_i_N_req,   
         i_N_ack    => Tile_0_0_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_0_i_N_pixel, 
         t_N_x_dest => Tile_0_0_i_N_x_dest,
         t_N_y_dest => Tile_0_0_i_N_y_dest,
         t_N_step   => Tile_0_0_i_N_step,  
         t_N_frame  => Tile_0_0_i_N_frame, 
         t_N_x_orig => Tile_0_0_i_N_x_orig,
         t_N_y_orig => Tile_0_0_i_N_y_orig,
         t_N_fb     => Tile_0_0_i_N_fb,    
         t_N_req    => Tile_0_0_i_N_req,   
         t_N_ack    => Tile_0_0_i_N_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_0_0_i_S_pixel, 
         i_S_x_dest => Tile_0_0_i_S_x_dest,
         i_S_y_dest => Tile_0_0_i_S_y_dest,
         i_S_step   => Tile_0_0_i_S_step,  
         i_S_frame  => Tile_0_0_i_S_frame, 
         i_S_x_orig => Tile_0_0_i_S_x_orig,
         i_S_y_orig => Tile_0_0_i_S_y_orig,
         i_S_fb     => Tile_0_0_i_S_fb,    
         i_S_req    => Tile_0_0_i_S_req,   
         i_S_ack    => Tile_0_0_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_0_i_N_pixel, 
         t_S_x_dest => Tile_2_0_i_N_x_dest,
         t_S_y_dest => Tile_2_0_i_N_y_dest,
         t_S_step   => Tile_2_0_i_N_step,  
         t_S_frame  => Tile_2_0_i_N_frame, 
         t_S_x_orig => Tile_2_0_i_N_x_orig,
         t_S_y_orig => Tile_2_0_i_N_y_orig,
         t_S_fb     => Tile_2_0_i_N_fb,    
         t_S_req    => Tile_2_0_i_N_req,   
         t_S_ack    => Tile_2_0_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_0_0_i_E_pixel, 
         i_E_x_dest => Tile_0_0_i_E_x_dest,
         i_E_y_dest => Tile_0_0_i_E_y_dest,
         i_E_step   => Tile_0_0_i_E_step,  
         i_E_frame  => Tile_0_0_i_E_frame, 
         i_E_x_orig => Tile_0_0_i_E_x_orig,
         i_E_y_orig => Tile_0_0_i_E_y_orig,
         i_E_fb     => Tile_0_0_i_E_fb,    
         i_E_req    => Tile_0_0_i_E_req,   
         i_E_ack    => Tile_0_0_i_E_ack,   
                                                   
         t_E_pixel  => Tile_0_2_i_W_pixel, 
         t_E_x_dest => Tile_0_2_i_W_x_dest,
         t_E_y_dest => Tile_0_2_i_W_y_dest,
         t_E_step   => Tile_0_2_i_W_step,  
         t_E_frame  => Tile_0_2_i_W_frame, 
         t_E_x_orig => Tile_0_2_i_W_x_orig,
         t_E_y_orig => Tile_0_2_i_W_y_orig,
         t_E_fb     => Tile_0_2_i_W_fb,    
         t_E_req    => Tile_0_2_i_W_req,   
         t_E_ack    => Tile_0_2_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_0_0_i_W_pixel, 
         i_W_x_dest => Tile_0_0_i_W_x_dest,
         i_W_y_dest => Tile_0_0_i_W_y_dest,
         i_W_step   => Tile_0_0_i_W_step,  
         i_W_frame  => Tile_0_0_i_W_frame, 
         i_W_x_orig => Tile_0_0_i_W_x_orig,
         i_W_y_orig => Tile_0_0_i_W_y_orig,
         i_W_fb     => Tile_0_0_i_W_fb,    
         i_W_req    => Tile_0_0_i_W_req,   
         i_W_ack    => Tile_0_0_i_W_ack,   
                                                   
         t_W_pixel  => Tile_0_0_i_W_pixel, 
         t_W_x_dest => Tile_0_0_i_W_x_dest,
         t_W_y_dest => Tile_0_0_i_W_y_dest,
         t_W_step   => Tile_0_0_i_W_step,  
         t_W_frame  => Tile_0_0_i_W_frame, 
         t_W_x_orig => Tile_0_0_i_W_x_orig,
         t_W_y_orig => Tile_0_0_i_W_y_orig,
         t_W_fb     => Tile_0_0_i_W_fb,    
         t_W_req    => Tile_0_0_i_W_req,   
         t_W_ack    => Tile_0_0_i_W_ack    
     );                                                
-----------------------------------------------------------
Tile_0_2 : tile               
     generic map(                                 
            y_init              => 0            ,
            x_init              => 128          ,
            img_width           => img_width         ,
            img_height          => img_height        ,
            tile_width          => tile_width        ,
            tile_height         => tile_height       ,
            n_frames            => n_frames          ,
            n_steps             => n_steps           ,
            pix_depth           => pix_depth         ,
            subimg_width        => subimg_width      ,
            subimg_height       => subimg_height     ,
            buffer_length       => buffer_length     ,
            bit_width           => bit_width         ,
            memory_length       => memory_length     ,
            instruction_length  => instruction_length,
            isf_opcode          => isf_opcode        ,
            regfile_size        => regfile_size      ,
            isf_x               => isf_x             ,
            isf_y               => isf_y             ,
            isf_s               => isf_s             ,
            isf_f               => isf_f             ,
            isf_px              => isf_px            ,
            NOP                 => NOP               ,
            GPX                 => GPX               ,
            SPX                 => SPX               ,
            ADD                 => ADD               ,
            SUB                 => SUB               ,
            MUL                 => MUL               ,
            DIV                 => DIV               ,
            AND1                => AND1              ,
            OR1                 => OR1               ,
            XOR1                => XOR1              ,
            NOT1                => NOT1              ,
            BGT                 => BGT               ,
            BST                 => BST               ,
            BEQ                 => BEQ               ,
            JMP                 => JMP               ,
            ENDPGR              => ENDPGR            ,
            addr_size           => addr_size         
            )                                  
     port map(                                    
         clk => clk,                                                        
         -----------------------------------------
         -- AXI connections
         gen_port_in    => gen_port_in_0_2,
         gen_port_out   => gen_port_out_0_2,
         -- North connections                     
         i_N_pixel  => Tile_0_2_i_N_pixel, 
         i_N_x_dest => Tile_0_2_i_N_x_dest,
         i_N_y_dest => Tile_0_2_i_N_y_dest,
         i_N_step   => Tile_0_2_i_N_step,  
         i_N_frame  => Tile_0_2_i_N_frame, 
         i_N_x_orig => Tile_0_2_i_N_x_orig,
         i_N_y_orig => Tile_0_2_i_N_y_orig,
         i_N_fb     => Tile_0_2_i_N_fb,    
         i_N_req    => Tile_0_2_i_N_req,   
         i_N_ack    => Tile_0_2_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_2_i_N_pixel, 
         t_N_x_dest => Tile_0_2_i_N_x_dest,
         t_N_y_dest => Tile_0_2_i_N_y_dest,
         t_N_step   => Tile_0_2_i_N_step,  
         t_N_frame  => Tile_0_2_i_N_frame, 
         t_N_x_orig => Tile_0_2_i_N_x_orig,
         t_N_y_orig => Tile_0_2_i_N_y_orig,
         t_N_fb     => Tile_0_2_i_N_fb,    
         t_N_req    => Tile_0_2_i_N_req,   
         t_N_ack    => Tile_0_2_i_N_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_0_2_i_S_pixel, 
         i_S_x_dest => Tile_0_2_i_S_x_dest,
         i_S_y_dest => Tile_0_2_i_S_y_dest,
         i_S_step   => Tile_0_2_i_S_step,  
         i_S_frame  => Tile_0_2_i_S_frame, 
         i_S_x_orig => Tile_0_2_i_S_x_orig,
         i_S_y_orig => Tile_0_2_i_S_y_orig,
         i_S_fb     => Tile_0_2_i_S_fb,    
         i_S_req    => Tile_0_2_i_S_req,   
         i_S_ack    => Tile_0_2_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_2_i_N_pixel, 
         t_S_x_dest => Tile_2_2_i_N_x_dest,
         t_S_y_dest => Tile_2_2_i_N_y_dest,
         t_S_step   => Tile_2_2_i_N_step,  
         t_S_frame  => Tile_2_2_i_N_frame, 
         t_S_x_orig => Tile_2_2_i_N_x_orig,
         t_S_y_orig => Tile_2_2_i_N_y_orig,
         t_S_fb     => Tile_2_2_i_N_fb,    
         t_S_req    => Tile_2_2_i_N_req,   
         t_S_ack    => Tile_2_2_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_0_2_i_E_pixel, 
         i_E_x_dest => Tile_0_2_i_E_x_dest,
         i_E_y_dest => Tile_0_2_i_E_y_dest,
         i_E_step   => Tile_0_2_i_E_step,  
         i_E_frame  => Tile_0_2_i_E_frame, 
         i_E_x_orig => Tile_0_2_i_E_x_orig,
         i_E_y_orig => Tile_0_2_i_E_y_orig,
         i_E_fb     => Tile_0_2_i_E_fb,    
         i_E_req    => Tile_0_2_i_E_req,   
         i_E_ack    => Tile_0_2_i_E_ack,   
                                                   
         t_E_pixel  => Tile_0_2_i_E_pixel, 
         t_E_x_dest => Tile_0_2_i_E_x_dest,
         t_E_y_dest => Tile_0_2_i_E_y_dest,
         t_E_step   => Tile_0_2_i_E_step,  
         t_E_frame  => Tile_0_2_i_E_frame, 
         t_E_x_orig => Tile_0_2_i_E_x_orig,
         t_E_y_orig => Tile_0_2_i_E_y_orig,
         t_E_fb     => Tile_0_2_i_E_fb,    
         t_E_req    => Tile_0_2_i_E_req,   
         t_E_ack    => Tile_0_2_i_E_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_0_2_i_W_pixel, 
         i_W_x_dest => Tile_0_2_i_W_x_dest,
         i_W_y_dest => Tile_0_2_i_W_y_dest,
         i_W_step   => Tile_0_2_i_W_step,  
         i_W_frame  => Tile_0_2_i_W_frame, 
         i_W_x_orig => Tile_0_2_i_W_x_orig,
         i_W_y_orig => Tile_0_2_i_W_y_orig,
         i_W_fb     => Tile_0_2_i_W_fb,    
         i_W_req    => Tile_0_2_i_W_req,   
         i_W_ack    => Tile_0_2_i_W_ack,   
                                                   
         t_W_pixel  => Tile_0_0_i_E_pixel, 
         t_W_x_dest => Tile_0_0_i_E_x_dest,
         t_W_y_dest => Tile_0_0_i_E_y_dest,
         t_W_step   => Tile_0_0_i_E_step,  
         t_W_frame  => Tile_0_0_i_E_frame, 
         t_W_x_orig => Tile_0_0_i_E_x_orig,
         t_W_y_orig => Tile_0_0_i_E_y_orig,
         t_W_fb     => Tile_0_0_i_E_fb,    
         t_W_req    => Tile_0_0_i_E_req,   
         t_W_ack    => Tile_0_0_i_E_ack    
     );                                                
-----------------------------------------------------------
Tile_2_0 : tile               
     generic map(                                
            y_init              => 128          , 
            x_init              => 0            ,
            img_width           => img_width         ,
            img_height          => img_height        ,
            tile_width          => tile_width        ,
            tile_height         => tile_height       ,
            n_frames            => n_frames          ,
            n_steps             => n_steps           ,
            pix_depth           => pix_depth         ,
            subimg_width        => subimg_width      ,
            subimg_height       => subimg_height     ,
            buffer_length       => buffer_length     ,
            bit_width           => bit_width         ,
            memory_length       => memory_length     ,
            instruction_length  => instruction_length,
            isf_opcode          => isf_opcode        ,
            regfile_size        => regfile_size      ,
            isf_x               => isf_x             ,
            isf_y               => isf_y             ,
            isf_s               => isf_s             ,
            isf_f               => isf_f             ,
            isf_px              => isf_px            ,
            NOP                 => NOP               ,
            GPX                 => GPX               ,
            SPX                 => SPX               ,
            ADD                 => ADD               ,
            SUB                 => SUB               ,
            MUL                 => MUL               ,
            DIV                 => DIV               ,
            AND1                => AND1              ,
            OR1                 => OR1               ,
            XOR1                => XOR1              ,
            NOT1                => NOT1              ,
            BGT                 => BGT               ,
            BST                 => BST               ,
            BEQ                 => BEQ               ,
            JMP                 => JMP               ,
            ENDPGR              => ENDPGR            ,
            addr_size           => addr_size         
            )                                  
     port map(                                    
         clk => clk,                                     
         -----------------------------------------
         -- AXI connections
         gen_port_in    => gen_port_in_2_0,
         gen_port_out   => gen_port_out_2_0,
         -- North connections                     
         i_N_pixel  => Tile_2_0_i_N_pixel, 
         i_N_x_dest => Tile_2_0_i_N_x_dest,
         i_N_y_dest => Tile_2_0_i_N_y_dest,
         i_N_step   => Tile_2_0_i_N_step,  
         i_N_frame  => Tile_2_0_i_N_frame, 
         i_N_x_orig => Tile_2_0_i_N_x_orig,
         i_N_y_orig => Tile_2_0_i_N_y_orig,
         i_N_fb     => Tile_2_0_i_N_fb,    
         i_N_req    => Tile_2_0_i_N_req,   
         i_N_ack    => Tile_2_0_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_0_i_S_pixel, 
         t_N_x_dest => Tile_0_0_i_S_x_dest,
         t_N_y_dest => Tile_0_0_i_S_y_dest,
         t_N_step   => Tile_0_0_i_S_step,  
         t_N_frame  => Tile_0_0_i_S_frame, 
         t_N_x_orig => Tile_0_0_i_S_x_orig,
         t_N_y_orig => Tile_0_0_i_S_y_orig,
         t_N_fb     => Tile_0_0_i_S_fb,    
         t_N_req    => Tile_0_0_i_S_req,   
         t_N_ack    => Tile_0_0_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_2_0_i_S_pixel, 
         i_S_x_dest => Tile_2_0_i_S_x_dest,
         i_S_y_dest => Tile_2_0_i_S_y_dest,
         i_S_step   => Tile_2_0_i_S_step,  
         i_S_frame  => Tile_2_0_i_S_frame, 
         i_S_x_orig => Tile_2_0_i_S_x_orig,
         i_S_y_orig => Tile_2_0_i_S_y_orig,
         i_S_fb     => Tile_2_0_i_S_fb,    
         i_S_req    => Tile_2_0_i_S_req,   
         i_S_ack    => Tile_2_0_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_0_i_S_pixel, 
         t_S_x_dest => Tile_2_0_i_S_x_dest,
         t_S_y_dest => Tile_2_0_i_S_y_dest,
         t_S_step   => Tile_2_0_i_S_step,  
         t_S_frame  => Tile_2_0_i_S_frame, 
         t_S_x_orig => Tile_2_0_i_S_x_orig,
         t_S_y_orig => Tile_2_0_i_S_y_orig,
         t_S_fb     => Tile_2_0_i_S_fb,    
         t_S_req    => Tile_2_0_i_S_req,   
         t_S_ack    => Tile_2_0_i_S_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_2_0_i_E_pixel, 
         i_E_x_dest => Tile_2_0_i_E_x_dest,
         i_E_y_dest => Tile_2_0_i_E_y_dest,
         i_E_step   => Tile_2_0_i_E_step,  
         i_E_frame  => Tile_2_0_i_E_frame, 
         i_E_x_orig => Tile_2_0_i_E_x_orig,
         i_E_y_orig => Tile_2_0_i_E_y_orig,
         i_E_fb     => Tile_2_0_i_E_fb,    
         i_E_req    => Tile_2_0_i_E_req,   
         i_E_ack    => Tile_2_0_i_E_ack,   
                                                   
         t_E_pixel  => Tile_2_2_i_W_pixel, 
         t_E_x_dest => Tile_2_2_i_W_x_dest,
         t_E_y_dest => Tile_2_2_i_W_y_dest,
         t_E_step   => Tile_2_2_i_W_step,  
         t_E_frame  => Tile_2_2_i_W_frame, 
         t_E_x_orig => Tile_2_2_i_W_x_orig,
         t_E_y_orig => Tile_2_2_i_W_y_orig,
         t_E_fb     => Tile_2_2_i_W_fb,    
         t_E_req    => Tile_2_2_i_W_req,   
         t_E_ack    => Tile_2_2_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_2_0_i_W_pixel, 
         i_W_x_dest => Tile_2_0_i_W_x_dest,
         i_W_y_dest => Tile_2_0_i_W_y_dest,
         i_W_step   => Tile_2_0_i_W_step,  
         i_W_frame  => Tile_2_0_i_W_frame, 
         i_W_x_orig => Tile_2_0_i_W_x_orig,
         i_W_y_orig => Tile_2_0_i_W_y_orig,
         i_W_fb     => Tile_2_0_i_W_fb,    
         i_W_req    => Tile_2_0_i_W_req,   
         i_W_ack    => Tile_2_0_i_W_ack,   
                                                   
         t_W_pixel  => Tile_2_0_i_W_pixel, 
         t_W_x_dest => Tile_2_0_i_W_x_dest,
         t_W_y_dest => Tile_2_0_i_W_y_dest,
         t_W_step   => Tile_2_0_i_W_step,  
         t_W_frame  => Tile_2_0_i_W_frame, 
         t_W_x_orig => Tile_2_0_i_W_x_orig,
         t_W_y_orig => Tile_2_0_i_W_y_orig,
         t_W_fb     => Tile_2_0_i_W_fb,    
         t_W_req    => Tile_2_0_i_W_req,   
         t_W_ack    => Tile_2_0_i_W_ack    
     );                                                
-----------------------------------------------------------
Tile_2_2 : tile               
     generic map(                                
            y_init              => 128          , 
            x_init              => 128          ,
            img_width           => img_width         ,
            img_height          => img_height        ,
            tile_width          => tile_width        ,
            tile_height         => tile_height       ,
            n_frames            => n_frames          ,
            n_steps             => n_steps           ,
            pix_depth           => pix_depth         ,
            subimg_width        => subimg_width      ,
            subimg_height       => subimg_height     ,
            buffer_length       => buffer_length     ,
            bit_width           => bit_width         ,
            memory_length       => memory_length     ,
            instruction_length  => instruction_length,
            isf_opcode          => isf_opcode        ,
            regfile_size        => regfile_size      ,
            isf_x               => isf_x             ,
            isf_y               => isf_y             ,
            isf_s               => isf_s             ,
            isf_f               => isf_f             ,
            isf_px              => isf_px            ,
            NOP                 => NOP               ,
            GPX                 => GPX               ,
            SPX                 => SPX               ,
            ADD                 => ADD               ,
            SUB                 => SUB               ,
            MUL                 => MUL               ,
            DIV                 => DIV               ,
            AND1                => AND1              ,
            OR1                 => OR1               ,
            XOR1                => XOR1              ,
            NOT1                => NOT1              ,
            BGT                 => BGT               ,
            BST                 => BST               ,
            BEQ                 => BEQ               ,
            JMP                 => JMP               ,
            ENDPGR              => ENDPGR            ,
            addr_size           => addr_size         
            )                                  
     port map(                                    
         clk => clk,                                              
         -----------------------------------------
         -- AXI connections
         gen_port_in    => gen_port_in_2_2,
         gen_port_out   => gen_port_out_2_2,
         -- North connections                     
         i_N_pixel  => Tile_2_2_i_N_pixel, 
         i_N_x_dest => Tile_2_2_i_N_x_dest,
         i_N_y_dest => Tile_2_2_i_N_y_dest,
         i_N_step   => Tile_2_2_i_N_step,  
         i_N_frame  => Tile_2_2_i_N_frame, 
         i_N_x_orig => Tile_2_2_i_N_x_orig,
         i_N_y_orig => Tile_2_2_i_N_y_orig,
         i_N_fb     => Tile_2_2_i_N_fb,    
         i_N_req    => Tile_2_2_i_N_req,   
         i_N_ack    => Tile_2_2_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_2_i_S_pixel, 
         t_N_x_dest => Tile_0_2_i_S_x_dest,
         t_N_y_dest => Tile_0_2_i_S_y_dest,
         t_N_step   => Tile_0_2_i_S_step,  
         t_N_frame  => Tile_0_2_i_S_frame, 
         t_N_x_orig => Tile_0_2_i_S_x_orig,
         t_N_y_orig => Tile_0_2_i_S_y_orig,
         t_N_fb     => Tile_0_2_i_S_fb,    
         t_N_req    => Tile_0_2_i_S_req,   
         t_N_ack    => Tile_0_2_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_2_2_i_S_pixel, 
         i_S_x_dest => Tile_2_2_i_S_x_dest,
         i_S_y_dest => Tile_2_2_i_S_y_dest,
         i_S_step   => Tile_2_2_i_S_step,  
         i_S_frame  => Tile_2_2_i_S_frame, 
         i_S_x_orig => Tile_2_2_i_S_x_orig,
         i_S_y_orig => Tile_2_2_i_S_y_orig,
         i_S_fb     => Tile_2_2_i_S_fb,    
         i_S_req    => Tile_2_2_i_S_req,   
         i_S_ack    => Tile_2_2_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_2_i_S_pixel, 
         t_S_x_dest => Tile_2_2_i_S_x_dest,
         t_S_y_dest => Tile_2_2_i_S_y_dest,
         t_S_step   => Tile_2_2_i_S_step,  
         t_S_frame  => Tile_2_2_i_S_frame, 
         t_S_x_orig => Tile_2_2_i_S_x_orig,
         t_S_y_orig => Tile_2_2_i_S_y_orig,
         t_S_fb     => Tile_2_2_i_S_fb,    
         t_S_req    => Tile_2_2_i_S_req,   
         t_S_ack    => Tile_2_2_i_S_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_2_2_i_E_pixel, 
         i_E_x_dest => Tile_2_2_i_E_x_dest,
         i_E_y_dest => Tile_2_2_i_E_y_dest,
         i_E_step   => Tile_2_2_i_E_step,  
         i_E_frame  => Tile_2_2_i_E_frame, 
         i_E_x_orig => Tile_2_2_i_E_x_orig,
         i_E_y_orig => Tile_2_2_i_E_y_orig,
         i_E_fb     => Tile_2_2_i_E_fb,    
         i_E_req    => Tile_2_2_i_E_req,   
         i_E_ack    => Tile_2_2_i_E_ack,   
                                                   
         t_E_pixel  => Tile_2_2_i_E_pixel, 
         t_E_x_dest => Tile_2_2_i_E_x_dest,
         t_E_y_dest => Tile_2_2_i_E_y_dest,
         t_E_step   => Tile_2_2_i_E_step,  
         t_E_frame  => Tile_2_2_i_E_frame, 
         t_E_x_orig => Tile_2_2_i_E_x_orig,
         t_E_y_orig => Tile_2_2_i_E_y_orig,
         t_E_fb     => Tile_2_2_i_E_fb,    
         t_E_req    => Tile_2_2_i_E_req,   
         t_E_ack    => Tile_2_2_i_E_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_2_2_i_W_pixel, 
         i_W_x_dest => Tile_2_2_i_W_x_dest,
         i_W_y_dest => Tile_2_2_i_W_y_dest,
         i_W_step   => Tile_2_2_i_W_step,  
         i_W_frame  => Tile_2_2_i_W_frame, 
         i_W_x_orig => Tile_2_2_i_W_x_orig,
         i_W_y_orig => Tile_2_2_i_W_y_orig,
         i_W_fb     => Tile_2_2_i_W_fb,    
         i_W_req    => Tile_2_2_i_W_req,   
         i_W_ack    => Tile_2_2_i_W_ack,   
                                                   
         t_W_pixel  => Tile_2_0_i_E_pixel, 
         t_W_x_dest => Tile_2_0_i_E_x_dest,
         t_W_y_dest => Tile_2_0_i_E_y_dest,
         t_W_step   => Tile_2_0_i_E_step,  
         t_W_frame  => Tile_2_0_i_E_frame, 
         t_W_x_orig => Tile_2_0_i_E_x_orig,
         t_W_y_orig => Tile_2_0_i_E_y_orig,
         t_W_fb     => Tile_2_0_i_E_fb,    
         t_W_req    => Tile_2_0_i_E_req,   
         t_W_ack    => Tile_2_0_i_E_ack    
     );                                                
-----------------------------------------------------------
end architecture behavioral;                                          