----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2020 05:17:22 PM
-- Design Name: 
-- Module Name: pe_wrapper_test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pe_wrapper_test is
    generic (
            x_init : integer;
            y_init : integer;
            bit_width           : natural;
            memory_length       : natural;
            instruction_length  : natural;
            isf_opcode          : natural;
            regfile_size        : natural;
            isf_x               : natural;
            isf_y               : natural;
            isf_s               : natural;
            isf_f               : natural;
            isf_px              : natural;
            NOP                 : std_logic_vector(4-1 downto 0);
            GPX                 : std_logic_vector(4-1 downto 0);
            SPX                 : std_logic_vector(4-1 downto 0);
            ADD                 : std_logic_vector(4-1 downto 0);
            SUB                 : std_logic_vector(4-1 downto 0);
            MUL                 : std_logic_vector(4-1 downto 0);
            DIV                 : std_logic_vector(4-1 downto 0);
            AND1                : std_logic_vector(4-1 downto 0);
            OR1                 : std_logic_vector(4-1 downto 0);
            XOR1                : std_logic_vector(4-1 downto 0);
            NOT1                : std_logic_vector(4-1 downto 0);
            BGT                 : std_logic_vector(4-1 downto 0);
            BST                 : std_logic_vector(4-1 downto 0);
            BEQ                 : std_logic_vector(4-1 downto 0);
            JMP                 : std_logic_vector(4-1 downto 0);
            ENDPGR              : std_logic_vector(4-1 downto 0);
            addr_size           : natural;
            img_width           : natural;
            img_height          : natural;
            n_frames            : natural;
            n_steps             : natural;
            pix_depth           : natural
            );
    port(
        clk, reset : in STD_LOGIC;
        o_endpgr_flag : out STD_LOGIC;

        -- New Local target ports - the PE requests values using these ports.
        i_PE_pixel  : out STD_LOGIC_VECTOR(pix_depth-1 downto 0);
        i_PE_x_dest : out STD_LOGIC_VECTOR(img_width-1 downto 0);
        i_PE_y_dest : out STD_LOGIC_VECTOR(img_height-1 downto 0);
        i_PE_step   : out STD_LOGIC_VECTOR(n_steps-1 downto 0);
        i_PE_frame  : out STD_LOGIC_VECTOR(n_frames-1 downto 0);
        i_PE_x_orig : out STD_LOGIC_VECTOR(img_width-1 downto 0);
        i_PE_y_orig : out STD_LOGIC_VECTOR(img_height-1 downto 0);
        i_PE_fb     : out STD_LOGIC; -- message forward/backward
        i_PE_req    : inout STD_LOGIC; -- message request
       --iPEN_busy   : in std_logic; -- router is busy
        i_PE_ack    : in STD_LOGIC := '0'; -- message acknowledge  
            
        t_PE_pixel  : in STD_LOGIC_VECTOR(pix_depth-1 downto 0);
        t_PE_x_dest : in STD_LOGIC_VECTOR(img_width-1 downto 0);
        t_PE_y_dest : in STD_LOGIC_VECTOR(img_height-1 downto 0);
        t_PE_step   : in STD_LOGIC_VECTOR(n_steps-1 downto 0);
        t_PE_frame  : in STD_LOGIC_VECTOR(n_frames-1 downto 0);
        t_PE_x_orig : in STD_LOGIC_VECTOR(img_width-1 downto 0);
        t_PE_y_orig : in STD_LOGIC_VECTOR(img_height-1 downto 0);
        t_PE_fb     : in STD_LOGIC; -- message forward/backward
        t_PE_req    : in STD_LOGIC; -- message request
        --t_PE_busy   : out std_logic; -- router is busy
        t_PE_ack    : out STD_LOGIC -- message acknowledge
     );
end pe_wrapper_test;

architecture Behavioral of pe_wrapper_test is

component pe_wrapper is
    generic (
            x_init : integer;
            y_init : integer;
            bit_width           : natural;
            memory_length       : natural;
            instruction_length  : natural;
            isf_opcode          : natural;
            regfile_size        : natural;
            isf_x               : natural;
            isf_y               : natural;
            isf_s               : natural;
            isf_f               : natural;
            isf_px              : natural;
            NOP                 : std_logic_vector(4-1 downto 0);
            GPX                 : std_logic_vector(4-1 downto 0);
            SPX                 : std_logic_vector(4-1 downto 0);
            ADD                 : std_logic_vector(4-1 downto 0);
            SUB                 : std_logic_vector(4-1 downto 0);
            MUL                 : std_logic_vector(4-1 downto 0);
            DIV                 : std_logic_vector(4-1 downto 0);
            AND1                : std_logic_vector(4-1 downto 0);
            OR1                 : std_logic_vector(4-1 downto 0);
            XOR1                : std_logic_vector(4-1 downto 0);
            NOT1                : std_logic_vector(4-1 downto 0);
            BGT                 : std_logic_vector(4-1 downto 0);
            BST                 : std_logic_vector(4-1 downto 0);
            BEQ                 : std_logic_vector(4-1 downto 0);
            JMP                 : std_logic_vector(4-1 downto 0);
            ENDPGR              : std_logic_vector(4-1 downto 0);
            addr_size           : natural;
            img_width           : natural;
            img_height          : natural;
            n_frames            : natural;
            n_steps             : natural;
            pix_depth           : natural
            );
    port(
        clk, reset : in STD_LOGIC;
        o_endpgr_flag : out STD_LOGIC;
    
    -- New Local target ports - the PE requests values using these ports.
        i_PE_pixel  : out STD_LOGIC_VECTOR(pix_depth-1 downto 0);
        i_PE_x_dest : out STD_LOGIC_VECTOR(img_width-1 downto 0);
        i_PE_y_dest : out STD_LOGIC_VECTOR(img_height-1 downto 0);
        i_PE_step   : out STD_LOGIC_VECTOR(n_steps-1 downto 0);
        i_PE_frame  : out STD_LOGIC_VECTOR(n_frames-1 downto 0);
        i_PE_x_orig : out STD_LOGIC_VECTOR(img_width-1 downto 0);
        i_PE_y_orig : out STD_LOGIC_VECTOR(img_height-1 downto 0);
        i_PE_fb     : out STD_LOGIC; -- message forward/backward
        i_PE_req    : inout STD_LOGIC; -- message request
       --iPEN_busy   : in std_logic; -- router is busy
        i_PE_ack    : in STD_LOGIC := '0'; -- message acknowledge  
            
        t_PE_pixel  : in STD_LOGIC_VECTOR(pix_depth-1 downto 0);
        t_PE_x_dest : in STD_LOGIC_VECTOR(img_width-1 downto 0);
        t_PE_y_dest : in STD_LOGIC_VECTOR(img_height-1 downto 0);
        t_PE_step   : in STD_LOGIC_VECTOR(n_steps-1 downto 0);
        t_PE_frame  : in STD_LOGIC_VECTOR(n_frames-1 downto 0);
        t_PE_x_orig : in STD_LOGIC_VECTOR(img_width-1 downto 0);
        t_PE_y_orig : in STD_LOGIC_VECTOR(img_height-1 downto 0);
        t_PE_fb     : in STD_LOGIC; -- message forward/backward
        t_PE_req    : in STD_LOGIC; -- message request
        --t_PE_busy   : out std_logic; -- router is busy
        t_PE_ack    : out STD_LOGIC; -- message acknowledge 
        
        -- AXI/Instruction Memory interface
        i_axi_clk       : in STD_LOGIC;
        i_axi_we        : in STD_LOGIC;
        i_axi_addr      : in STD_LOGIC_VECTOR(31 downto 0);
        i_axi_data0     : in STD_LOGIC_VECTOR(31 downto 0);
        i_axi_data1     : in STD_LOGIC_VECTOR(31 downto 0);
        o_axi_imok      : out STD_LOGIC 
    );
end component pe_wrapper;

    -- AXI Signals
    signal done             : std_logic;
    signal rd_done          : std_logic;     
    signal i_axi_clk        : std_logic;
    signal i_axi_en         : std_logic;
    signal i_axi_we         : std_logic;
    signal i_axi_x          : unsigned(31 downto 0);
    signal i_axi_y          : unsigned(31 downto 0);
    signal i_axi_s          : unsigned(31 downto 0);
    signal i_axi_f          : unsigned(31 downto 0);
    signal i_axi_px         : unsigned(31 downto 0);
    signal o_axi_px         : unsigned(31 downto 0);
    
    -- AXI Constants
    constant iclk_f         : integer   := 1;
    constant ien_f          : integer   := 1 + iclk_f;
    constant iwe_f          : integer   := 1 + ien_f;
    constant ix_f           : integer   := 32 + iwe_f;
    constant iy_f           : integer   := 32 + ix_f;
    constant is_f           : integer   := 32 + iy_f;
    constant if_f           : integer   := 32 + is_f;
    constant ipx_f          : integer   := 32 + if_f;
    
    constant done_f         : integer   := 1;
    constant rd_done_f      : integer   := 1 + done_f;
    constant opx_f          : integer   := 32 + rd_done_f;
    
    -- AXI/Instruction Memory interface signals
    signal i_axi_addr      : STD_LOGIC_VECTOR(31 downto 0);
    signal i_axi_data0     : STD_LOGIC_VECTOR(31 downto 0);
    signal i_axi_data1     : STD_LOGIC_VECTOR(31 downto 0);
    signal o_axi_imok       : STD_LOGIC; 

    -- New Local target ports - the PE requests values using these ports.
    signal si_PE_pixel  :  STD_LOGIC_VECTOR(pix_depth-1 downto 0)   := (others => '0');
    signal si_PE_x_dest :  STD_LOGIC_VECTOR(img_width-1 downto 0)   := (others => '0');
    signal si_PE_y_dest :  STD_LOGIC_VECTOR(img_height-1 downto 0)  := (others => '0');
    signal si_PE_step   :  STD_LOGIC_VECTOR(n_steps-1 downto 0)     := (others => '0');
    signal si_PE_frame  :  STD_LOGIC_VECTOR(n_frames-1 downto 0)    := (others => '0');
    signal si_PE_x_orig :  STD_LOGIC_VECTOR(img_width-1 downto 0)   := (others => '0');
    signal si_PE_y_orig :  STD_LOGIC_VECTOR(img_height-1 downto 0)  := (others => '0');
    signal si_PE_fb     :  STD_LOGIC := '0'; -- message forward/backward
    signal si_PE_req    :  STD_LOGIC := '0'; -- message request
   --siPEN_busy   : in std_logic; -- router is busy
    signal si_PE_ack    :  STD_LOGIC := '0'; -- message acknowledge  
         
    signal st_PE_pixel  :  STD_LOGIC_VECTOR(pix_depth-1 downto 0)   := (others => '0');
    signal st_PE_x_dest :  STD_LOGIC_VECTOR(img_width-1 downto 0)   := (others => '0');
    signal st_PE_y_dest :  STD_LOGIC_VECTOR(img_height-1 downto 0)  := (others => '0');
    signal st_PE_step   :  STD_LOGIC_VECTOR(n_steps-1 downto 0)     := (others => '0');
    signal st_PE_frame  :  STD_LOGIC_VECTOR(n_frames-1 downto 0)    := (others => '0');
    signal st_PE_x_orig :  STD_LOGIC_VECTOR(img_width-1 downto 0)   := (others => '0');
    signal st_PE_y_orig :  STD_LOGIC_VECTOR(img_height-1 downto 0)  := (others => '0');
    signal st_PE_fb     :  STD_LOGIC := '0'; -- message forward/backward
    signal st_PE_req    :  STD_LOGIC := '0'; -- message request
  --t_PE_busy   : out std_logic; -- router is busy
    signal st_PE_ack    :  STD_LOGIC := '0'; -- message acknowledge    
    

    signal st_PM_pixel  : std_logic_vector(pix_depth-1 downto 0) := (others => '0');
    signal st_PM_x_dest : std_logic_vector(img_width-1 downto 0) := (others => '0');
    signal st_PM_y_dest : std_logic_vector(img_height-1 downto 0) := (others => '0');
    signal st_PM_step   : std_logic_vector(n_steps-1 downto 0) := (others => '0');
    signal st_PM_frame  : std_logic_vector(n_frames-1 downto 0) := (others => '0');
    signal st_PM_x_orig : std_logic_vector(img_width-1 downto 0) := (others => '0');
    signal st_PM_y_orig : std_logic_vector(img_height-1 downto 0) := (others => '0');
    signal st_PM_fb     : std_logic := '0'; -- message forward/backward
    signal st_PM_req    : std_logic := '0'; -- message request
    signal st_PM_ack    : std_logic := '0'; -- message acknowledge       
     -- initiar output ports    
    signal si_PM_pixel  : std_logic_vector(pix_depth-1 downto 0) := (others => '0');
    signal si_PM_x_dest : std_logic_vector(img_width-1 downto 0) := (others => '0');
    signal si_PM_y_dest : std_logic_vector(img_height-1 downto 0) := (others => '0');
    signal si_PM_step   : std_logic_vector(n_steps-1 downto 0) := (others => '0');
    signal si_PM_frame  : std_logic_vector(n_frames-1 downto 0) := (others => '0');
    signal si_PM_x_orig : std_logic_vector(img_width-1 downto 0) := (others => '0');
    signal si_PM_y_orig : std_logic_vector(img_height-1 downto 0) := (others => '0');
    signal si_PM_fb     : std_logic := '0'; -- message forward/backward
    signal si_PM_req    : std_logic := '0'; -- message request
    signal si_PM_ack    : std_logic := '0'; -- message acknowledge 


begin

        pe_wrapper_inst : pe_wrapper generic  map(
                                            x_init      => x_init,
                                            y_init      => y_init,
                                            bit_width           => bit_width         ,
                                            memory_length       => memory_length     ,
                                            instruction_length  => instruction_length,
                                            isf_opcode          => isf_opcode        ,
                                            regfile_size        => regfile_size      ,
                                            isf_x               => isf_x             ,
                                            isf_y               => isf_y             ,
                                            isf_s               => isf_s             ,
                                            isf_f               => isf_f             ,
                                            isf_px              => isf_px            ,
                                            NOP                 => NOP               ,
                                            GPX                 => GPX               ,
                                            SPX                 => SPX               ,
                                            ADD                 => ADD               ,
                                            SUB                 => SUB               ,
                                            MUL                 => MUL               ,
                                            DIV                 => DIV               ,
                                            AND1                => AND1              ,
                                            OR1                 => OR1               ,
                                            XOR1                => XOR1              ,
                                            NOT1                => NOT1              ,
                                            BGT                 => BGT               ,
                                            BST                 => BST               ,
                                            BEQ                 => BEQ               ,
                                            JMP                 => JMP               ,
                                            ENDPGR              => ENDPGR            ,
                                            addr_size           => addr_size         ,
                                            img_width           => img_width         ,
                                            img_height          => img_height        ,
                                            n_frames            => n_frames          ,
                                            n_steps             => n_steps           ,
                                            pix_depth           => pix_depth         
                                            )
                                port    map(
                                            clk             => clk,
                                            reset           => reset,
                                            o_endpgr_flag   => done,
                                            i_PE_pixel      => i_PE_pixel,
                                            i_PE_x_dest     => i_PE_x_dest,
                                            i_PE_y_dest     => i_PE_y_dest,
                                            i_PE_step       => i_PE_step,
                                            i_PE_frame      => i_PE_frame,
                                            i_PE_x_orig     => i_PE_x_orig,
                                            i_PE_y_orig     => i_PE_y_orig,
                                            i_PE_fb         => i_PE_fb,
                                            i_PE_req        => i_PE_req,
                                            i_PE_ack        => i_PE_ack,
                                            t_PE_pixel      => t_PE_pixel,
                                            t_PE_x_dest     => t_PE_x_dest,
                                            t_PE_y_dest     => t_PE_y_dest,
                                            t_PE_step       => t_PE_step,
                                            t_PE_frame      => t_PE_frame,
                                            t_PE_x_orig     => t_PE_x_orig,
                                            t_PE_y_orig     => t_PE_y_orig,
                                            t_PE_fb         => t_PE_fb,
                                            t_PE_req        => t_PE_req,
                                            t_PE_ack        => t_PE_ack,
                                            i_axi_clk       => i_axi_clk,
                                            i_axi_we        => i_axi_we,
                                            i_axi_addr      => i_axi_addr,
                                            i_axi_data0     => i_axi_data0,
                                            i_axi_data1     => i_axi_data1,
                                            o_axi_imok      => o_axi_imok
                                            );    


end Behavioral;
