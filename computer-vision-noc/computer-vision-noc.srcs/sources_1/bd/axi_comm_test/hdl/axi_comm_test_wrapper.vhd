--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
--Date        : Tue Mar 10 07:33:55 2020
--Host        : leia-workstation running 64-bit Ubuntu 18.04.3 LTS
--Command     : generate_target axi_comm_test_wrapper.bd
--Design      : axi_comm_test_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity axi_comm_test_wrapper is
  port (
    reset : in STD_LOGIC
  );
end axi_comm_test_wrapper;

architecture STRUCTURE of axi_comm_test_wrapper is
  component axi_comm_test is
  port (
    reset : in STD_LOGIC
  );
  end component axi_comm_test;
begin
axi_comm_test_i: component axi_comm_test
     port map (
      reset => reset
    );
end STRUCTURE;
