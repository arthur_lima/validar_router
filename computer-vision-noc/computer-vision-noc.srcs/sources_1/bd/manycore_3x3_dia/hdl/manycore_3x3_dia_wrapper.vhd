--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
--Date        : Thu Feb 13 10:46:42 2020
--Host        : leia-workstation running 64-bit Ubuntu 18.04.3 LTS
--Command     : generate_target manycore_3x3_dia_wrapper.bd
--Design      : manycore_3x3_dia_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity manycore_3x3_dia_wrapper is
  port (
    reset : in STD_LOGIC
  );
end manycore_3x3_dia_wrapper;

architecture STRUCTURE of manycore_3x3_dia_wrapper is
  component manycore_3x3_dia is
  port (
    reset : in STD_LOGIC
  );
  end component manycore_3x3_dia;
begin
manycore_3x3_dia_i: component manycore_3x3_dia
     port map (
      reset => reset
    );
end STRUCTURE;
