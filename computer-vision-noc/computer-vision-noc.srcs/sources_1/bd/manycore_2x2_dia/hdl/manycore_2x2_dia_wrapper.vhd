--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
--Date        : Fri Mar 20 02:24:45 2020
--Host        : DESKTOP-K89RT1A running 64-bit major release  (build 9200)
--Command     : generate_target manycore_2x2_dia_wrapper.bd
--Design      : manycore_2x2_dia_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity manycore_2x2_dia_wrapper is
  port (
    reset : in STD_LOGIC
  );
end manycore_2x2_dia_wrapper;

architecture STRUCTURE of manycore_2x2_dia_wrapper is
  component manycore_2x2_dia is
  port (
    reset : in STD_LOGIC
  );
  end component manycore_2x2_dia;
begin
manycore_2x2_dia_i: component manycore_2x2_dia
     port map (
      reset => reset
    );
end STRUCTURE;
