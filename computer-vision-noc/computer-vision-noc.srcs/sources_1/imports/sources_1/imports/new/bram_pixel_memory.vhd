-- Author: Jones Yudi - University of Brasilia
-- Modified by: Bruno Almeida

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity bram_pixel_memory is

generic(
    x_init          : natural := 0;
    y_init          : natural := 0;
    pix_depth       : natural;
    img_width       : natural;
    img_height      : natural;
    tile_width      : natural;
    tile_height     : natural;
    n_frames        : natural;
    n_steps         : natural
);

port(
    ----------------------------------------------------
    -- port to write pixel values from the AXI
    i_axi_clk   : in std_logic;
    i_axi_en    : in std_logic;
    i_axi_we    : in std_logic;

    i_axi_x     : in unsigned(31 downto 0);
    i_axi_y     : in unsigned(31 downto 0);
    i_axi_s     : in unsigned(31 downto 0);
    i_axi_f     : in unsigned(31 downto 0);
    i_axi_px    : in unsigned(31 downto 0);

    o_rd_done   : out std_logic;
    o_axi_px    : out unsigned(31 downto 0);
    
    o_router_addr    : out unsigned(31 downto 0);
    o_router_x       : out unsigned(31 downto 0);
    o_router_y       : out unsigned(31 downto 0);
    o_router_s       : out unsigned(31 downto 0);
    o_router_f       : out unsigned(31 downto 0);
    o_router_px      : out unsigned(31 downto 0);
    ----------------------------------------------------
    -- ports to access from the Tile Router
    i_reset         : in std_logic;
    i_clk           : in std_logic;
    i_enable        : in std_logic;
    i_wr_enable     : in std_logic;

    i_router_x      : in  unsigned(img_width-1 downto 0);
    i_router_y      : in  unsigned(img_height-1 downto 0);
    i_router_s      : in  unsigned(n_steps-1 downto 0);
    i_router_f      : in  unsigned(n_frames-1 downto 0);
    i_router_px     : in  unsigned(pix_depth-1 downto 0);
    
    o_router_opx    : out unsigned(pix_depth-1 downto 0);
    o_router_fpx    : out std_logic;
    o_out_ok        : out std_logic
);
end entity bram_pixel_memory;

architecture behavioral of bram_pixel_memory is

    component rams_tdp_rf_rf
    generic(
        mem_size : natural
        );
    port(  
            clka    : in  std_logic;
            clkb    : in  std_logic;
            ena     : in  std_logic;
            enb     : in  std_logic;
            wea     : in  std_logic;
            web     : in  std_logic;
            addra   : in  std_logic_vector(17 downto 0);
            addrb   : in  std_logic_vector(17 downto 0);
            dia     : in  std_logic_vector(8 downto 0);
            dib     : in  std_logic_vector(8 downto 0);
            doa     : out std_logic_vector(8 downto 0);
            dob     : out std_logic_vector(8 downto 0);
            doa_ok  : out std_logic;
            dob_ok  : out std_logic 
        );
    end component;
        
    signal s_router_addr    : unsigned(31 downto 0);
    signal s_router_x       : unsigned(31 downto 0);
    signal s_router_y       : unsigned(31 downto 0);
    signal s_router_s       : unsigned(31 downto 0);
    signal s_router_f       : unsigned(31 downto 0);
    signal s_router_px      : unsigned(31 downto 0);
        
    signal s0_clka : std_logic;
    signal s0_ena  : std_logic;
    signal s0_wea  : std_logic;
    signal s0_addra: std_logic_vector(17 downto 0);
    signal s0_dia  : std_logic_vector(8 downto 0);
    signal s0_doa  : std_logic_vector(8 downto 0);
    signal s0_clkb : std_logic;
    signal s0_enb  : std_logic;
    signal s0_web  : std_logic;
    signal s0_addrb: std_logic_vector(17 downto 0);
    signal s0_dib  : std_logic_vector(8 downto 0);
    signal s0_dob  : std_logic_vector(8 downto 0);
    signal s0_doa_ok  : std_logic;   
    signal s0_dob_ok  : std_logic;
    signal so0_dob_ok  : std_logic;
    signal s1_clka : std_logic;
    signal s1_ena  : std_logic;
    signal s1_wea  : std_logic;
    signal s1_addra: std_logic_vector(17 downto 0);
    signal s1_dia  : std_logic_vector(8 downto 0);
    signal s1_doa  : std_logic_vector(8 downto 0);
    signal s1_clkb : std_logic;
    signal s1_enb  : std_logic;
    signal s1_web  : std_logic;
    signal s1_addrb: std_logic_vector(17 downto 0);
    signal s1_dib  : std_logic_vector(8 downto 0);
    signal s1_dob  : std_logic_vector(8 downto 0);
    signal s1_doa_ok  : std_logic;
    signal s1_dob_ok  : std_logic;
    signal so1_dob_ok  : std_logic;
    signal s2_clka : std_logic;
    signal s2_ena  : std_logic;
    signal s2_wea  : std_logic;
    signal s2_addra: std_logic_vector(17 downto 0);
    signal s2_dia  : std_logic_vector(8 downto 0);
    signal s2_doa  : std_logic_vector(8 downto 0);
    signal s2_clkb : std_logic;
    signal s2_enb  : std_logic;
    signal s2_web  : std_logic;
    signal s2_addrb: std_logic_vector(17 downto 0);
    signal s2_dib  : std_logic_vector(8 downto 0);
    signal s2_dob  : std_logic_vector(8 downto 0);
    signal s2_doa_ok  : std_logic;
    signal s2_dob_ok  : std_logic;
    signal so2_dob_ok  : std_logic;
    signal s3_clka : std_logic;
    signal s3_ena  : std_logic;
    signal s3_wea  : std_logic;
    signal s3_addra: std_logic_vector(17 downto 0);
    signal s3_dia  : std_logic_vector(8 downto 0);
    signal s3_doa  : std_logic_vector(8 downto 0);
    signal s3_clkb : std_logic;
    signal s3_enb  : std_logic;
    signal s3_web  : std_logic;
    signal s3_addrb: std_logic_vector(17 downto 0);
    signal s3_dib  : std_logic_vector(8 downto 0);
    signal s3_dob  : std_logic_vector(8 downto 0);
    signal s3_doa_ok  : std_logic;
    signal s3_dob_ok  : std_logic;
    signal so3_dob_ok  : std_logic;
                
    constant c_mem_size : natural := 2**(tile_width+tile_height+n_steps-2);
    signal mem_size : unsigned(31 downto 0)                 := to_unsigned(c_mem_size, 32);
    signal img_size : unsigned(img_height downto 0)         := to_unsigned(2**tile_height, img_height+1);
    signal steps    : unsigned(n_steps downto 0)            := to_unsigned(2**n_steps, n_steps+1);
    signal frames   : unsigned(n_frames downto 0)           := to_unsigned(n_frames, n_frames+1);

begin

    o_rd_done       <= s0_doa_ok or s1_doa_ok or s2_doa_ok or s3_doa_ok;
    o_out_ok        <= so0_dob_ok or so1_dob_ok or so2_dob_ok or so3_dob_ok;
    
    -- Clock sources
    s0_clka          <= i_axi_clk;
    s0_clkb          <= i_clk;
    s1_clka          <= i_axi_clk;
    s1_clkb          <= i_clk;
    s2_clka          <= i_axi_clk;
    s2_clkb          <= i_clk;
    s3_clka          <= i_axi_clk;
    s3_clkb          <= i_clk;

    PM_inst0: rams_tdp_rf_rf 
    generic map(
                mem_size    => c_mem_size
                )
    port map(
        clka => s0_clka,
        ena  => s0_ena,
        wea  => s0_wea,
        addra=> s0_addra,
        dia  => s0_dia,
        doa  => s0_doa,
        doa_ok => s0_doa_ok,
        
        clkb => s0_clkb,
        enb  => s0_enb,
        web  => s0_web,
        addrb=> s0_addrb,
        dib  => s0_dib,
        dob  => s0_dob,
        dob_ok => s0_dob_ok
    );

    PM_inst1: rams_tdp_rf_rf 
    generic map(
                mem_size    => c_mem_size
                )
    port map(
        clka => s1_clka,
        ena  => s1_ena,
        wea  => s1_wea,
        addra=> s1_addra,
        dia  => s1_dia,
        doa  => s1_doa,
        doa_ok => s1_doa_ok,
        
        clkb => s1_clkb,
        enb  => s1_enb,
        web  => s1_web,
        addrb=> s1_addrb,
        dib  => s1_dib,
        dob  => s1_dob,
        dob_ok => s1_dob_ok
    );

    PM_inst2: rams_tdp_rf_rf
    generic map(
                mem_size    => c_mem_size
                )
    port map(
        clka => s2_clka,
        ena  => s2_ena,
        wea  => s2_wea,
        addra=> s2_addra,
        dia  => s2_dia,
        doa  => s2_doa,
        doa_ok => s2_doa_ok,
        
        clkb => s2_clkb,
        enb  => s2_enb,
        web  => s2_web,
        addrb=> s2_addrb,
        dib  => s2_dib,
        dob  => s2_dob,
        dob_ok => s2_dob_ok
    );
    
    PM_inst3: rams_tdp_rf_rf
    generic map(
                mem_size    => c_mem_size
                )
    port map(
        clka => s3_clka,
        ena  => s3_ena,
        wea  => s3_wea,
        addra=> s3_addra,
        dia  => s3_dia,
        doa  => s3_doa,
        doa_ok => s3_doa_ok,
        
        clkb => s3_clkb,
        enb  => s3_enb,
        web  => s3_web,
        addrb=> s3_addrb,
        dib  => s3_dib,
        dob  => s3_dob,
        dob_ok => s3_dob_ok
    );

    s_router_x      <= to_unsigned(to_integer(i_router_x), s_router_x'length);
    s_router_y      <= to_unsigned(to_integer(i_router_y), s_router_y'length);
    s_router_s      <= to_unsigned(to_integer(i_router_s), s_router_s'length);
    s_router_f      <= to_unsigned(to_integer(i_router_f), s_router_f'length);
    s_router_px     <= to_unsigned(to_integer(i_router_px), s_router_px'length);
    s_router_addr   <= resize(img_size*(s_router_x-x_init)*steps*frames+(s_router_y-y_init)*steps*frames+s_router_s*frames+s_router_f,
                       s_router_addr'length);

    o_router_x      <= s_router_x;
    o_router_y      <= s_router_y;
    o_router_s      <= s_router_s;
    o_router_f      <= s_router_f;
    o_router_px     <= s_router_px;
    o_router_addr   <= s_router_addr;

    -- AXI memory part
    process(i_axi_en, i_axi_we,i_axi_x,i_axi_y,i_axi_s,i_axi_f,i_axi_px,s0_doa,s1_doa,s2_doa,s3_doa)
        variable s_axi_addr : unsigned(31 downto 0);
        variable temp_index : unsigned(s0_addra'length-1 downto 0);
    begin
        if(i_axi_en = '1') then
            s_axi_addr  :=  resize(img_size*(i_axi_x-x_init)*steps*frames+(i_axi_y-y_init)*steps*frames+i_axi_s*frames+i_axi_f, s_axi_addr'length);
            if(s_axi_addr < mem_size) then
                s0_ena <= '1';
                s0_addra     <= std_logic_vector(s_axi_addr(s0_addra'length-1 downto 0));
                o_axi_px    <= to_unsigned(to_integer(unsigned(s0_doa)),o_axi_px'length);
                if(i_axi_we = '1') then
                    s0_wea   <= '1';
                    s0_dia   <= std_logic_vector(to_signed(to_integer(i_axi_px), s0_dia'length));
                else
                    s0_wea   <= '0';
                end if;
            elsif(s_axi_addr >= mem_size) and (s_axi_addr < 2*mem_size) then
                s1_ena <= '1';
                temp_index := resize(s_axi_addr-mem_size, temp_index'length);
                s1_addra     <= std_logic_vector(temp_index);
                o_axi_px    <= to_unsigned(to_integer(unsigned(s1_doa)),o_axi_px'length);
                if(i_axi_we = '1') then
                    s1_wea   <= '1';
                    s1_dia   <= std_logic_vector(to_signed(to_integer(i_axi_px), s1_dia'length));
                else
                    s1_wea   <= '0';
                end if;            
            elsif(s_axi_addr >= 2*mem_size) and (s_axi_addr < 3*mem_size) then
                s2_ena <= '1';
                temp_index := resize(s_axi_addr-2*mem_size, temp_index'length);
                s2_addra     <= std_logic_vector(temp_index);
                o_axi_px    <= to_unsigned(to_integer(unsigned(s2_doa)),o_axi_px'length);
                if(i_axi_we = '1') then
                    s2_wea   <= '1';
                    s2_dia   <= std_logic_vector(to_signed(to_integer(i_axi_px), s2_dia'length));
                else
                    s2_wea   <= '0';
                end if;
            elsif(s_axi_addr >= 3*mem_size) and (s_axi_addr < 4*mem_size) then
                s3_ena <= '1';
                temp_index := resize(s_axi_addr-3*mem_size, temp_index'length);
                s3_addra     <= std_logic_vector(temp_index);
                o_axi_px    <= to_unsigned(to_integer(unsigned(s3_doa)),o_axi_px'length);
                if(i_axi_we = '1') then
                    s3_wea   <= '1';
                    s3_dia   <= std_logic_vector(to_signed(to_integer(i_axi_px), s3_dia'length));
                else
                    s3_wea   <= '0';
                end if;
            end if;
        else
            s0_ena <= '0';
            s1_ena <= '0';
            s2_ena <= '0';
            s3_ena <= '0';
        end if;
    end process;

    -- Router memory part
    process(i_reset,i_wr_enable,s_router_x,s_router_y,s_router_s,s_router_f,i_enable,s_router_px,s0_dob,s1_dob,s2_dob,s3_dob,
            s0_dob_ok,s1_dob_ok,s2_dob_ok,s3_dob_ok,s_router_addr)
        variable temp_index     : unsigned(s0_addrb'length-1 downto 0);
    begin
        if(i_reset='1')then
            o_router_opx <= (others=>'0');
        else
            if(i_enable='1')then
                if(s_router_addr < mem_size) then
                    s0_enb <= '1';
                    s0_addrb <= std_logic_vector(s_router_addr(s0_addrb'length-1 downto 0));
                    o_router_opx <= to_unsigned(to_integer(unsigned(s0_dob)),o_router_opx'length);
                    so0_dob_ok <= s0_dob_ok;
                    o_router_fpx <= '1';
                    if(i_wr_enable='1')then
                        s0_web <= '1';
                        s0_dib <= std_logic_vector(to_signed(to_integer(i_router_px(pix_depth-2 downto 0)), s0_dib'length));
                    else
                        s0_web <= '0';
                    end if;
                elsif(s_router_addr >= mem_size) and (s_router_addr < 2*mem_size) then
                    s1_enb <= '1';
                    temp_index := resize(s_router_addr-mem_size, temp_index'length);
                    s1_addrb <= std_logic_vector(temp_index);
                    o_router_opx <= to_unsigned(to_integer(unsigned(s1_dob)),o_router_opx'length);
                    so1_dob_ok <= s1_dob_ok;
                    o_router_fpx <= '1';
                    if(i_wr_enable='1')then
                        s1_web <= '1';
                        s1_dib <= std_logic_vector(to_signed(to_integer(i_router_px(pix_depth-2 downto 0)), s1_dib'length));
                    else
                        s1_web <= '0';
                    end if;
                elsif(s_router_addr >= 2*mem_size) and (s_router_addr < 3*mem_size) then
                    s2_enb <= '1';
                    temp_index := resize(s_router_addr-2*mem_size, temp_index'length);
                    s2_addrb <= std_logic_vector(temp_index);
                    o_router_opx <= to_unsigned(to_integer(unsigned(s2_dob)),o_router_opx'length);
                    so2_dob_ok <= s2_dob_ok;
                    o_router_fpx <= '1';
                    if(i_wr_enable='1')then
                        s2_web <= '1';
                        s2_dib <= std_logic_vector(to_signed(to_integer(i_router_px(pix_depth-2 downto 0)), s2_dib'length));
                    else
                        s2_web <= '0';
                    end if;
                elsif(s_router_addr >= 3*mem_size) and (s_router_addr < 4*mem_size) then
                    s3_enb <= '1';
                    temp_index := resize(s_router_addr-3*mem_size, temp_index'length);
                    s3_addrb <= std_logic_vector(temp_index);
                    o_router_opx <= to_unsigned(to_integer(unsigned(s3_dob)),o_router_opx'length);
                    so3_dob_ok <= s3_dob_ok;
                    o_router_fpx <= '1';
                    if(i_wr_enable='1')then
                        s3_web <= '1';
                        s3_dib <= std_logic_vector(to_signed(to_integer(i_router_px(pix_depth-2 downto 0)), s3_dib'length));
                    else
                        s3_web <= '0';
                    end if;
                end if;
            else
                s0_enb <= '0';
                s1_enb <= '0';
                s2_enb <= '0';
                s3_enb <= '0';
                o_router_fpx <= '0';
            end if;
        end if;
    end process;
       
end architecture behavioral;
