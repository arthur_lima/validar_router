---------------------------------------------------------------------- 
-- ManyCore auto-generated code.                                      
-- Pixel Length : 8                               
-- Image Width  : 6                               
-- Image Height : 6                               
-- Region Width : 2                               
-- Region Height: 2                               
-- Array of 3 x 3                           
----------------------------------------------------------------------
library ieee;                                                         
use ieee.std_logic_1164.all;                                          
use ieee.numeric_std.all;                                             
use work.parameters.all;                                              
                                                                      
entity ManyCore is                                                    
  port(                                                               
      clk : in std_logic;                                             
      reset : in std_logic;                                           
      done_0_0 : out std_logic;                           
      done_0_2 : out std_logic;                           
      done_0_4 : out std_logic;                           
      done_2_0 : out std_logic;                           
      done_2_2 : out std_logic;                           
      done_2_4 : out std_logic;                           
      done_4_0 : out std_logic;                           
      done_4_2 : out std_logic;                           
      done_4_4 : out std_logic                           
  );                                                                  
                                                                      
end entity ManyCore;                                                  
                                                                      
architecture behavioral of ManyCore is                                
                                                                      
       -- Programs file names
       constant File_0_0 : string := code_endpgr;
       constant File_0_2 : string := code_endpgr;
       constant File_0_4 : string := code_endpgr;
       constant File_2_0 : string := code_endpgr;
       constant File_2_2 : string := code_2_2;
       constant File_2_4 : string := code_endpgr;
       constant File_4_0 : string := code_endpgr;
       constant File_4_2 : string := code_endpgr;
       constant File_4_4 : string := code_endpgr;       
   
   -- Signals for Tile_0_0;                                                                                 
       -- connections to North                                                        
       signal Tile_0_0_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_N_fb     : std_logic;                                
       signal Tile_0_0_i_N_req    : std_logic;                                
       signal Tile_0_0_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_0_0_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_S_fb     : std_logic;                                
       signal Tile_0_0_i_S_req    : std_logic;                                
       signal Tile_0_0_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_0_0_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_E_fb     : std_logic;                                
       signal Tile_0_0_i_E_req    : std_logic;                                
       signal Tile_0_0_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_0_0_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_0_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_0_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_0_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_0_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_0_i_W_fb     : std_logic;                                
       signal Tile_0_0_i_W_req    : std_logic;                                
       signal Tile_0_0_i_W_ack    : std_logic;                                
   -- Signals for Tile_0_2;                                                   
       -- connections to North                                                        
       signal Tile_0_2_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_N_fb     : std_logic;                                
       signal Tile_0_2_i_N_req    : std_logic;                                
       signal Tile_0_2_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_0_2_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_S_fb     : std_logic;                                
       signal Tile_0_2_i_S_req    : std_logic;                                
       signal Tile_0_2_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_0_2_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_E_fb     : std_logic;                                
       signal Tile_0_2_i_E_req    : std_logic;                                
       signal Tile_0_2_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_0_2_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_2_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_2_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_2_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_2_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_2_i_W_fb     : std_logic;                                
       signal Tile_0_2_i_W_req    : std_logic;                                
       signal Tile_0_2_i_W_ack    : std_logic;                                
   -- Signals for Tile_0_4;                                                   
       -- connections to North                                                        
       signal Tile_0_4_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_4_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_4_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_4_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_N_fb     : std_logic;                                
       signal Tile_0_4_i_N_req    : std_logic;                                
       signal Tile_0_4_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_0_4_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_4_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_4_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_4_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_S_fb     : std_logic;                                
       signal Tile_0_4_i_S_req    : std_logic;                                
       signal Tile_0_4_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_0_4_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_4_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_4_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_4_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_E_fb     : std_logic;                                
       signal Tile_0_4_i_E_req    : std_logic;                                
       signal Tile_0_4_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_0_4_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_0_4_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_0_4_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_0_4_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_0_4_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_0_4_i_W_fb     : std_logic;                                
       signal Tile_0_4_i_W_req    : std_logic;                                
       signal Tile_0_4_i_W_ack    : std_logic;                                
   -- Signals for Tile_2_0;                                                   
       -- connections to North                                                        
       signal Tile_2_0_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_N_fb     : std_logic;                                
       signal Tile_2_0_i_N_req    : std_logic;                                
       signal Tile_2_0_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_2_0_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_S_fb     : std_logic;                                
       signal Tile_2_0_i_S_req    : std_logic;                                
       signal Tile_2_0_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_2_0_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_E_fb     : std_logic;                                
       signal Tile_2_0_i_E_req    : std_logic;                                
       signal Tile_2_0_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_2_0_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_0_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_0_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_0_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_0_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_0_i_W_fb     : std_logic;                                
       signal Tile_2_0_i_W_req    : std_logic;                                
       signal Tile_2_0_i_W_ack    : std_logic;                                
   -- Signals for Tile_2_2;                                                   
       -- connections to North                                                        
       signal Tile_2_2_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_N_fb     : std_logic;                                
       signal Tile_2_2_i_N_req    : std_logic;                                
       signal Tile_2_2_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_2_2_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_S_fb     : std_logic;                                
       signal Tile_2_2_i_S_req    : std_logic;                                
       signal Tile_2_2_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_2_2_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_E_fb     : std_logic;                                
       signal Tile_2_2_i_E_req    : std_logic;                                
       signal Tile_2_2_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_2_2_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_2_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_2_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_2_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_2_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_2_i_W_fb     : std_logic;                                
       signal Tile_2_2_i_W_req    : std_logic;                                
       signal Tile_2_2_i_W_ack    : std_logic;                                
   -- Signals for Tile_2_4;                                                   
       -- connections to North                                                        
       signal Tile_2_4_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_4_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_4_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_4_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_N_fb     : std_logic;                                
       signal Tile_2_4_i_N_req    : std_logic;                                
       signal Tile_2_4_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_2_4_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_4_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_4_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_4_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_S_fb     : std_logic;                                
       signal Tile_2_4_i_S_req    : std_logic;                                
       signal Tile_2_4_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_2_4_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_4_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_4_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_4_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_E_fb     : std_logic;                                
       signal Tile_2_4_i_E_req    : std_logic;                                
       signal Tile_2_4_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_2_4_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_2_4_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_2_4_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_2_4_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_2_4_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_2_4_i_W_fb     : std_logic;                                
       signal Tile_2_4_i_W_req    : std_logic;                                
       signal Tile_2_4_i_W_ack    : std_logic;                                
   -- Signals for Tile_4_0;                                                   
       -- connections to North                                                        
       signal Tile_4_0_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_0_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_0_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_0_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_N_fb     : std_logic;                                
       signal Tile_4_0_i_N_req    : std_logic;                                
       signal Tile_4_0_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_4_0_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_0_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_0_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_0_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_S_fb     : std_logic;                                
       signal Tile_4_0_i_S_req    : std_logic;                                
       signal Tile_4_0_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_4_0_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_0_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_0_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_0_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_E_fb     : std_logic;                                
       signal Tile_4_0_i_E_req    : std_logic;                                
       signal Tile_4_0_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_4_0_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_0_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_0_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_0_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_0_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_0_i_W_fb     : std_logic;                                
       signal Tile_4_0_i_W_req    : std_logic;                                
       signal Tile_4_0_i_W_ack    : std_logic;                                
   -- Signals for Tile_4_2;                                                   
       -- connections to North                                                        
       signal Tile_4_2_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_2_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_2_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_2_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_N_fb     : std_logic;                                
       signal Tile_4_2_i_N_req    : std_logic;                                
       signal Tile_4_2_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_4_2_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_2_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_2_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_2_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_S_fb     : std_logic;                                
       signal Tile_4_2_i_S_req    : std_logic;                                
       signal Tile_4_2_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_4_2_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_2_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_2_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_2_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_E_fb     : std_logic;                                
       signal Tile_4_2_i_E_req    : std_logic;                                
       signal Tile_4_2_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_4_2_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_2_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_2_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_2_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_2_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_2_i_W_fb     : std_logic;                                
       signal Tile_4_2_i_W_req    : std_logic;                                
       signal Tile_4_2_i_W_ack    : std_logic;                                
   -- Signals for Tile_4_4;                                                   
       -- connections to North                                                        
       signal Tile_4_4_i_N_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_4_i_N_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_N_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_N_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_4_i_N_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_4_i_N_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_N_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_N_fb     : std_logic;                                
       signal Tile_4_4_i_N_req    : std_logic;                                
       signal Tile_4_4_i_N_ack    : std_logic;                                
       -- connections to South                                                        
       signal Tile_4_4_i_S_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_4_i_S_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_S_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_S_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_4_i_S_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_4_i_S_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_S_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_S_fb     : std_logic;                                
       signal Tile_4_4_i_S_req    : std_logic;                                
       signal Tile_4_4_i_S_ack    : std_logic;                                
       -- connections to East                                                         
       signal Tile_4_4_i_E_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_4_i_E_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_E_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_E_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_4_i_E_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_4_i_E_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_E_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_E_fb     : std_logic;                                
       signal Tile_4_4_i_E_req    : std_logic;                                
       signal Tile_4_4_i_E_ack    : std_logic;                                
       -- connections to West                                                         
       signal Tile_4_4_i_W_pixel  : std_logic_vector(pix_depth -1 downto 0);  
       signal Tile_4_4_i_W_x_dest : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_W_y_dest : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_W_step   : std_logic_vector(n_steps   -1 downto 0);  
       signal Tile_4_4_i_W_frame  : std_logic_vector(n_frames  -1 downto 0);  
       signal Tile_4_4_i_W_x_orig : std_logic_vector(img_width -1 downto 0);  
       signal Tile_4_4_i_W_y_orig : std_logic_vector(img_height-1 downto 0);  
       signal Tile_4_4_i_W_fb     : std_logic;                                
       signal Tile_4_4_i_W_req    : std_logic;                                
       signal Tile_4_4_i_W_ack    : std_logic;                                
begin                                                                 
                                                                      

-----------------------------------------------------------
Tile_0_0 : entity work.tile               
     generic map(                                 
         y_init     => 0,                         
         x_init     => 0,
         file_name  => File_0_0            
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_0_0,                   
         -- North connections                     
         i_N_pixel  => Tile_0_0_i_N_pixel, 
         i_N_x_dest => Tile_0_0_i_N_x_dest,
         i_N_y_dest => Tile_0_0_i_N_y_dest,
         i_N_step   => Tile_0_0_i_N_step,  
         i_N_frame  => Tile_0_0_i_N_frame, 
         i_N_x_orig => Tile_0_0_i_N_x_orig,
         i_N_y_orig => Tile_0_0_i_N_y_orig,
         i_N_fb     => Tile_0_0_i_N_fb,    
         i_N_req    => Tile_0_0_i_N_req,   
         i_N_ack    => Tile_0_0_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_0_i_N_pixel, 
         t_N_x_dest => Tile_0_0_i_N_x_dest,
         t_N_y_dest => Tile_0_0_i_N_y_dest,
         t_N_step   => Tile_0_0_i_N_step,  
         t_N_frame  => Tile_0_0_i_N_frame, 
         t_N_x_orig => Tile_0_0_i_N_x_orig,
         t_N_y_orig => Tile_0_0_i_N_y_orig,
         t_N_fb     => Tile_0_0_i_N_fb,    
         t_N_req    => Tile_0_0_i_N_req,   
         t_N_ack    => Tile_0_0_i_N_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_0_0_i_S_pixel, 
         i_S_x_dest => Tile_0_0_i_S_x_dest,
         i_S_y_dest => Tile_0_0_i_S_y_dest,
         i_S_step   => Tile_0_0_i_S_step,  
         i_S_frame  => Tile_0_0_i_S_frame, 
         i_S_x_orig => Tile_0_0_i_S_x_orig,
         i_S_y_orig => Tile_0_0_i_S_y_orig,
         i_S_fb     => Tile_0_0_i_S_fb,    
         i_S_req    => Tile_0_0_i_S_req,   
         i_S_ack    => Tile_0_0_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_0_i_N_pixel, 
         t_S_x_dest => Tile_2_0_i_N_x_dest,
         t_S_y_dest => Tile_2_0_i_N_y_dest,
         t_S_step   => Tile_2_0_i_N_step,  
         t_S_frame  => Tile_2_0_i_N_frame, 
         t_S_x_orig => Tile_2_0_i_N_x_orig,
         t_S_y_orig => Tile_2_0_i_N_y_orig,
         t_S_fb     => Tile_2_0_i_N_fb,    
         t_S_req    => Tile_2_0_i_N_req,   
         t_S_ack    => Tile_2_0_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_0_0_i_E_pixel, 
         i_E_x_dest => Tile_0_0_i_E_x_dest,
         i_E_y_dest => Tile_0_0_i_E_y_dest,
         i_E_step   => Tile_0_0_i_E_step,  
         i_E_frame  => Tile_0_0_i_E_frame, 
         i_E_x_orig => Tile_0_0_i_E_x_orig,
         i_E_y_orig => Tile_0_0_i_E_y_orig,
         i_E_fb     => Tile_0_0_i_E_fb,    
         i_E_req    => Tile_0_0_i_E_req,   
         i_E_ack    => Tile_0_0_i_E_ack,   
                                                   
         t_E_pixel  => Tile_0_2_i_W_pixel, 
         t_E_x_dest => Tile_0_2_i_W_x_dest,
         t_E_y_dest => Tile_0_2_i_W_y_dest,
         t_E_step   => Tile_0_2_i_W_step,  
         t_E_frame  => Tile_0_2_i_W_frame, 
         t_E_x_orig => Tile_0_2_i_W_x_orig,
         t_E_y_orig => Tile_0_2_i_W_y_orig,
         t_E_fb     => Tile_0_2_i_W_fb,    
         t_E_req    => Tile_0_2_i_W_req,   
         t_E_ack    => Tile_0_2_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_0_0_i_W_pixel, 
         i_W_x_dest => Tile_0_0_i_W_x_dest,
         i_W_y_dest => Tile_0_0_i_W_y_dest,
         i_W_step   => Tile_0_0_i_W_step,  
         i_W_frame  => Tile_0_0_i_W_frame, 
         i_W_x_orig => Tile_0_0_i_W_x_orig,
         i_W_y_orig => Tile_0_0_i_W_y_orig,
         i_W_fb     => Tile_0_0_i_W_fb,    
         i_W_req    => Tile_0_0_i_W_req,   
         i_W_ack    => Tile_0_0_i_W_ack,   
                                                   
         t_W_pixel  => Tile_0_0_i_W_pixel, 
         t_W_x_dest => Tile_0_0_i_W_x_dest,
         t_W_y_dest => Tile_0_0_i_W_y_dest,
         t_W_step   => Tile_0_0_i_W_step,  
         t_W_frame  => Tile_0_0_i_W_frame, 
         t_W_x_orig => Tile_0_0_i_W_x_orig,
         t_W_y_orig => Tile_0_0_i_W_y_orig,
         t_W_fb     => Tile_0_0_i_W_fb,    
         t_W_req    => Tile_0_0_i_W_req,   
         t_W_ack    => Tile_0_0_i_W_ack    
     );                                                
-----------------------------------------------------------
Tile_0_2 : entity work.tile               
     generic map(                                 
         y_init     => 0,                         
         x_init     => 2,
         file_name  => File_0_2                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_0_2,                   
         -- North connections                     
         i_N_pixel  => Tile_0_2_i_N_pixel, 
         i_N_x_dest => Tile_0_2_i_N_x_dest,
         i_N_y_dest => Tile_0_2_i_N_y_dest,
         i_N_step   => Tile_0_2_i_N_step,  
         i_N_frame  => Tile_0_2_i_N_frame, 
         i_N_x_orig => Tile_0_2_i_N_x_orig,
         i_N_y_orig => Tile_0_2_i_N_y_orig,
         i_N_fb     => Tile_0_2_i_N_fb,    
         i_N_req    => Tile_0_2_i_N_req,   
         i_N_ack    => Tile_0_2_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_2_i_N_pixel, 
         t_N_x_dest => Tile_0_2_i_N_x_dest,
         t_N_y_dest => Tile_0_2_i_N_y_dest,
         t_N_step   => Tile_0_2_i_N_step,  
         t_N_frame  => Tile_0_2_i_N_frame, 
         t_N_x_orig => Tile_0_2_i_N_x_orig,
         t_N_y_orig => Tile_0_2_i_N_y_orig,
         t_N_fb     => Tile_0_2_i_N_fb,    
         t_N_req    => Tile_0_2_i_N_req,   
         t_N_ack    => Tile_0_2_i_N_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_0_2_i_S_pixel, 
         i_S_x_dest => Tile_0_2_i_S_x_dest,
         i_S_y_dest => Tile_0_2_i_S_y_dest,
         i_S_step   => Tile_0_2_i_S_step,  
         i_S_frame  => Tile_0_2_i_S_frame, 
         i_S_x_orig => Tile_0_2_i_S_x_orig,
         i_S_y_orig => Tile_0_2_i_S_y_orig,
         i_S_fb     => Tile_0_2_i_S_fb,    
         i_S_req    => Tile_0_2_i_S_req,   
         i_S_ack    => Tile_0_2_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_2_i_N_pixel, 
         t_S_x_dest => Tile_2_2_i_N_x_dest,
         t_S_y_dest => Tile_2_2_i_N_y_dest,
         t_S_step   => Tile_2_2_i_N_step,  
         t_S_frame  => Tile_2_2_i_N_frame, 
         t_S_x_orig => Tile_2_2_i_N_x_orig,
         t_S_y_orig => Tile_2_2_i_N_y_orig,
         t_S_fb     => Tile_2_2_i_N_fb,    
         t_S_req    => Tile_2_2_i_N_req,   
         t_S_ack    => Tile_2_2_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_0_2_i_E_pixel, 
         i_E_x_dest => Tile_0_2_i_E_x_dest,
         i_E_y_dest => Tile_0_2_i_E_y_dest,
         i_E_step   => Tile_0_2_i_E_step,  
         i_E_frame  => Tile_0_2_i_E_frame, 
         i_E_x_orig => Tile_0_2_i_E_x_orig,
         i_E_y_orig => Tile_0_2_i_E_y_orig,
         i_E_fb     => Tile_0_2_i_E_fb,    
         i_E_req    => Tile_0_2_i_E_req,   
         i_E_ack    => Tile_0_2_i_E_ack,   
                                                   
         t_E_pixel  => Tile_0_4_i_W_pixel, 
         t_E_x_dest => Tile_0_4_i_W_x_dest,
         t_E_y_dest => Tile_0_4_i_W_y_dest,
         t_E_step   => Tile_0_4_i_W_step,  
         t_E_frame  => Tile_0_4_i_W_frame, 
         t_E_x_orig => Tile_0_4_i_W_x_orig,
         t_E_y_orig => Tile_0_4_i_W_y_orig,
         t_E_fb     => Tile_0_4_i_W_fb,    
         t_E_req    => Tile_0_4_i_W_req,   
         t_E_ack    => Tile_0_4_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_0_2_i_W_pixel, 
         i_W_x_dest => Tile_0_2_i_W_x_dest,
         i_W_y_dest => Tile_0_2_i_W_y_dest,
         i_W_step   => Tile_0_2_i_W_step,  
         i_W_frame  => Tile_0_2_i_W_frame, 
         i_W_x_orig => Tile_0_2_i_W_x_orig,
         i_W_y_orig => Tile_0_2_i_W_y_orig,
         i_W_fb     => Tile_0_2_i_W_fb,    
         i_W_req    => Tile_0_2_i_W_req,   
         i_W_ack    => Tile_0_2_i_W_ack,   
                                                   
         t_W_pixel  => Tile_0_0_i_E_pixel, 
         t_W_x_dest => Tile_0_0_i_E_x_dest,
         t_W_y_dest => Tile_0_0_i_E_y_dest,
         t_W_step   => Tile_0_0_i_E_step,  
         t_W_frame  => Tile_0_0_i_E_frame, 
         t_W_x_orig => Tile_0_0_i_E_x_orig,
         t_W_y_orig => Tile_0_0_i_E_y_orig,
         t_W_fb     => Tile_0_0_i_E_fb,    
         t_W_req    => Tile_0_0_i_E_req,   
         t_W_ack    => Tile_0_0_i_E_ack    
     );                                                
-----------------------------------------------------------
Tile_0_4 : entity work.tile               
     generic map(                                 
         y_init     => 0,                         
         x_init     => 4,
         file_name  => File_0_4                  
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_0_4,                   
         -- North connections                     
         i_N_pixel  => Tile_0_4_i_N_pixel, 
         i_N_x_dest => Tile_0_4_i_N_x_dest,
         i_N_y_dest => Tile_0_4_i_N_y_dest,
         i_N_step   => Tile_0_4_i_N_step,  
         i_N_frame  => Tile_0_4_i_N_frame, 
         i_N_x_orig => Tile_0_4_i_N_x_orig,
         i_N_y_orig => Tile_0_4_i_N_y_orig,
         i_N_fb     => Tile_0_4_i_N_fb,    
         i_N_req    => Tile_0_4_i_N_req,   
         i_N_ack    => Tile_0_4_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_4_i_N_pixel, 
         t_N_x_dest => Tile_0_4_i_N_x_dest,
         t_N_y_dest => Tile_0_4_i_N_y_dest,
         t_N_step   => Tile_0_4_i_N_step,  
         t_N_frame  => Tile_0_4_i_N_frame, 
         t_N_x_orig => Tile_0_4_i_N_x_orig,
         t_N_y_orig => Tile_0_4_i_N_y_orig,
         t_N_fb     => Tile_0_4_i_N_fb,    
         t_N_req    => Tile_0_4_i_N_req,   
         t_N_ack    => Tile_0_4_i_N_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_0_4_i_S_pixel, 
         i_S_x_dest => Tile_0_4_i_S_x_dest,
         i_S_y_dest => Tile_0_4_i_S_y_dest,
         i_S_step   => Tile_0_4_i_S_step,  
         i_S_frame  => Tile_0_4_i_S_frame, 
         i_S_x_orig => Tile_0_4_i_S_x_orig,
         i_S_y_orig => Tile_0_4_i_S_y_orig,
         i_S_fb     => Tile_0_4_i_S_fb,    
         i_S_req    => Tile_0_4_i_S_req,   
         i_S_ack    => Tile_0_4_i_S_ack,   
                                                   
         t_S_pixel  => Tile_2_4_i_N_pixel, 
         t_S_x_dest => Tile_2_4_i_N_x_dest,
         t_S_y_dest => Tile_2_4_i_N_y_dest,
         t_S_step   => Tile_2_4_i_N_step,  
         t_S_frame  => Tile_2_4_i_N_frame, 
         t_S_x_orig => Tile_2_4_i_N_x_orig,
         t_S_y_orig => Tile_2_4_i_N_y_orig,
         t_S_fb     => Tile_2_4_i_N_fb,    
         t_S_req    => Tile_2_4_i_N_req,   
         t_S_ack    => Tile_2_4_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_0_4_i_E_pixel, 
         i_E_x_dest => Tile_0_4_i_E_x_dest,
         i_E_y_dest => Tile_0_4_i_E_y_dest,
         i_E_step   => Tile_0_4_i_E_step,  
         i_E_frame  => Tile_0_4_i_E_frame, 
         i_E_x_orig => Tile_0_4_i_E_x_orig,
         i_E_y_orig => Tile_0_4_i_E_y_orig,
         i_E_fb     => Tile_0_4_i_E_fb,    
         i_E_req    => Tile_0_4_i_E_req,   
         i_E_ack    => Tile_0_4_i_E_ack,   
                                                   
         t_E_pixel  => Tile_0_4_i_E_pixel, 
         t_E_x_dest => Tile_0_4_i_E_x_dest,
         t_E_y_dest => Tile_0_4_i_E_y_dest,
         t_E_step   => Tile_0_4_i_E_step,  
         t_E_frame  => Tile_0_4_i_E_frame, 
         t_E_x_orig => Tile_0_4_i_E_x_orig,
         t_E_y_orig => Tile_0_4_i_E_y_orig,
         t_E_fb     => Tile_0_4_i_E_fb,    
         t_E_req    => Tile_0_4_i_E_req,   
         t_E_ack    => Tile_0_4_i_E_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_0_4_i_W_pixel, 
         i_W_x_dest => Tile_0_4_i_W_x_dest,
         i_W_y_dest => Tile_0_4_i_W_y_dest,
         i_W_step   => Tile_0_4_i_W_step,  
         i_W_frame  => Tile_0_4_i_W_frame, 
         i_W_x_orig => Tile_0_4_i_W_x_orig,
         i_W_y_orig => Tile_0_4_i_W_y_orig,
         i_W_fb     => Tile_0_4_i_W_fb,    
         i_W_req    => Tile_0_4_i_W_req,   
         i_W_ack    => Tile_0_4_i_W_ack,   
                                                   
         t_W_pixel  => Tile_0_2_i_E_pixel, 
         t_W_x_dest => Tile_0_2_i_E_x_dest,
         t_W_y_dest => Tile_0_2_i_E_y_dest,
         t_W_step   => Tile_0_2_i_E_step,  
         t_W_frame  => Tile_0_2_i_E_frame, 
         t_W_x_orig => Tile_0_2_i_E_x_orig,
         t_W_y_orig => Tile_0_2_i_E_y_orig,
         t_W_fb     => Tile_0_2_i_E_fb,    
         t_W_req    => Tile_0_2_i_E_req,   
         t_W_ack    => Tile_0_2_i_E_ack    
     );                                                
-----------------------------------------------------------
Tile_2_0 : entity work.tile               
     generic map(                                 
         y_init     => 2,                         
         x_init     => 0,
         file_name  => File_2_0                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_2_0,                   
         -- North connections                     
         i_N_pixel  => Tile_2_0_i_N_pixel, 
         i_N_x_dest => Tile_2_0_i_N_x_dest,
         i_N_y_dest => Tile_2_0_i_N_y_dest,
         i_N_step   => Tile_2_0_i_N_step,  
         i_N_frame  => Tile_2_0_i_N_frame, 
         i_N_x_orig => Tile_2_0_i_N_x_orig,
         i_N_y_orig => Tile_2_0_i_N_y_orig,
         i_N_fb     => Tile_2_0_i_N_fb,    
         i_N_req    => Tile_2_0_i_N_req,   
         i_N_ack    => Tile_2_0_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_0_i_S_pixel, 
         t_N_x_dest => Tile_0_0_i_S_x_dest,
         t_N_y_dest => Tile_0_0_i_S_y_dest,
         t_N_step   => Tile_0_0_i_S_step,  
         t_N_frame  => Tile_0_0_i_S_frame, 
         t_N_x_orig => Tile_0_0_i_S_x_orig,
         t_N_y_orig => Tile_0_0_i_S_y_orig,
         t_N_fb     => Tile_0_0_i_S_fb,    
         t_N_req    => Tile_0_0_i_S_req,   
         t_N_ack    => Tile_0_0_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_2_0_i_S_pixel, 
         i_S_x_dest => Tile_2_0_i_S_x_dest,
         i_S_y_dest => Tile_2_0_i_S_y_dest,
         i_S_step   => Tile_2_0_i_S_step,  
         i_S_frame  => Tile_2_0_i_S_frame, 
         i_S_x_orig => Tile_2_0_i_S_x_orig,
         i_S_y_orig => Tile_2_0_i_S_y_orig,
         i_S_fb     => Tile_2_0_i_S_fb,    
         i_S_req    => Tile_2_0_i_S_req,   
         i_S_ack    => Tile_2_0_i_S_ack,   
                                                   
         t_S_pixel  => Tile_4_0_i_N_pixel, 
         t_S_x_dest => Tile_4_0_i_N_x_dest,
         t_S_y_dest => Tile_4_0_i_N_y_dest,
         t_S_step   => Tile_4_0_i_N_step,  
         t_S_frame  => Tile_4_0_i_N_frame, 
         t_S_x_orig => Tile_4_0_i_N_x_orig,
         t_S_y_orig => Tile_4_0_i_N_y_orig,
         t_S_fb     => Tile_4_0_i_N_fb,    
         t_S_req    => Tile_4_0_i_N_req,   
         t_S_ack    => Tile_4_0_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_2_0_i_E_pixel, 
         i_E_x_dest => Tile_2_0_i_E_x_dest,
         i_E_y_dest => Tile_2_0_i_E_y_dest,
         i_E_step   => Tile_2_0_i_E_step,  
         i_E_frame  => Tile_2_0_i_E_frame, 
         i_E_x_orig => Tile_2_0_i_E_x_orig,
         i_E_y_orig => Tile_2_0_i_E_y_orig,
         i_E_fb     => Tile_2_0_i_E_fb,    
         i_E_req    => Tile_2_0_i_E_req,   
         i_E_ack    => Tile_2_0_i_E_ack,   
                                                   
         t_E_pixel  => Tile_2_2_i_W_pixel, 
         t_E_x_dest => Tile_2_2_i_W_x_dest,
         t_E_y_dest => Tile_2_2_i_W_y_dest,
         t_E_step   => Tile_2_2_i_W_step,  
         t_E_frame  => Tile_2_2_i_W_frame, 
         t_E_x_orig => Tile_2_2_i_W_x_orig,
         t_E_y_orig => Tile_2_2_i_W_y_orig,
         t_E_fb     => Tile_2_2_i_W_fb,    
         t_E_req    => Tile_2_2_i_W_req,   
         t_E_ack    => Tile_2_2_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_2_0_i_W_pixel, 
         i_W_x_dest => Tile_2_0_i_W_x_dest,
         i_W_y_dest => Tile_2_0_i_W_y_dest,
         i_W_step   => Tile_2_0_i_W_step,  
         i_W_frame  => Tile_2_0_i_W_frame, 
         i_W_x_orig => Tile_2_0_i_W_x_orig,
         i_W_y_orig => Tile_2_0_i_W_y_orig,
         i_W_fb     => Tile_2_0_i_W_fb,    
         i_W_req    => Tile_2_0_i_W_req,   
         i_W_ack    => Tile_2_0_i_W_ack,   
                                                   
         t_W_pixel  => Tile_2_0_i_W_pixel, 
         t_W_x_dest => Tile_2_0_i_W_x_dest,
         t_W_y_dest => Tile_2_0_i_W_y_dest,
         t_W_step   => Tile_2_0_i_W_step,  
         t_W_frame  => Tile_2_0_i_W_frame, 
         t_W_x_orig => Tile_2_0_i_W_x_orig,
         t_W_y_orig => Tile_2_0_i_W_y_orig,
         t_W_fb     => Tile_2_0_i_W_fb,    
         t_W_req    => Tile_2_0_i_W_req,   
         t_W_ack    => Tile_2_0_i_W_ack    
     );                                                
-----------------------------------------------------------
Tile_2_2 : entity work.tile               
     generic map(                                 
         y_init     => 2,                         
         x_init     => 2,
         file_name  => File_2_2                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_2_2,                   
         -- North connections                     
         i_N_pixel  => Tile_2_2_i_N_pixel, 
         i_N_x_dest => Tile_2_2_i_N_x_dest,
         i_N_y_dest => Tile_2_2_i_N_y_dest,
         i_N_step   => Tile_2_2_i_N_step,  
         i_N_frame  => Tile_2_2_i_N_frame, 
         i_N_x_orig => Tile_2_2_i_N_x_orig,
         i_N_y_orig => Tile_2_2_i_N_y_orig,
         i_N_fb     => Tile_2_2_i_N_fb,    
         i_N_req    => Tile_2_2_i_N_req,   
         i_N_ack    => Tile_2_2_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_2_i_S_pixel, 
         t_N_x_dest => Tile_0_2_i_S_x_dest,
         t_N_y_dest => Tile_0_2_i_S_y_dest,
         t_N_step   => Tile_0_2_i_S_step,  
         t_N_frame  => Tile_0_2_i_S_frame, 
         t_N_x_orig => Tile_0_2_i_S_x_orig,
         t_N_y_orig => Tile_0_2_i_S_y_orig,
         t_N_fb     => Tile_0_2_i_S_fb,    
         t_N_req    => Tile_0_2_i_S_req,   
         t_N_ack    => Tile_0_2_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_2_2_i_S_pixel, 
         i_S_x_dest => Tile_2_2_i_S_x_dest,
         i_S_y_dest => Tile_2_2_i_S_y_dest,
         i_S_step   => Tile_2_2_i_S_step,  
         i_S_frame  => Tile_2_2_i_S_frame, 
         i_S_x_orig => Tile_2_2_i_S_x_orig,
         i_S_y_orig => Tile_2_2_i_S_y_orig,
         i_S_fb     => Tile_2_2_i_S_fb,    
         i_S_req    => Tile_2_2_i_S_req,   
         i_S_ack    => Tile_2_2_i_S_ack,   
                                                   
         t_S_pixel  => Tile_4_2_i_N_pixel, 
         t_S_x_dest => Tile_4_2_i_N_x_dest,
         t_S_y_dest => Tile_4_2_i_N_y_dest,
         t_S_step   => Tile_4_2_i_N_step,  
         t_S_frame  => Tile_4_2_i_N_frame, 
         t_S_x_orig => Tile_4_2_i_N_x_orig,
         t_S_y_orig => Tile_4_2_i_N_y_orig,
         t_S_fb     => Tile_4_2_i_N_fb,    
         t_S_req    => Tile_4_2_i_N_req,   
         t_S_ack    => Tile_4_2_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_2_2_i_E_pixel, 
         i_E_x_dest => Tile_2_2_i_E_x_dest,
         i_E_y_dest => Tile_2_2_i_E_y_dest,
         i_E_step   => Tile_2_2_i_E_step,  
         i_E_frame  => Tile_2_2_i_E_frame, 
         i_E_x_orig => Tile_2_2_i_E_x_orig,
         i_E_y_orig => Tile_2_2_i_E_y_orig,
         i_E_fb     => Tile_2_2_i_E_fb,    
         i_E_req    => Tile_2_2_i_E_req,   
         i_E_ack    => Tile_2_2_i_E_ack,   
                                                   
         t_E_pixel  => Tile_2_4_i_W_pixel, 
         t_E_x_dest => Tile_2_4_i_W_x_dest,
         t_E_y_dest => Tile_2_4_i_W_y_dest,
         t_E_step   => Tile_2_4_i_W_step,  
         t_E_frame  => Tile_2_4_i_W_frame, 
         t_E_x_orig => Tile_2_4_i_W_x_orig,
         t_E_y_orig => Tile_2_4_i_W_y_orig,
         t_E_fb     => Tile_2_4_i_W_fb,    
         t_E_req    => Tile_2_4_i_W_req,   
         t_E_ack    => Tile_2_4_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_2_2_i_W_pixel, 
         i_W_x_dest => Tile_2_2_i_W_x_dest,
         i_W_y_dest => Tile_2_2_i_W_y_dest,
         i_W_step   => Tile_2_2_i_W_step,  
         i_W_frame  => Tile_2_2_i_W_frame, 
         i_W_x_orig => Tile_2_2_i_W_x_orig,
         i_W_y_orig => Tile_2_2_i_W_y_orig,
         i_W_fb     => Tile_2_2_i_W_fb,    
         i_W_req    => Tile_2_2_i_W_req,   
         i_W_ack    => Tile_2_2_i_W_ack,   
                                                   
         t_W_pixel  => Tile_2_0_i_E_pixel, 
         t_W_x_dest => Tile_2_0_i_E_x_dest,
         t_W_y_dest => Tile_2_0_i_E_y_dest,
         t_W_step   => Tile_2_0_i_E_step,  
         t_W_frame  => Tile_2_0_i_E_frame, 
         t_W_x_orig => Tile_2_0_i_E_x_orig,
         t_W_y_orig => Tile_2_0_i_E_y_orig,
         t_W_fb     => Tile_2_0_i_E_fb,    
         t_W_req    => Tile_2_0_i_E_req,   
         t_W_ack    => Tile_2_0_i_E_ack    
     );                                                
-----------------------------------------------------------
Tile_2_4 : entity work.tile               
     generic map(                                 
         y_init     => 2,                         
         x_init     => 4,
         file_name  => File_2_4                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_2_4,                   
         -- North connections                     
         i_N_pixel  => Tile_2_4_i_N_pixel, 
         i_N_x_dest => Tile_2_4_i_N_x_dest,
         i_N_y_dest => Tile_2_4_i_N_y_dest,
         i_N_step   => Tile_2_4_i_N_step,  
         i_N_frame  => Tile_2_4_i_N_frame, 
         i_N_x_orig => Tile_2_4_i_N_x_orig,
         i_N_y_orig => Tile_2_4_i_N_y_orig,
         i_N_fb     => Tile_2_4_i_N_fb,    
         i_N_req    => Tile_2_4_i_N_req,   
         i_N_ack    => Tile_2_4_i_N_ack,   
                                                   
         t_N_pixel  => Tile_0_4_i_S_pixel, 
         t_N_x_dest => Tile_0_4_i_S_x_dest,
         t_N_y_dest => Tile_0_4_i_S_y_dest,
         t_N_step   => Tile_0_4_i_S_step,  
         t_N_frame  => Tile_0_4_i_S_frame, 
         t_N_x_orig => Tile_0_4_i_S_x_orig,
         t_N_y_orig => Tile_0_4_i_S_y_orig,
         t_N_fb     => Tile_0_4_i_S_fb,    
         t_N_req    => Tile_0_4_i_S_req,   
         t_N_ack    => Tile_0_4_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_2_4_i_S_pixel, 
         i_S_x_dest => Tile_2_4_i_S_x_dest,
         i_S_y_dest => Tile_2_4_i_S_y_dest,
         i_S_step   => Tile_2_4_i_S_step,  
         i_S_frame  => Tile_2_4_i_S_frame, 
         i_S_x_orig => Tile_2_4_i_S_x_orig,
         i_S_y_orig => Tile_2_4_i_S_y_orig,
         i_S_fb     => Tile_2_4_i_S_fb,    
         i_S_req    => Tile_2_4_i_S_req,   
         i_S_ack    => Tile_2_4_i_S_ack,   
                                                   
         t_S_pixel  => Tile_4_4_i_N_pixel, 
         t_S_x_dest => Tile_4_4_i_N_x_dest,
         t_S_y_dest => Tile_4_4_i_N_y_dest,
         t_S_step   => Tile_4_4_i_N_step,  
         t_S_frame  => Tile_4_4_i_N_frame, 
         t_S_x_orig => Tile_4_4_i_N_x_orig,
         t_S_y_orig => Tile_4_4_i_N_y_orig,
         t_S_fb     => Tile_4_4_i_N_fb,    
         t_S_req    => Tile_4_4_i_N_req,   
         t_S_ack    => Tile_4_4_i_N_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_2_4_i_E_pixel, 
         i_E_x_dest => Tile_2_4_i_E_x_dest,
         i_E_y_dest => Tile_2_4_i_E_y_dest,
         i_E_step   => Tile_2_4_i_E_step,  
         i_E_frame  => Tile_2_4_i_E_frame, 
         i_E_x_orig => Tile_2_4_i_E_x_orig,
         i_E_y_orig => Tile_2_4_i_E_y_orig,
         i_E_fb     => Tile_2_4_i_E_fb,    
         i_E_req    => Tile_2_4_i_E_req,   
         i_E_ack    => Tile_2_4_i_E_ack,   
                                                   
         t_E_pixel  => Tile_2_4_i_E_pixel, 
         t_E_x_dest => Tile_2_4_i_E_x_dest,
         t_E_y_dest => Tile_2_4_i_E_y_dest,
         t_E_step   => Tile_2_4_i_E_step,  
         t_E_frame  => Tile_2_4_i_E_frame, 
         t_E_x_orig => Tile_2_4_i_E_x_orig,
         t_E_y_orig => Tile_2_4_i_E_y_orig,
         t_E_fb     => Tile_2_4_i_E_fb,    
         t_E_req    => Tile_2_4_i_E_req,   
         t_E_ack    => Tile_2_4_i_E_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_2_4_i_W_pixel, 
         i_W_x_dest => Tile_2_4_i_W_x_dest,
         i_W_y_dest => Tile_2_4_i_W_y_dest,
         i_W_step   => Tile_2_4_i_W_step,  
         i_W_frame  => Tile_2_4_i_W_frame, 
         i_W_x_orig => Tile_2_4_i_W_x_orig,
         i_W_y_orig => Tile_2_4_i_W_y_orig,
         i_W_fb     => Tile_2_4_i_W_fb,    
         i_W_req    => Tile_2_4_i_W_req,   
         i_W_ack    => Tile_2_4_i_W_ack,   
                                                   
         t_W_pixel  => Tile_2_2_i_E_pixel, 
         t_W_x_dest => Tile_2_2_i_E_x_dest,
         t_W_y_dest => Tile_2_2_i_E_y_dest,
         t_W_step   => Tile_2_2_i_E_step,  
         t_W_frame  => Tile_2_2_i_E_frame, 
         t_W_x_orig => Tile_2_2_i_E_x_orig,
         t_W_y_orig => Tile_2_2_i_E_y_orig,
         t_W_fb     => Tile_2_2_i_E_fb,    
         t_W_req    => Tile_2_2_i_E_req,   
         t_W_ack    => Tile_2_2_i_E_ack    
     );                                                
-----------------------------------------------------------
Tile_4_0 : entity work.tile               
     generic map(                                 
         y_init     => 4,                         
         x_init     => 0,
         file_name  => File_4_0                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_4_0,                   
         -- North connections                     
         i_N_pixel  => Tile_4_0_i_N_pixel, 
         i_N_x_dest => Tile_4_0_i_N_x_dest,
         i_N_y_dest => Tile_4_0_i_N_y_dest,
         i_N_step   => Tile_4_0_i_N_step,  
         i_N_frame  => Tile_4_0_i_N_frame, 
         i_N_x_orig => Tile_4_0_i_N_x_orig,
         i_N_y_orig => Tile_4_0_i_N_y_orig,
         i_N_fb     => Tile_4_0_i_N_fb,    
         i_N_req    => Tile_4_0_i_N_req,   
         i_N_ack    => Tile_4_0_i_N_ack,   
                                                   
         t_N_pixel  => Tile_2_0_i_S_pixel, 
         t_N_x_dest => Tile_2_0_i_S_x_dest,
         t_N_y_dest => Tile_2_0_i_S_y_dest,
         t_N_step   => Tile_2_0_i_S_step,  
         t_N_frame  => Tile_2_0_i_S_frame, 
         t_N_x_orig => Tile_2_0_i_S_x_orig,
         t_N_y_orig => Tile_2_0_i_S_y_orig,
         t_N_fb     => Tile_2_0_i_S_fb,    
         t_N_req    => Tile_2_0_i_S_req,   
         t_N_ack    => Tile_2_0_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_4_0_i_S_pixel, 
         i_S_x_dest => Tile_4_0_i_S_x_dest,
         i_S_y_dest => Tile_4_0_i_S_y_dest,
         i_S_step   => Tile_4_0_i_S_step,  
         i_S_frame  => Tile_4_0_i_S_frame, 
         i_S_x_orig => Tile_4_0_i_S_x_orig,
         i_S_y_orig => Tile_4_0_i_S_y_orig,
         i_S_fb     => Tile_4_0_i_S_fb,    
         i_S_req    => Tile_4_0_i_S_req,   
         i_S_ack    => Tile_4_0_i_S_ack,   
                                                   
         t_S_pixel  => Tile_4_0_i_S_pixel, 
         t_S_x_dest => Tile_4_0_i_S_x_dest,
         t_S_y_dest => Tile_4_0_i_S_y_dest,
         t_S_step   => Tile_4_0_i_S_step,  
         t_S_frame  => Tile_4_0_i_S_frame, 
         t_S_x_orig => Tile_4_0_i_S_x_orig,
         t_S_y_orig => Tile_4_0_i_S_y_orig,
         t_S_fb     => Tile_4_0_i_S_fb,    
         t_S_req    => Tile_4_0_i_S_req,   
         t_S_ack    => Tile_4_0_i_S_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_4_0_i_E_pixel, 
         i_E_x_dest => Tile_4_0_i_E_x_dest,
         i_E_y_dest => Tile_4_0_i_E_y_dest,
         i_E_step   => Tile_4_0_i_E_step,  
         i_E_frame  => Tile_4_0_i_E_frame, 
         i_E_x_orig => Tile_4_0_i_E_x_orig,
         i_E_y_orig => Tile_4_0_i_E_y_orig,
         i_E_fb     => Tile_4_0_i_E_fb,    
         i_E_req    => Tile_4_0_i_E_req,   
         i_E_ack    => Tile_4_0_i_E_ack,   
                                                   
         t_E_pixel  => Tile_4_2_i_W_pixel, 
         t_E_x_dest => Tile_4_2_i_W_x_dest,
         t_E_y_dest => Tile_4_2_i_W_y_dest,
         t_E_step   => Tile_4_2_i_W_step,  
         t_E_frame  => Tile_4_2_i_W_frame, 
         t_E_x_orig => Tile_4_2_i_W_x_orig,
         t_E_y_orig => Tile_4_2_i_W_y_orig,
         t_E_fb     => Tile_4_2_i_W_fb,    
         t_E_req    => Tile_4_2_i_W_req,   
         t_E_ack    => Tile_4_2_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_4_0_i_W_pixel, 
         i_W_x_dest => Tile_4_0_i_W_x_dest,
         i_W_y_dest => Tile_4_0_i_W_y_dest,
         i_W_step   => Tile_4_0_i_W_step,  
         i_W_frame  => Tile_4_0_i_W_frame, 
         i_W_x_orig => Tile_4_0_i_W_x_orig,
         i_W_y_orig => Tile_4_0_i_W_y_orig,
         i_W_fb     => Tile_4_0_i_W_fb,    
         i_W_req    => Tile_4_0_i_W_req,   
         i_W_ack    => Tile_4_0_i_W_ack,   
                                                   
         t_W_pixel  => Tile_4_0_i_W_pixel, 
         t_W_x_dest => Tile_4_0_i_W_x_dest,
         t_W_y_dest => Tile_4_0_i_W_y_dest,
         t_W_step   => Tile_4_0_i_W_step,  
         t_W_frame  => Tile_4_0_i_W_frame, 
         t_W_x_orig => Tile_4_0_i_W_x_orig,
         t_W_y_orig => Tile_4_0_i_W_y_orig,
         t_W_fb     => Tile_4_0_i_W_fb,    
         t_W_req    => Tile_4_0_i_W_req,   
         t_W_ack    => Tile_4_0_i_W_ack    
     );                                                
-----------------------------------------------------------
Tile_4_2 : entity work.tile               
     generic map(                                 
         y_init     => 4,                         
         x_init     => 2,
         file_name  => File_4_2                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_4_2,                   
         -- North connections                     
         i_N_pixel  => Tile_4_2_i_N_pixel, 
         i_N_x_dest => Tile_4_2_i_N_x_dest,
         i_N_y_dest => Tile_4_2_i_N_y_dest,
         i_N_step   => Tile_4_2_i_N_step,  
         i_N_frame  => Tile_4_2_i_N_frame, 
         i_N_x_orig => Tile_4_2_i_N_x_orig,
         i_N_y_orig => Tile_4_2_i_N_y_orig,
         i_N_fb     => Tile_4_2_i_N_fb,    
         i_N_req    => Tile_4_2_i_N_req,   
         i_N_ack    => Tile_4_2_i_N_ack,   
                                                   
         t_N_pixel  => Tile_2_2_i_S_pixel, 
         t_N_x_dest => Tile_2_2_i_S_x_dest,
         t_N_y_dest => Tile_2_2_i_S_y_dest,
         t_N_step   => Tile_2_2_i_S_step,  
         t_N_frame  => Tile_2_2_i_S_frame, 
         t_N_x_orig => Tile_2_2_i_S_x_orig,
         t_N_y_orig => Tile_2_2_i_S_y_orig,
         t_N_fb     => Tile_2_2_i_S_fb,    
         t_N_req    => Tile_2_2_i_S_req,   
         t_N_ack    => Tile_2_2_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_4_2_i_S_pixel, 
         i_S_x_dest => Tile_4_2_i_S_x_dest,
         i_S_y_dest => Tile_4_2_i_S_y_dest,
         i_S_step   => Tile_4_2_i_S_step,  
         i_S_frame  => Tile_4_2_i_S_frame, 
         i_S_x_orig => Tile_4_2_i_S_x_orig,
         i_S_y_orig => Tile_4_2_i_S_y_orig,
         i_S_fb     => Tile_4_2_i_S_fb,    
         i_S_req    => Tile_4_2_i_S_req,   
         i_S_ack    => Tile_4_2_i_S_ack,   
                                                   
         t_S_pixel  => Tile_4_2_i_S_pixel, 
         t_S_x_dest => Tile_4_2_i_S_x_dest,
         t_S_y_dest => Tile_4_2_i_S_y_dest,
         t_S_step   => Tile_4_2_i_S_step,  
         t_S_frame  => Tile_4_2_i_S_frame, 
         t_S_x_orig => Tile_4_2_i_S_x_orig,
         t_S_y_orig => Tile_4_2_i_S_y_orig,
         t_S_fb     => Tile_4_2_i_S_fb,    
         t_S_req    => Tile_4_2_i_S_req,   
         t_S_ack    => Tile_4_2_i_S_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_4_2_i_E_pixel, 
         i_E_x_dest => Tile_4_2_i_E_x_dest,
         i_E_y_dest => Tile_4_2_i_E_y_dest,
         i_E_step   => Tile_4_2_i_E_step,  
         i_E_frame  => Tile_4_2_i_E_frame, 
         i_E_x_orig => Tile_4_2_i_E_x_orig,
         i_E_y_orig => Tile_4_2_i_E_y_orig,
         i_E_fb     => Tile_4_2_i_E_fb,    
         i_E_req    => Tile_4_2_i_E_req,   
         i_E_ack    => Tile_4_2_i_E_ack,   
                                                   
         t_E_pixel  => Tile_4_4_i_W_pixel, 
         t_E_x_dest => Tile_4_4_i_W_x_dest,
         t_E_y_dest => Tile_4_4_i_W_y_dest,
         t_E_step   => Tile_4_4_i_W_step,  
         t_E_frame  => Tile_4_4_i_W_frame, 
         t_E_x_orig => Tile_4_4_i_W_x_orig,
         t_E_y_orig => Tile_4_4_i_W_y_orig,
         t_E_fb     => Tile_4_4_i_W_fb,    
         t_E_req    => Tile_4_4_i_W_req,   
         t_E_ack    => Tile_4_4_i_W_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_4_2_i_W_pixel, 
         i_W_x_dest => Tile_4_2_i_W_x_dest,
         i_W_y_dest => Tile_4_2_i_W_y_dest,
         i_W_step   => Tile_4_2_i_W_step,  
         i_W_frame  => Tile_4_2_i_W_frame, 
         i_W_x_orig => Tile_4_2_i_W_x_orig,
         i_W_y_orig => Tile_4_2_i_W_y_orig,
         i_W_fb     => Tile_4_2_i_W_fb,    
         i_W_req    => Tile_4_2_i_W_req,   
         i_W_ack    => Tile_4_2_i_W_ack,   
                                                   
         t_W_pixel  => Tile_4_0_i_E_pixel, 
         t_W_x_dest => Tile_4_0_i_E_x_dest,
         t_W_y_dest => Tile_4_0_i_E_y_dest,
         t_W_step   => Tile_4_0_i_E_step,  
         t_W_frame  => Tile_4_0_i_E_frame, 
         t_W_x_orig => Tile_4_0_i_E_x_orig,
         t_W_y_orig => Tile_4_0_i_E_y_orig,
         t_W_fb     => Tile_4_0_i_E_fb,    
         t_W_req    => Tile_4_0_i_E_req,   
         t_W_ack    => Tile_4_0_i_E_ack    
     );                                                
-----------------------------------------------------------
Tile_4_4 : entity work.tile               
     generic map(                                 
         y_init     => 4,                         
         x_init     => 4,
         file_name  => File_4_4                          
     )                                            
     port map(                                    
         clk => clk,                              
         reset => reset,                          
         -----------------------------------------
         done => done_4_4,                   
         -- North connections                     
         i_N_pixel  => Tile_4_4_i_N_pixel, 
         i_N_x_dest => Tile_4_4_i_N_x_dest,
         i_N_y_dest => Tile_4_4_i_N_y_dest,
         i_N_step   => Tile_4_4_i_N_step,  
         i_N_frame  => Tile_4_4_i_N_frame, 
         i_N_x_orig => Tile_4_4_i_N_x_orig,
         i_N_y_orig => Tile_4_4_i_N_y_orig,
         i_N_fb     => Tile_4_4_i_N_fb,    
         i_N_req    => Tile_4_4_i_N_req,   
         i_N_ack    => Tile_4_4_i_N_ack,   
                                                   
         t_N_pixel  => Tile_2_4_i_S_pixel, 
         t_N_x_dest => Tile_2_4_i_S_x_dest,
         t_N_y_dest => Tile_2_4_i_S_y_dest,
         t_N_step   => Tile_2_4_i_S_step,  
         t_N_frame  => Tile_2_4_i_S_frame, 
         t_N_x_orig => Tile_2_4_i_S_x_orig,
         t_N_y_orig => Tile_2_4_i_S_y_orig,
         t_N_fb     => Tile_2_4_i_S_fb,    
         t_N_req    => Tile_2_4_i_S_req,   
         t_N_ack    => Tile_2_4_i_S_ack,   
         -----------------------------------------
         -- South connections                     
         i_S_pixel  => Tile_4_4_i_S_pixel, 
         i_S_x_dest => Tile_4_4_i_S_x_dest,
         i_S_y_dest => Tile_4_4_i_S_y_dest,
         i_S_step   => Tile_4_4_i_S_step,  
         i_S_frame  => Tile_4_4_i_S_frame, 
         i_S_x_orig => Tile_4_4_i_S_x_orig,
         i_S_y_orig => Tile_4_4_i_S_y_orig,
         i_S_fb     => Tile_4_4_i_S_fb,    
         i_S_req    => Tile_4_4_i_S_req,   
         i_S_ack    => Tile_4_4_i_S_ack,   
                                                   
         t_S_pixel  => Tile_4_4_i_S_pixel, 
         t_S_x_dest => Tile_4_4_i_S_x_dest,
         t_S_y_dest => Tile_4_4_i_S_y_dest,
         t_S_step   => Tile_4_4_i_S_step,  
         t_S_frame  => Tile_4_4_i_S_frame, 
         t_S_x_orig => Tile_4_4_i_S_x_orig,
         t_S_y_orig => Tile_4_4_i_S_y_orig,
         t_S_fb     => Tile_4_4_i_S_fb,    
         t_S_req    => Tile_4_4_i_S_req,   
         t_S_ack    => Tile_4_4_i_S_ack,   
         -----------------------------------------
         -- East connections                     
         i_E_pixel  => Tile_4_4_i_E_pixel, 
         i_E_x_dest => Tile_4_4_i_E_x_dest,
         i_E_y_dest => Tile_4_4_i_E_y_dest,
         i_E_step   => Tile_4_4_i_E_step,  
         i_E_frame  => Tile_4_4_i_E_frame, 
         i_E_x_orig => Tile_4_4_i_E_x_orig,
         i_E_y_orig => Tile_4_4_i_E_y_orig,
         i_E_fb     => Tile_4_4_i_E_fb,    
         i_E_req    => Tile_4_4_i_E_req,   
         i_E_ack    => Tile_4_4_i_E_ack,   
                                                   
         t_E_pixel  => Tile_4_4_i_E_pixel, 
         t_E_x_dest => Tile_4_4_i_E_x_dest,
         t_E_y_dest => Tile_4_4_i_E_y_dest,
         t_E_step   => Tile_4_4_i_E_step,  
         t_E_frame  => Tile_4_4_i_E_frame, 
         t_E_x_orig => Tile_4_4_i_E_x_orig,
         t_E_y_orig => Tile_4_4_i_E_y_orig,
         t_E_fb     => Tile_4_4_i_E_fb,    
         t_E_req    => Tile_4_4_i_E_req,   
         t_E_ack    => Tile_4_4_i_E_ack,   
         -----------------------------------------
         -- West connections                     
         i_W_pixel  => Tile_4_4_i_W_pixel, 
         i_W_x_dest => Tile_4_4_i_W_x_dest,
         i_W_y_dest => Tile_4_4_i_W_y_dest,
         i_W_step   => Tile_4_4_i_W_step,  
         i_W_frame  => Tile_4_4_i_W_frame, 
         i_W_x_orig => Tile_4_4_i_W_x_orig,
         i_W_y_orig => Tile_4_4_i_W_y_orig,
         i_W_fb     => Tile_4_4_i_W_fb,    
         i_W_req    => Tile_4_4_i_W_req,   
         i_W_ack    => Tile_4_4_i_W_ack,   
                                                   
         t_W_pixel  => Tile_4_2_i_E_pixel, 
         t_W_x_dest => Tile_4_2_i_E_x_dest,
         t_W_y_dest => Tile_4_2_i_E_y_dest,
         t_W_step   => Tile_4_2_i_E_step,  
         t_W_frame  => Tile_4_2_i_E_frame, 
         t_W_x_orig => Tile_4_2_i_E_x_orig,
         t_W_y_orig => Tile_4_2_i_E_y_orig,
         t_W_fb     => Tile_4_2_i_E_fb,    
         t_W_req    => Tile_4_2_i_E_req,   
         t_W_ack    => Tile_4_2_i_E_ack    
     );                                                
end architecture behavioral;                                          