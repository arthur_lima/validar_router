----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/30/2020 01:02:56 PM
-- Design Name: 
-- Module Name: bram_axi_test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bram_axi_test is
    port(
        ----------------------------------------------------
        -- port to write pixel values from the AXI
        i_axi_clk   : in std_logic;
        i_axi_we    : in std_logic;
    
        i_axi_x     : in signed(31 downto 0);
        i_axi_y     : in signed(31 downto 0);
        i_axi_s     : in signed(31 downto 0);
        i_axi_f     : in signed(31 downto 0);
        i_axi_px    : in signed(31 downto 0);
    
        o_axi_px    : out signed(31 downto 0);
        o_done_tmp  : out std_logic
    );
end bram_axi_test;

architecture Behavioral of bram_axi_test is
component bram_pixel_memory is

generic(
    x_init          : natural := 0;
    y_init          : natural := 0;
    isf_x           : natural;
    isf_y           : natural;
    isf_s           : natural;
    isf_f           : natural;
    isf_px          : natural;
    img_width       : natural;
    img_height      : natural;
    n_frames        : natural;
    n_steps         : natural
);

port(
    ----------------------------------------------------
    -- port to write pixel values from the AXI
    i_axi_clk   : in std_logic;
    i_axi_we    : in std_logic;

    i_axi_x     : in signed(31 downto 0);
    i_axi_y     : in signed(31 downto 0);
    i_axi_s     : in signed(31 downto 0);
    i_axi_f     : in signed(31 downto 0);
    i_axi_px    : in signed(31 downto 0);

    o_axi_px    : out signed(31 downto 0);
    ----------------------------------------------------
    -- ports to access from the Tile Router
    i_reset         : in std_logic;
    i_clk           : in std_logic;
    i_enable        : in std_logic;
    i_wr_enable     : in std_logic;

    i_router_x      : in  signed(6-1 downto 0);
    i_router_y      : in  signed(6-1 downto 0);
    i_router_s      : in  signed(6-1 downto 0);
    i_router_f      : in  signed(5-1 downto 0);
    i_router_px     : in  signed(9-1 downto 0);
    
    o_router_opx    : out   signed(9-1 downto 0);
    o_router_fpx    : out std_logic;
    o_out_ok        : out std_logic
);
end component bram_pixel_memory;

    ----------------------------------------------------
    -- ports to access from the Tile Router
    signal i_reset         : std_logic;
    signal i_clk           : std_logic;
    signal i_enable        : std_logic;
    signal i_wr_enable     : std_logic;
    
    signal i_router_x      :  signed(6-1 downto 0);
    signal i_router_y      :  signed(6-1 downto 0);
    signal i_router_s      :  signed(6-1 downto 0);
    signal i_router_f      :  signed(5-1 downto 0);
    signal i_router_px     :  signed(9-1 downto 0);
    
    signal o_router_opx    :  signed(9-1 downto 0);
    signal o_router_fpx    :  std_logic;
    signal o_out_ok        :  std_logic;
    
    signal so_done_tmp  : std_logic := '0';
    
    constant noc_width : natural := 3; -- noc size
    constant noc_height : natural := 3; -- noc size
    constant px_true_size : natural := 8;   -- true pixel depth
    constant pix_depth : natural := px_true_size + 1; -- must include +1 because of foward/backward 
    constant img_width : natural := 6; -- resolution
    constant img_height: natural := 6; -- resolution
    constant tile_width  : natural := 2; -- tile resolution 
    constant tile_height : natural := 2; -- tile resolution
    constant n_steps     : natural := 6; -- steps in the image processing chain
    constant n_frames    : natural := 5; -- frames needed by an algorithm
    constant isf_x : natural := img_width; -- temporary 
    constant isf_y : natural := img_height; -- temporary
    constant isf_s : natural := n_steps; -- temporary
    constant isf_f : natural := n_frames; -- temporary
    constant isf_px: natural := pix_depth; -- px value range = 0 to 255
    
begin

    bram_inst : bram_pixel_memory generic   map(
                                                x_init => 0,
                                                y_init => 0,
                                                isf_x     => isf_x     ,
                                                isf_y     => isf_y     ,
                                                isf_s     => isf_s     ,
                                                isf_f     => isf_f     ,
                                                isf_px    => isf_px    ,
                                                img_width => img_width ,
                                                img_height=> img_height,
                                                n_frames  => n_frames  ,
                                                n_steps   => n_steps
                                                )
                                    port map(
                                            i_axi_clk    => i_axi_clk   ,
                                            i_axi_we     => i_axi_we    ,
                                            i_axi_x      => i_axi_x     ,
                                            i_axi_y      => i_axi_y     ,
                                            i_axi_s      => i_axi_s     ,
                                            i_axi_f      => i_axi_f     ,
                                            i_axi_px     => i_axi_px    ,
                                            o_axi_px     => o_axi_px    ,
                                            i_reset      => i_reset     ,
                                            i_clk        => i_clk       ,
                                            i_enable     => i_enable    ,
                                            i_wr_enable  => i_wr_enable ,
                                            i_router_x   => i_router_x  ,
                                            i_router_y   => i_router_y  ,
                                            i_router_s   => i_router_s  ,
                                            i_router_f   => i_router_f  ,
                                            i_router_px  => i_router_px ,
                                            o_router_opx => o_router_opx,
                                            o_router_fpx => o_router_fpx,
                                            o_out_ok     => o_out_ok    
                                            );

    o_done_tmp <= so_done_tmp;

end Behavioral;
